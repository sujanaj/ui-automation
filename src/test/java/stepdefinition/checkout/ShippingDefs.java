package stepdefinition.checkout;

import commonfunctions.CommonFunctions;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import org.testng.asserts.SoftAssert;
import pages.signin.RegisterSignInPage;
import pages.ShippingFormPage;

import java.util.List;

public class ShippingDefs {

    RegisterSignInPage registerSignInPage = new RegisterSignInPage();
    ShippingFormPage shippingFormPage = new ShippingFormPage();
    CommonFunctions commonFunctions = new CommonFunctions();
    SoftAssert assertion = new SoftAssert();

    @Then("^I should see the '(.*)' validation Error Message$")
    public void iShouldSeeAValidationErrorMessage(String expectedMessage) {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + ": " + expectedMessage);
        List<String> errorMessages = shippingFormPage.grabValidationMessages();
        Assert.assertTrue("Failure: email error message was not as expected. Expected: " + expectedMessage
                + " -- Actual: " + errorMessages, errorMessages.contains(expectedMessage));
    }

    @Then("I click 'CONTINUE' then I am navigated to the 'REVIEW and PAY' page")
    public void clickingContinueNavigatesToReviewAndPayPage() {

        shippingFormPage.clickOnContinue();
        String actualUrl = commonFunctions.getCurrentURL();
        assertion.assertTrue(actualUrl.contains("/checkout/review-and-pay"),"Failure. If I click 'CONTINUE' I am navigated to the 'REVIEW and PAY' page. Actual url: "
                + actualUrl + " -- Expected: /checkout/review-and-pay");
        assertion.assertAll();
    }

}
