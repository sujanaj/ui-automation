package stepdefinition.account;

//import cucumber.api.DataTable;
import cucumber.api.java.en.Then;
import io.cucumber.datatable.DataTable;
import org.junit.Assert;
import org.testng.asserts.SoftAssert;
import pages.account.AddressBookPage;
import pages.account.MyCreditsPage;


import java.math.BigDecimal;
import java.util.List;

public class MyCreditsDefs {
    config.ObjPropertyReader obj_Property= new config.ObjPropertyReader();
    MyCreditsPage myCreditsPage = new MyCreditsPage();
    SoftAssert assertion = new SoftAssert();


    @Then("^the Gift Card Activation Code field should contain '(.*)' as Placeholder Text$")
    public void verifyCardActivationMessage(String expectedValue) {
        String actualValue = myCreditsPage.grabGiftCardSerialLabel();

        assertion.assertTrue(actualValue.contains(String.valueOf(expectedValue)),"Card activation message not as expected. Actual: " + actualValue + " -- Expected: " + expectedValue);
    }

    private void selectCreditCurrency(String currencyKey, String currencySymbol) {
        myCreditsPage.selectCurrency(currencyKey);
        String actualTotal = myCreditsPage.grabConvertTotal();
        assertion.assertTrue(actualTotal.contains(currencySymbol), "Total currency not as expected. Actual: " + actualTotal + " -- Expected: " + currencySymbol);
    }

    @Then("^For county '(.*)' the Total should be correctly summed and the currency symbol updated$")
    public void theTotalShouldBeCorrectlySummedAndTheCurrencySymbolUpdated(String country, DataTable conversionTable) {
        List<List<String>> rawConversionTable = conversionTable.asLists();
        if (rawConversionTable.size() >= 2)
            for (int i = 1; i < rawConversionTable.size(); i++) {

                List<String> row = rawConversionTable.get(i);
                String currencyKey = row.get(0);
                String currencySymbol = row.get(1);
                if (country.equalsIgnoreCase("United Kingdom") || country.equalsIgnoreCase("United States") || country.equalsIgnoreCase("France")) {
                    if (currencyKey.equalsIgnoreCase("GBP") || currencyKey.equalsIgnoreCase("USD") || currencyKey.equalsIgnoreCase("EUR")) {
                        selectCreditCurrency(currencyKey, currencySymbol);
                    }
                } else if (country.equalsIgnoreCase("Australia")) {
                    if (currencyKey.equalsIgnoreCase("GBP") || currencyKey.equalsIgnoreCase("USD") || currencyKey.equalsIgnoreCase("EUR") || currencyKey.equalsIgnoreCase("AUD")) {
                        selectCreditCurrency(currencyKey, currencySymbol);
                    }
                } else if (country.equalsIgnoreCase("Hong Kong")) {
                    if (currencyKey.equalsIgnoreCase("GBP") || currencyKey.equalsIgnoreCase("USD") || currencyKey.equalsIgnoreCase("EUR") || currencyKey.equalsIgnoreCase("HKD")) {
                        selectCreditCurrency(currencyKey, currencySymbol);
                    }
                } else if (country.equalsIgnoreCase("Japan")) {
                    if (currencyKey.equalsIgnoreCase("GBP") || currencyKey.equalsIgnoreCase("USD") || currencyKey.equalsIgnoreCase("EUR") || currencyKey.equalsIgnoreCase("JPY")) {
                        selectCreditCurrency(currencyKey, currencySymbol);
                    }
                }
            }
        assertion.assertAll();
    }
}
