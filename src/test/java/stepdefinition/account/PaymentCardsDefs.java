package stepdefinition.account;


import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.asserts.SoftAssert;
import pages.account.PaymentCardsPage;


public class PaymentCardsDefs {

    SoftAssert assertion = new SoftAssert();
    PaymentCardsPage paymentCardsPage = new PaymentCardsPage();

    @When("^I click Delete under the payment card where the Card Owner is '(.*)'$")
    public void iClickDeleteCardWhereOwnerIsGiven(String owner) {
        assertion.assertTrue(paymentCardsPage.isCardOnPage(owner),"Failure. The payment card where the Card Owner is " + owner
                + " exists on the page. Actual result: " + paymentCardsPage.isCardOnPage(owner) + " -- Expected result: true");

        paymentCardsPage.clickOnDeleteButton(owner);
    }

    @And("^I click Delete in the overlay confirmation window$")
    public void iClickDeleteInTheOverlayConfirmationWindow() {
        assertion.assertTrue(paymentCardsPage.checkIfDeleteConfirmationModalIsDisplayed(),"Failure. The confirmation modal should be displayed. Actual result: " + paymentCardsPage.checkIfDeleteConfirmationModalIsDisplayed()
                + " -- Expected: true");

        paymentCardsPage.clickOnDeleteButtonInModal();
    }

    @Then("^the payment card where the Card Owner is '(.*)' no longer exists on the page$")
    public void theDeletedPaymentCardNoLongerExistsOnThePage(String owner) {
        assertion.assertFalse(paymentCardsPage.isCardOnPage(owner),"Failure. The payment card where the Card Owner is " + owner
                + "exists on the page. Actual result: true -- Expected result: " + paymentCardsPage.isCardOnPage(owner));
    }
}
