package stepdefinition.auth;

import commonfunctions.CommonFunctions;
import commonfunctions.NavigateUsingURL;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.asserts.SoftAssert;
import pages.signin.RegisterSignInPage;
import utility.CustomStringUtils;

public class RegistrationDefs {

    RegisterSignInPage registerSignInPage = new RegisterSignInPage();
    CustomStringUtils customStringUtils = new CustomStringUtils();
    CommonFunctions commonFunctions= new CommonFunctions();
    NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    SoftAssert assertion= new SoftAssert();

    @When("^I select '(.*)' from the Title Dropdown List for language '(.*)'$")
    public void iSelectTitleFromTheTitleDropdownList(String title,String language) {
        registerSignInPage.selectTitle(title,language);
    }

    @When("^I enter '(.*)' in the First Name field$")
    public void iEnterFirstNameInTheFirstNameField(String firstName) {
        registerSignInPage.inputFirstName(firstName);
    }

    @When("^I enter '(.*)' in the Last Name field$")
    public void iEnterLastNameInTheLastNameField(String lastName) {
        registerSignInPage.inputLastName(lastName);
    }


    @And("^I enter new email '(.*)' in the Email field$")
    public void iEnterNewEmailFirst_nameInTheEmailField(String firstName)  {
        registerSignInPage.inputEmail(firstName);

    }

    @When("^I enter '(.*)' in the Phone Number field$")
    public void iEnterPhoneNumberInThePhoneNumberField(String phone) {
        registerSignInPage.inputPhone(phone);
    }


    @When("^I enter '(.*)' in the Password field$")
    public void iEnterMatchesInThePasswordField(String password) {
        registerSignInPage.inputPassword(password);
    }

    @When("^I enter '(.*)' in the Confirm Password field$")
    public void iEnterMatchesInTheConfirmPasswordField(String password) {
        registerSignInPage.inputConfirmPassword(password);
    }

    @And("^I select the promotional email checkbox$")
    public void iSelectThePromotionalEmailCheckbox()  {
        registerSignInPage.selectEmailable();
    }

    @And("^I deselect the promotional email checkbox$")
    public void iDeselectThePromotionalEmailCheckbox() {
        registerSignInPage.deselectEmailable();
    }


    @When("^I click Create An Account$")
    public void iClickCreateAnAccount() {
        registerSignInPage.clickOnCreateAnAccount();
    }

    @When("^I continue with Create An Account$")
    public void iContinueCreateAnAccount() {
        registerSignInPage.clickOnContinueCreateAnAccountButton();
    }

    @Then("^I should be redirected to the homepage$")
    public void iShouldBeRedirectedToTheHomepage() {
        String actualURL = commonFunctions.getCurrentURL();
        System.out.println("URL: " + actualURL);

        actualURL = customStringUtils.removeProtocolFromURL(actualURL);
        String expectedURL = customStringUtils.removeProtocolFromURL(navigateUsingURL.getURL());

        assertion.assertTrue(actualURL.contains(expectedURL), "Failure: URL does not match expected url. Expected: " + expectedURL + " -- Actual: " + actualURL);
        assertion.assertTrue(!actualURL.contains("login"), "Failure: URL contains 'login'. Expected not to contain: 'login' -- Actual: " + actualURL);
        assertion.assertAll();
    }

    @And("^I enter existing email in the Email field$")
    public void iEnterEmailInTheEmailField()  {
        registerSignInPage.inputExisitngEmail();
    }

    @And("^I enter existing password in the Password field$")
    public void iEnterExistingPasswordInThePasswordField()  {
        registerSignInPage.inputPasswordForExitingEmail();
    }

    @And("^I enter existing password in the Confirm Password field$")
    public void iEnterExistingPasswordInTheConfirmPasswordField()  {
        registerSignInPage.inputConfirmPasswordForExitingEmail();
        }
}
