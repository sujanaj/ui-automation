package stepdefinition.auth;

import commonfunctions.CommonFunctions;
//import cucumber.api.DataTable;
import config.ObjPropertyReader;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.testng.asserts.SoftAssert;
import pages.HeaderPage;
import pages.auth.AuthenticationPage;
import io.cucumber.datatable.DataTable;
import java.util.List;


public class LoggedInUserDefs {

    AuthenticationPage authenticationPage = new AuthenticationPage();
    HeaderPage headerPage = new HeaderPage();
    CommonFunctions commonFunctions = new CommonFunctions();
    SoftAssert assertion = new SoftAssert();
    ObjPropertyReader objPropertyReader = new ObjPropertyReader();

    @And("^I navigate to the My Details page$")
    public void navigateContactDetails() {
        authenticationPage.clickAccountLink("contactdetails");

    }

    @And("^I navigate to the My Account page$")
    public void iNavigateToTheMyAccountPage()  {
        headerPage.selectMyAccountOption();
    }

    @And("^I click on '(.*)' link on My Account page$")
    public void iClicksOnManagePreferencesLinkOnMyAccountPage(String link)  {
        headerPage.clickOnMyAccountLink(link);
        String actualURL = commonFunctions.getCurrentURL();
        assertion.assertTrue(actualURL.contains("/account/managepreferences".toLowerCase()),"Failure: My Account menu link URL not as expected. Expected: " + "/account/managepreferences" + " -- Actual: "
                + actualURL);
        assertion.assertAll();

    }

    @When("^I navigate to the My Credits page$")
    public void iNavigateToTheMyCreditsPage() {
        headerPage.selectMyCreditsOption();
    }

    @And("^I navigate to the Payment Cards page$")
    public void iNavigateToThePaymentCardsPage(){
        headerPage.selectPaymentCardsOption();
    }

    @Then("^each dropdown link should take me to the correct page$")
    public void eachDropdownLinkShouldTakeMeToTheCorrectPage(DataTable dataTable) {
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2)
            for (int i = 1; i < rawTable.size(); i++) {
                String link = rawTable.get(i).get(0);
                String url = rawTable.get(i).get(1);
                //headerSteps.selectDropDownOption(link);
                authenticationPage.clickAccountLink(link);
                String actualURL = commonFunctions.getCurrentURL();
                assertion.assertTrue(
                        actualURL.contains(url.toLowerCase()), "Failure: Account menu link URL not as expected. Expected: " + url + " -- Actual: " + actualURL);
            }
        assertion.assertAll();
    }

    @Then("^each link should take me to the correct page$")
    public void eachLinkShouldTakeMeToTheCorrectPage(DataTable dataTable) {
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2)
            for (int i = 1; i < rawTable.size(); i++) {
                String link = rawTable.get(i).get(0);
                String url = rawTable.get(i).get(1);
                headerPage.selectMyAccountOption();
                headerPage.clickOnMyAccountLink(link);
                String actualURL = commonFunctions.getCurrentURL();
                assertion.assertTrue(actualURL.contains(url.toLowerCase()),"Failure: My Account menu link URL not as expected. Expected: " + url + " -- Actual: "
                        + actualURL);
            }
            assertion.assertAll();
    }

}
