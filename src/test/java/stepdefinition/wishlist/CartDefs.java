package stepdefinition.wishlist;

import commonfunctions.CommonFunctions;
import cucumber.api.java.en.And;
import pages.HeaderPage;
import pages.cart.CartPage;

public class CartDefs {

    CommonFunctions commonFunctions = new CommonFunctions();
    HeaderPage headerPage = new HeaderPage();
    CartPage cartPage = new CartPage();

    @And("I proceed to my shopping bag")
    public void iProceedToMyShoppingBag() throws Throwable {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        String impersonationBaseUrl = "9001";
        if (commonFunctions.getCurrentURL().contains(impersonationBaseUrl)) {
            headerPage.goToImpersonationCart();
        } else {
            headerPage.goToCart();
        }
    }

    @And("I proceed to checkout from the shopping bag")
    public void iProceedToCheckoutFromTheShoppingBag() {
        cartPage.clickOnButtomProceedToPurchaseButton();
    }
}
