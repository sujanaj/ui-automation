package stepdefinition.softLoginDefs;

import commonfunctions.CommonFunctions;
import commonfunctions.NavigateUsingURL;
import config.Constants;
import cucumber.api.PendingException;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.asserts.SoftAssert;
import pages.signin.SignInPage;
import pages.softLogin.*;
import stepdefinition.signIn.SignInDefs;

public class SoftLoginDefs {

    SoftAssert assertion = new SoftAssert();
    CommonFunctions commonFunctions= new CommonFunctions();
    SoftLoginPage softLoginPage = new SoftLoginPage();
    NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    SignInPage signInPage = new SignInPage();

    @And("^I navigate to the page '(.*)'$")
    public void iNavigateToThePageUrl(String url) {
        commonFunctions.waitForPageToLoad();
        navigateUsingURL.navigateToRelativeURL(url);
        commonFunctions.waitForPageToLoad();
    }

    @Then("^I should be navigated to new hard login page '(.*)'$")
    public void iShouldBeNavigatedToNewHardLoginPage(String loginURL) {

//        assertion.assertTrue(commonFunctions.getCurrentURL().equalsIgnoreCase(softLoginPage.getExpectedURL(loginURL)),"Failure: URL should contain /login. Actual:" +commonFunctions.getCurrentURL()+ "Expected:" +softLoginPage.getExpectedURL(loginURL));
        assertion.assertTrue(commonFunctions.getCurrentURL().contains(loginURL),"Failure: URL should contain /login. Actual:" +commonFunctions.getCurrentURL()+ "Expected:" +softLoginPage.getExpectedURL(loginURL));
        assertion.assertFalse(softLoginPage.isEmailIdEmpty(),"Failure: Email id shouldn't be empty. Actual:" + softLoginPage.isEmailIdEmpty());
        assertion.assertAll();
    }

    @When("^I enter password and click on login button$")
    public void iEnterPasswordAndClickOnLoginButton()  {
        softLoginPage.enterPassword();
        signInPage.clickOnLoginButton();
    }

    @Then("^I should be navigated to '(.*)'$")
    public void iShouldBeNavigatedToUrl(String url) {
//        assertion.assertTrue(commonFunctions.getCurrentURL().equalsIgnoreCase(softLoginPage.getExpectedURL(url)), "Failure: URL should contain: " +url+ "Actual:" + commonFunctions.getCurrentURL() + "Expected:" + softLoginPage.getExpectedURL(url));
        assertion.assertTrue(commonFunctions.getCurrentURL().contains(url), "Failure: URL should contain: " +url+ "Actual:" + commonFunctions.getCurrentURL() + "Expected:" + softLoginPage.getExpectedURL(url));
        assertion.assertAll();
    }

    @And("^'Remember Me' cookie exist after sign-in$")
    public void rememberMeCookieExistAfterSignIn() {
        assertion.assertTrue(softLoginPage.isRememberMeCookieExist(), "Failure: 'Remember Me' cookie doesn't exist. Expected: TRUE, Actual: " + softLoginPage.isRememberMeCookieExist());
        assertion.assertAll();
    }

    @Then("^'Remember Me' cookie shouldn't exist$")
    public void rememberMeCookieShouldnTExist() {
        assertion.assertFalse(softLoginPage.isRememberMeCookieExist(), "Failure: 'Remember Me' cookie exist. Expected: FALSE, Actual: " + softLoginPage.isRememberMeCookieExist());
        assertion.assertAll();
    }

    @When("^I click on ‘Signed into different account’ link$")
    public void iClickOnSignedIntoDifferentAccountLink()  {
        softLoginPage.clickOnSignedIntoDifferentAccountLink();
    }

    @When("^I click on ‘Create a new account’ link$")
    public void iClickOnCreateANewAccountLink() {
        softLoginPage.ClickOnCreateANewAccountLink();
    }

    @When("^I click on ‘Checkout as a guest’ link$")
    public void iClickOnCheckoutAsAGuestLink() {
        softLoginPage.ClickOnCheckoutAsAGuestLink();

    }
}
