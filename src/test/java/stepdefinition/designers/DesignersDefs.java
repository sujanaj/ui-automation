package stepdefinition.designers;

import commonfunctions.CommonFunctions;
import config.Constants;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.testng.asserts.SoftAssert;

public class DesignersDefs {

    CommonFunctions commonFunctions = new CommonFunctions();
    SoftAssert softAssert = new SoftAssert();
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    String designerCollectionLink = obj_Property.readProperty("designerCollectionLinkByCSS");
    String designerPageDesignerHeader = obj_Property.readProperty("designerPageDesignerHeaderByCSS");
    String signUpDesignerUpdatesLink = obj_Property.readProperty("emailSignupDesignerUpdatesButtonByID");
    String closeBtnSignupPopup = obj_Property.readProperty("wishlistOverlayCloseButtonLocator");
    String submitbtnDesignerUpdatepopUp = obj_Property.readProperty("submitbtnDesignerUpdatepopUpByID");
    String emailAddressFieldDesignerUpdate = obj_Property.readProperty("emailFieldDesignerUpdatePopUpByCSS");
    String firstNameFieldDesignerUpdatePopUp = obj_Property.readProperty("firstNameFieldDesignerUpdatePopUpByID");
    String lastNameFieldDesignerUpdatePopUp = obj_Property.readProperty("lastNameFieldDesignerUpdatePopUpByCSS");
    String countryDropDownDesignerUpdatePopUp = obj_Property.readProperty("countryDropDownDesignerUpdatePopUpByID");
    String thankYouMsgDesignerUpdatePopUp = obj_Property.readProperty("thankYouMsgDesignerUpdatePopUpByID");
    String msgUnderneathThankYouDesignerUpdatePopUp = obj_Property.readProperty("msgUnderneathThankYouDesignerUpdatePopUpByID");
    String titleDesignerUpdatePopUp = obj_Property.readProperty("titleDesignerUpdatePopUpByID");

    @And("^I click on the other gender of that designer's page$")
    public void iClickonGenderDesginerCollectionlink() {
        commonFunctions.waitForPageToLoad();
        commonFunctions.clickElementByCSS(designerCollectionLink);
    }

    @Then("^I should switch to the other gender of that designer's page$")
    public void switchtoOtherGenderDesignerPage(){
        String currentUrl = commonFunctions.getCurrentURL();
        String expectedUrl = "/womens/designers/gucci";
        String actualDesigner = commonFunctions.retrieveTextWithNoEscapeChars(designerPageDesignerHeader);
        softAssert.assertTrue(currentUrl.contains(expectedUrl), "Failure. I should be able to switch to the following currencies using the Homepage Currency Dropdown List. Actual currency list: "
                    + currentUrl + " -- Expected: " + expectedUrl);
        softAssert.assertTrue(actualDesigner.equalsIgnoreCase(Constants.DESIGNER_NAME), "Failure. Expected Designer page is not "+Constants.DESIGNER_NAME+
                "Actual is   "+actualDesigner);
        softAssert.assertAll();
    }

    @When("^I click on sign up for designer updates$")
    public void iClickonSignUpDesignerUpdates(){
        commonFunctions.selectRadioButton(signUpDesignerUpdatesLink);
    }


    @Then("^I should be able to close signup designer updates pop-up$")
    public void iShouldBeAbleToCloseDesignerUpdatePopup(){
            commonFunctions.switchBackToParentFrame();
            commonFunctions.clickElementByCSS(closeBtnSignupPopup);
    }

    @And("^I submit the designer update email form$")
    public void iSubmitEmptyForm(){
        commonFunctions.waitForPageToLoad();
        commonFunctions.selectRadioButton(submitbtnDesignerUpdatepopUp);
        commonFunctions.waitForPageToLoad();
    }

    @Then("^I should see the following error message '(.*)' on designer pop up$")
    public void iShouldSeeErrormsgsOnPopUp(String errorMsg){
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForPageToLoad();
       String actualErrorMsg = commonFunctions.retrieveText("#errors") ;
       softAssert.assertTrue(actualErrorMsg.equalsIgnoreCase(errorMsg),"Actual and Expected error messages on Designers page are not same." +
               "Actual error message : "+actualErrorMsg+"  Expected error message:"+errorMsg);
        softAssert.assertAll();
    }

    @Then("^I switch to iFrame$")
    public void iSwitchToIframe(){
        commonFunctions.waitForPageToLoad();
        commonFunctions.getWebDriver().switchTo().frame(commonFunctions.getWebDriver().findElement(By.tagName("iframe")));
    }
    @And("^I enter '(.*)' in email address field$")
    public void iEnterEmailAddress(String email){
        commonFunctions.enterTextInputBox(emailAddressFieldDesignerUpdate,email);
    }

    @And("^I select '(.*)' from title drop down$")
    public void iSelectTitleFromDropdown(String title){
        commonFunctions.selectValueFromDropDownviaSelect(titleDesignerUpdatePopUp,title);
    }

    @And("^I enter '(.*)' in first name field$")
    public void iEnterFirstName(String firstName){
        commonFunctions.enterTextInputBox(firstNameFieldDesignerUpdatePopUp,firstName);
    }

    @And("^I enter '(.*)' in last name field$")
    public void iEnterLastName(String lastName){
        commonFunctions.enterTextInputBox(lastNameFieldDesignerUpdatePopUp,lastName);
    }

    @And("^I select '(.*)' from country drop down$")
    public void iSelectCountryFromDD(String country){
        commonFunctions.selectValueFromDropDownviaSelect(countryDropDownDesignerUpdatePopUp,country);
    }

    @Then("^I see '(.*)' and '(.*)' message on the pop-up$")
    public void iSeeThankUMsgOnPopup(String thankUMsg,String msg){
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForPageToLoad();
        String actualThankUMsg = commonFunctions.getWebElement(thankYouMsgDesignerUpdatePopUp).findElement(By.cssSelector("img")).getAttribute("alt");
        String actualMsg = commonFunctions.retrieveText(msgUnderneathThankYouDesignerUpdatePopUp);
        softAssert.assertTrue(actualThankUMsg.equalsIgnoreCase(thankUMsg),"Failure! Actual and Expected Thank you messages are not same." +
                "Actual  "+actualThankUMsg+"   Expected is : "+thankUMsg);
        softAssert.assertTrue(actualMsg.equalsIgnoreCase(msg),"Failure! Actual and Expected messages underneath Thank you are not same." +
                "Actual  "+actualMsg+"   Expected is : "+msg);
        softAssert.assertAll();
    }
}

