package stepdefinition.navigation;

import commonfunctions.CommonFunctions;
import config.Constants;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import io.cucumber.datatable.DataTable;
//import org.apache.log4j.Logger;
import org.junit.Assert;
import org.testng.asserts.SoftAssert;
import pages.PreHomePage;

import java.util.ArrayList;
import java.util.List;

public class PreHomePageDefs {

    SoftAssert softAssert = new SoftAssert();
    CommonFunctions commonFunctions = new CommonFunctions();
    final static private String URL_PATH_PREHOMEPAGE = "/intl/";
   // final static Logger logger = Logger.getLogger(PreHomePageDefs.class);

    @Then("^I should see the homepage links in the correct order in '(.*)'$")
    public void iShouldSeeTheHomepageLinksInTheCorrectOrder(String language) {
       // logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        PreHomePage preHomepage = new PreHomePage();
        List<String> expectedMenPreHomePageLinks = new ArrayList<String>();
        List<String> expectedWomenPreHomePageLinks = new ArrayList<String>();
        List<String> actualMenPreHomePageLinks = new ArrayList<String>();
        List<String> actualWomenPreHomePageLinks = new ArrayList<String>();
        List<String> genderList = new ArrayList<>();
        genderList.add("men");
        genderList.add("women");

        for(String gender : genderList){
            if(gender.equalsIgnoreCase("men")){
                expectedMenPreHomePageLinks = preHomepage.getPrehomePageLinks(language,"men");
                actualMenPreHomePageLinks = preHomepage.grabActualMenuList(gender);
            }else {
                expectedWomenPreHomePageLinks = preHomepage.getPrehomePageLinks(language,"women");
                actualWomenPreHomePageLinks = preHomepage.grabActualMenuList(gender);
            }
        }
            softAssert.assertTrue(expectedMenPreHomePageLinks.size()==actualMenPreHomePageLinks.size(), " Expected number of links" +
                    " on men prehome page are " +expectedMenPreHomePageLinks.size() +
                    "Actual number of links Men pre home page in "+language+ "  are " +actualMenPreHomePageLinks.size());
            softAssert.assertTrue(expectedWomenPreHomePageLinks.size()==actualWomenPreHomePageLinks.size(), " Expected number of links" +
                    " on women prehome page are" +expectedWomenPreHomePageLinks.size() +
                    "Actual number of links Women pre home page in "+language+ " are " +actualWomenPreHomePageLinks.size());
            softAssert.assertTrue(expectedMenPreHomePageLinks.equals(actualMenPreHomePageLinks), "Expected and Actual links on Men PreHomePage in "+language+
                " are not as expected. Actual: "+actualMenPreHomePageLinks.toString()+"Expected: "+expectedMenPreHomePageLinks.toString());
            softAssert.assertTrue(expectedWomenPreHomePageLinks.equals(actualWomenPreHomePageLinks), "Expected and Actual links on Women PreHomePage " +language+
                " are not as expected. Actual: "+actualWomenPreHomePageLinks+" Expected: "+expectedWomenPreHomePageLinks.toString());
            softAssert.assertAll();
    }
}

