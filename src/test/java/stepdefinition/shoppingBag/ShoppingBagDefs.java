package stepdefinition.shoppingBag;
import commonfunctions.CommonFunctions;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import org.testng.asserts.SoftAssert;
import cucumber.api.java.en.When;
import pages.ProductDetailPage;
import pages.shoppingbag.ShoppingBagPage;

import java.util.List;

public class ShoppingBagDefs {
    CommonFunctions commonFunctions = new CommonFunctions();
    ShoppingBagPage shoppingBagPage = new ShoppingBagPage();
    SoftAssert softAssert = new SoftAssert();
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    String removeItemBtnOnShoppingBag = obj_Property.readProperty("removeItemShoppingBagByCSS");
    String totalItemsOnShoppingBagByCSS = obj_Property.readProperty("totalItemsOnShoppingBagByCSS");
    String emptyShoppingBagMsgByCSS = obj_Property.readProperty("emptyShoppingBagMsgByCSS");
    String shoppingBagTitle = obj_Property.readProperty("shoppingbagTitleByCSS");
    String shoppingbagSizeDropdown = obj_Property.readProperty("shoppingbagSizeDropdownByCSS");
    String shoppingbagSelectedSizedd = obj_Property.readProperty("shoppingbagSelectedSizeByCSS");
    String shoppingbagQtydd = obj_Property.readProperty("shoppingbagQtyddByCSS");
    String shoppingbagSelectedQuantity = obj_Property.readProperty("shoppingbagSelectedQuantityByCSS");
    String minicartIcon = obj_Property.readProperty("minicartIconByCSS");
    ProductDetailPage productDetailPage = new ProductDetailPage();

    @Then("^I remove all items from my shopping bag via the link$")
    public void removeAllItemsFromShoppingBag() {
        int totalItems = Integer.parseInt(commonFunctions.getWebElement(totalItemsOnShoppingBagByCSS).getAttribute("innerHTML").replaceAll("[A-za-z!\\&[\n];,:.]", "").trim());
        for (int i = 0; i <= totalItems - 1; i++) {
            commonFunctions.waitForPageToLoad();
            commonFunctions.clickElementByCSS(removeItemBtnOnShoppingBag);
        }
    }

    @And("^I should see the message '(.*)' confirming my Shopping Bag is empty$")
    public void verfifyShoppingBagIsEmpty(String shoppingBagBtnTextExpected) {
        commonFunctions.waitForPageToLoad();
        String actualEmptyShoppingBagMsgByCSS = commonFunctions.getTextByCssSelector(emptyShoppingBagMsgByCSS);
        softAssert.assertEquals(shoppingBagBtnTextExpected.equalsIgnoreCase(actualEmptyShoppingBagMsgByCSS), "Empty shopping bag has not been displayed");
    }

    @When("^I should be on '(.*)' page$")
    public void iShouldBeOnShoppingBag(String title){
        String actualTitle = commonFunctions.retrieveText(shoppingBagTitle);
        softAssert.assertTrue(actualTitle.equalsIgnoreCase(title), " Actual and Expected title of the page are not same. " +
                "Expected" + title + " -- Actual: " + actualTitle);
        softAssert.assertAll();
    }

    @And("^I change the size of the product on shopping bag$")
    public void iChangeTheSizeOfTheProduct(){
        List<String> availableSizelist = shoppingBagPage.retrieveAvailableSizesonShoppingBag();
        productDetailPage.requiredProductSize = availableSizelist.get(availableSizelist.size()-1);
        commonFunctions.selectFromDropdownByText(shoppingbagSizeDropdown,productDetailPage.requiredProductSize);
        commonFunctions.waitForPageToLoad();
    }

    @And("^I verify the size selected for the product$")
    public void iSeeTheSelectedSize(){
        commonFunctions.waitForPageToLoad();
        String expectedSize=productDetailPage.requiredProductSize;
        String actualSelectedSize = commonFunctions.retrieveText(shoppingbagSelectedSizedd).trim();
        softAssert.assertTrue(actualSelectedSize.equalsIgnoreCase(expectedSize), " Actual and Expected sizes are not same. " +
                "Expected" + expectedSize + " -- Actual: " + actualSelectedSize);
        softAssert.assertAll();
    }

    @And("^I change the quantity of the product to quantity$")
    public void iChangeQtyofTheProduct(){
        List<String> qtyList = shoppingBagPage.quantityListonShoppingBag();
        shoppingBagPage.setQuantity(qtyList.get(qtyList.size()-1));
        commonFunctions.selectFromDropdownByText(shoppingbagQtydd,shoppingBagPage.getQuantity());
    }

    @Then("^I verify the quantity of the product if changed to '(.*)'$")
    public void iVerifyQtyifChanged(String qty){
        commonFunctions.waitForPageToLoad();
        String expectedQty ="";
        if(qty.isEmpty()){
            expectedQty = shoppingBagPage.getQuantity();
        }else
            expectedQty = qty;
        String actualQty = commonFunctions.retrieveText(shoppingbagSelectedQuantity).trim();
        softAssert.assertTrue(actualQty.equalsIgnoreCase(expectedQty), "Actual and Expected quantities are not same."+
                "Expected: "+expectedQty+"\tActual: "+actualQty);
        softAssert.assertAll();
    }

    @And("^I go to shopping bag clicking on minicart$")
    public void iGotoShoppingBagClickingOnMinicart(){
        commonFunctions.clickElementByCSS(minicartIcon);
    }

    @Then("I change the size of first product to '(.*)'$")
    public void iChangesizeOfFirstProduct(String size){
        String productSize = "";
        if(size.isEmpty()){
            productSize = productDetailPage.requiredProductSize;
        }else
            productSize = size;
        shoppingBagPage.changeFirstProductSize(productSize);
    }
}
