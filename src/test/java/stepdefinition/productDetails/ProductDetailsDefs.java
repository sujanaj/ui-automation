package stepdefinition.productDetails;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import commonfunctions.ProductDetailsFromJson;
import config.*;
import config.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;

import commonfunctions.CommonFunctions;
import commonfunctions.CustomVerification;
import commonfunctions.NavigateUsingURL;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import pages.MiniCartPage;
import pages.ProductDetailPage;

public class ProductDetailsDefs {
   // public String productType = null;
    public String productID;
    public String productDesc_Instock;
    public String productDesc_Instock_SecondProduct;
    public String productDesc_instock_ThirdProduct;
    public String productDesc_instock_OneSize;
    public String productDesc_instock_SoltOut;
    public String productDesc_instock_FourthProduct;
   public ArrayList<String> product;
    boolean found;
    Boolean solrApi;
    SoftAssert assertions = new SoftAssert();
    NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    CommonFunctions commonFunctions = new CommonFunctions();
    ProductDetailPage productDetailPage = new ProductDetailPage();
    ArrayList<String> productcodewithVarientCode;
    String japChars = "/[\u3000-\u303F]|[\u3040-\u309F]|[\u30A0-\u30FF]|[\uFF00-\uFFEF]|[\u4E00-\u9FAF]|[\u2605-\u2606]|[\u2190-\u2195]|\u203B/g";
    GetInstockProductsDetail getInstockProductsDetail = new GetInstockProductsDetail();
    GetInstockMultipleProductsDetail getInstockMultipleProductsDetail = new GetInstockMultipleProductsDetail();
    GetInstockOneSizeProductsDetail getInstockOneSizeProductsDetail = new GetInstockOneSizeProductsDetail();
    GetSoldOutProductsDetail getSoldOutProductsDetail = new GetSoldOutProductsDetail();
    ProductDetailsFromJson productDetailsFromJson = new ProductDetailsFromJson();
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();

    String pdp_SizeDropDown_CSS = obj_Property.readProperty("pdp_SizeDropDown_CSS");
    String addToCartButtonLocatorLocator = obj_Property.readProperty("addToCartButtonLocatorLocator");
    String productQTYDropDownByCSS = obj_Property.readProperty("productQTYDropDownByCSS");
    String productSize = "";
    String productId;
    SoftAssert softAssertions = new SoftAssert();
    String AddToBagButtonStickyNav_CSS = obj_Property.readProperty("AddToBagButtonStickyNav_CSS");
    String minicartIconNumber = obj_Property.readProperty("minicartIconNumber");
    String pdp_FullScreen_CSS = obj_Property.readProperty("pdp_FullScreen_CSS");
    String pdp_SizeDropDown_FullScreen_CSS = obj_Property.readProperty("pdp_SizeDropDown_FullScreen_CSS");
    String addToBagFullScreen_CSS = obj_Property.readProperty("addToBagFullScreen_CSS");
    String errorMessageUnderSizeFullScreenByCSS = obj_Property.readProperty("errorMessageUnderSizeFullScreenByCSS");
    String pdpAccordionDetailsContainerByCSS = obj_Property.readProperty("pdpAccordionDetailsContainerByCSS");
    String pdpDescriptionAccordionBottomLocator = obj_Property.readProperty("pdpDescriptionAccordionBottomLocator");
    String pdpDetailsAccordionButtonLocator = obj_Property.readProperty("pdpDetailsAccordionButtonLocator");
    String pdpDetailsAccordionScrollBottomLocator = obj_Property.readProperty("pdpDetailsAccordionScrollBottomLocator");
    String pdpSizeAndFitAccordionButtonLocator = obj_Property.readProperty("pdpSizeAndFitAccordionButtonLocator");
    String pdpSizeAndFitAccordionScrollBottomLocator = obj_Property.readProperty("pdpSizeAndFitAccordionScrollBottomLocator");
    String productPriceOnPDPByCSS = obj_Property.readProperty("productPriceOnPDPByCSS");
   String productInactiveQTYDropDownByCSS = obj_Property.readProperty("productInactiveQTYDropDownByCSS");
    String productAactiveQTYDropDownByCSS = obj_Property.readProperty("productAactiveQTYDropDownByCSS");
   String productAddToBagSoldOutMsgByCSS = obj_Property.readProperty("productAddToBagSoldOutMsgByCSS");
   String qtyDropDownStickyNav = obj_Property.readProperty("qtyDropDownStickyNav");
    String pdp_SizeDropDownInactiveByCSS = obj_Property.readProperty("pdp_SizeDropDownInactiveByCSS");
   String getStickyNavSizeValue = obj_Property.readProperty("getStickyNavSizeValue");
    String getStickyNavQTYValue = obj_Property.readProperty("getStickyNavQTYValue");
    String pdpAddToBagSoldOutText = obj_Property.readProperty("pdpAddToBagSoldOutText");
    String viewVideoLinkLocatorByCSS = obj_Property.readProperty("viewVideoLinkLocatorByCSS");
    String videoContainerLocatorByCSS = obj_Property.readProperty("videoContainerLocatorByCSS");
    String videoIsPlayingLocatorByCSS = obj_Property.readProperty("videoIsPlayingLocatorByCSS");
    String closeVideoBtnLocatorByCSS = obj_Property.readProperty("closeVideoBtnLocatorByCSS");
    String videoThumbnailLocatorByCSS = obj_Property.readProperty("videoThumbnailLocatorByCSS");
    String pdpvideoContainerLocatorByCSS = obj_Property.readProperty("pdpvideoContainerLocatorByCSS");
    String pdpPauseVideoBtnLocator = obj_Property.readProperty("pdpPauseVideoBtnLocator");
    String PDPplayPauseVideoBtnDescriptionLocatorByCSS = obj_Property.readProperty("PDPplayPauseVideoBtnDescriptionLocatorByCSS");
    String pdpVideoContainerLocatorByCSS = obj_Property.readProperty("pdpVideoContainerLocatorByCSS");
    String pdpPlayVideoBtnLocatorByCSS = obj_Property.readProperty("pdpPlayVideoBtnLocatorByCSS");
    String pdpPreviousBtnLocatorByCSS = obj_Property.readProperty("pdpPreviousBtnLocatorByCSS");
    String pdpNextBtnLocatorByCSS = obj_Property.readProperty("pdpNextBtnLocatorByCSS");
    String pdpGetmainImageIndexByCSS = obj_Property.readProperty("pdpGetmainImageIndexByCSS");
    String pdpThumbImageIndex = obj_Property.readProperty("pdpThumbImageIndex");
    String pdpViewLargeThumbnailsLinkLocatorByCSS = obj_Property.readProperty("pdpViewLargeThumbnailsLinkLocatorByCSS");
    String pdplargeThumbnailsContainerLocaotrByCSS = obj_Property.readProperty("pdplargeThumbnailsContainerLocaotrByCSS");
    String pdpActiveLargeThumbnailLocatorByCSS = obj_Property.readProperty("pdpActiveLargeThumbnailLocatorByCSS");
    String pdpViewSmallThumbnailsLinkLocatorByCSS = obj_Property.readProperty("pdpViewSmallThumbnailsLinkLocatorByCSS");
    String pdpActiveMainImageDivLocatorByCSS = obj_Property.readProperty("pdpActiveMainImageDivLocatorByCSS");
    String pdpGridContainerLocatorByCSS = obj_Property.readProperty("pdpGridContainerLocatorByCSS");
    String pdpActiveMainImageListLocatorByCSS = obj_Property.readProperty("pdpActiveMainImageListLocatorByCSS");
    String fullscreenContainerLocator = obj_Property.readProperty("fullscreenContainerLocator");
    String pdpFullscreenThumbnailsDivListLocatorByCSS = obj_Property.readProperty("pdpFullscreenThumbnailsDivListLocatorByCSS");
    String pdpFullscreenMainImgListLocator = obj_Property.readProperty("pdpFullscreenMainImgListLocator");
    String pdpFullscreenImageNumberLocator = obj_Property.readProperty("pdpFullscreenImageNumberLocator");
    String fullscreenThumbnailsListLocator = obj_Property.readProperty("fullscreenThumbnailsListLocator");
    String pdpFullscreenPrevBtnLocatorByCSS = obj_Property.readProperty("pdpFullscreenPrevBtnLocatorByCSS");
    String pdpFullscreenNextBtnLocator = obj_Property.readProperty("pdpFullscreenNextBtnLocator");
    String fullscreenThumbnailsDivListLocator = obj_Property.readProperty("pdpFullscreenThumbnailsDivListLocatorByCSS");
    String pdpReturnsButtonLocatorByCSS = obj_Property.readProperty("pdpReturnsButtonLocatorByCSS");
    String pdpRetrunOverlayLocatorByCSS = obj_Property.readProperty("pdpRetrunOverlayLocatorByCSS");
    String pdpGetReturnOverlayTitleByCSS = obj_Property.readProperty("pdpGetReturnOverlayTitleByCSS");
    String pdpCountryDropDownOnReturnOverlayByCSS = obj_Property.readProperty("pdpCountryDropDownOnReturnOverlayByCSS");
    String pdpReturnOverlayShippingChargesByCSS = obj_Property.readProperty("pdpReturnOverlayShippingChargesByCSS");
    String pdpDeliveryButtonLocatorByCSS = obj_Property.readProperty("pdpDeliveryButtonLocatorByCSS");
    String pdpDeliveryOverlayByCSS = obj_Property.readProperty("pdpDeliveryOverlayByCSS");
    String pdpDeliverOverlayPageTitleByCSS = obj_Property.readProperty("pdpDeliverOverlayPageTitleByCSS");
    String pdpDeliveryChargeByCSS = obj_Property.readProperty("pdpDeliveryChargeByCSS");
    String pdpTopPageVerifyByCSS = obj_Property.readProperty("pdpTopPageVerifyByCSS");
    String pdpShopTheLookButtonLocatorByCSS = obj_Property.readProperty("pdpShopTheLookButtonLocatorByCSS");
    String pdpShopTheLookDisplayed = obj_Property.readProperty("pdpShopTheLookDisplayed");
    String pdpShopTheLookCloseButtonLocator = obj_Property.readProperty("pdpShopTheLookCloseButtonLocator");
    String pdpShopTheLookContainerLocatorByCSS = obj_Property.readProperty("pdpShopTheLookContainerLocatorByCSS");
    String pdpVerifyProductDisplayedOnShopTheLook = obj_Property.readProperty("pdpVerifyProductDisplayedOnShopTheLook");
    String pdpShopTheLookClickOnProductByCSS = obj_Property.readProperty("pdpShopTheLookClickOnProductByCSS");
    String pdpGetStyleElementByCSS = obj_Property.readProperty("pdpGetStyleElementByCSS");
    String pdpSelectSizeForSpecificProductFromShopTheLookByCSS = obj_Property.readProperty("pdpSelectSizeForSpecificProductFromShopTheLookByCSS");
    String pdpQTYDropDownOnShopTheLookByCSS = obj_Property.readProperty("pdpQTYDropDownOnShopTheLookByCSS");
    String pdpQTYDropDownWithOneSizeProductByCSS = obj_Property.readProperty("pdpQTYDropDownWithOneSizeProductByCSS");
    String pdpShopTheLookOutOfStockMessageByCSS = obj_Property.readProperty("pdpShopTheLookOutOfStockMessageByCSS");
    String pdpShopTheLookAddToBagByCSS = obj_Property.readProperty("pdpShopTheLookAddToBagByCSS");
    String minicartLocator = obj_Property.readProperty("minicartLocator");
    String pdpRecentlyViewedTabByCSS = obj_Property.readProperty("pdpRecentlyViewedTabByCSS");
    String pdpYourMatchesTabByCSS = obj_Property.readProperty("pdpYourMatchesTabByCSS");
    String pdpMatchWithItTabByCSS = obj_Property.readProperty("pdpMatchWithItTabByCSS");
    String pdpReentlyViewedProductsTextByCSS = obj_Property.readProperty("pdpReentlyViewedProductsTextByCSS");
    String pdpProductsListOnRecentlyViewedTabByCSS = obj_Property.readProperty("pdpProductsListOnRecentlyViewedTabByCSS");
    String pdpMatchITWithTabByCSS = obj_Property.readProperty("pdpMatchITWithTabByCSS");
    String pdpProductsListOnMatchITWithByCSS = obj_Property.readProperty("pdpProductsListOnMatchITWithByCSS");
    String pdpGetTextOfFirstProductFromMatchITWithByCSS = obj_Property.readProperty("pdpGetTextOfFirstProductFromMatchITWithByCSS");
    String pdpGetProductDescriptionByCSS = obj_Property.readProperty("pdpGetProductDescriptionByCSS");
    String pdpSocilaLinkByCSS = obj_Property.readProperty("pdpSocilaLinkByCSS");
    String pdpEmailAFriendByCSS = obj_Property.readProperty("pdpEmailAFriendByCSS");
    String pdpShareByEmailOverlayByCSS = obj_Property.readProperty("pdpShareByEmailOverlayByCSS");
    String pdpShareByEmailCloseOverlayByCSS = obj_Property.readProperty("pdpShareByEmailCloseOverlayByCSS");
    String pdpSizeGuideLinkLocatorByCSS = obj_Property.readProperty("pdpSizeGuideLinkLocatorByCSS");
    String pdpSizeAndFitGuideOverlayLocatorByCSS = obj_Property.readProperty("pdpSizeAndFitGuideOverlayLocatorByCSS");
    String pdpSizeGuidecloseButtonLocatorByCSS = obj_Property.readProperty("pdpSizeGuidecloseButtonLocatorByCSS");
    String pdpSizeGuideButtonLocatorInAccordianByCSS = obj_Property.readProperty("pdpSizeGuideButtonLocatorInAccordianByCSS");
    String pdpGetSelectedValueFromSizeGuideDropDown = obj_Property.readProperty("pdpGetSelectedValueFromSizeGuideDropDown");
    String pdpGetSelectedValueFromMeasuringGuideDD = obj_Property.readProperty("pdpGetSelectedValueFromMeasuringGuideDropDown");
    String pdpClickOnFirstColourThumbnailsListLocatorByCSS = obj_Property.readProperty("pdpClickOnFirstColourThumbnailsListLocatorByCSS");
    String pdpBreadCrumbItemListLocatorByCSS = obj_Property.readProperty("pdpBreadCrumbItemListLocatorByCSS");
    String pdpClickOnViewAllDesignerLinkLocator = obj_Property.readProperty("pdpClickOnViewAllDesignerLinkLocatorByCSS");
    String pdpClickOnViewAllTopsLinkLocatorByCSS = obj_Property.readProperty("pdpClickOnViewAllTopsLinkLocatorByCSS");
    String addToWishListOnPDPByCSS = obj_Property.readProperty("addToWishListOnPDPByCSS");
    String wishlistMiniRollOverByCSS = obj_Property.readProperty("wishlistMiniRollOverByCSS");
    String goToWishlistOnPDPByCSS = obj_Property.readProperty("goToWishlistOnPDPByCSS");
    String shopTheLookAddToWishlistByCSS = obj_Property.readProperty("shopTheLookAddToWishlistByCSS");
    String shopTheLookGetTextAddToWishlist = obj_Property.readProperty("shopTheLookGetTextAddToWishlist");
    String shopTheLookGoToWishlistByCSS = obj_Property.readProperty("shopTheLookGoToWishlistByCSS");
    String stickyNavAddToWishListByCSS = obj_Property.readProperty("stickyNavAddToWishListByCSS");
    String fullscreenAddToWishListByCSS = obj_Property.readProperty("fullscreenAddToWishListByCSS");
    String fullscreenAddTowishlist = obj_Property.readProperty("fullscreenAddTowishlist");
    String fullscreenGoToWishlistByCSS = obj_Property.readProperty("fullscreenGoToWishlistByCSS");
    String pdpBreadCrumbs = obj_Property.readProperty("pdpBreadCrumbsByCSS");
    String pdpProductDescriptionByCSS = obj_Property.readProperty("pdpProductDescriptionByCSS");


    @When("^I go to '(.*)' product page$")
    public void instockProductPage(String productType) throws IOException {
        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()) {
            if (productType.equalsIgnoreCase("instock")) {
                productDetailPage.setRequiredProduct(Constants.INSTOCK);
                navigateUsingURL.navigateToRelativeURL("/p/" + Constants.INSTOCK);
            } else if (productType.equalsIgnoreCase("soldOut")) {
                navigateUsingURL.navigateToRelativeURL("/p/" + Constants.SOLDOUT);
                productDetailPage.setRequiredProduct(Constants.SOLDOUT);
            } else if (productType.equalsIgnoreCase("oneSize")) {
                navigateUsingURL.navigateToRelativeURL("/p/" + Constants.INSTOCK_ONESIZE);
                productDetailPage.setRequiredProduct(Constants.INSTOCK_ONESIZE);
            } else if (productType.equalsIgnoreCase("oneQty")) {
                navigateUsingURL.navigateToRelativeURL("/p/" + Constants.PRODUCT_ONEQTYONLY);
                productDetailPage.setRequiredProduct(Constants.PRODUCT_ONEQTYONLY);
            } else if (productType.equalsIgnoreCase("instock_SecondProduct")) {
                navigateUsingURL.navigateToRelativeURL("/p/" + Constants.INSTOCK_SECONDPRODUCT);
                productDetailPage.setRequiredProduct(Constants.INSTOCK_SECONDPRODUCT);
            } else if ((productType.equalsIgnoreCase("PRODUCT_MATCHITWITH"))) {
                navigateUsingURL.navigateToRelativeURL("/p/" + Constants.PRODUCT_MATCHITWITH);
                productDetailPage.setRequiredProduct(Constants.PRODUCT_MATCHITWITH);
            }else if (productType.equalsIgnoreCase("instock_ThirdProduct") || (productType.equalsIgnoreCase("PRODUCT_MATCHITWITH"))) {
                product = productDetailsFromJson.getInStockProductIdWithAvailableVarients();
                productDetailPage.setRequiredProduct(product.get(3));
                navigateUsingURL.navigateToRelativeURL("/p/" + product.get(3));
            }
        }
        else{
            if (productType.equalsIgnoreCase("instock")) {
                product = productDetailsFromJson.getInStockProductIdWithAvailableVarients();
                productDetailPage.setRequiredProduct(product.get(0));
                navigateUsingURL.navigateToRelativeURL("/p/"+product.get(0));
                productDesc_Instock= commonFunctions.getTextByCssSelector(pdpProductDescriptionByCSS);
            } else if (productType.equalsIgnoreCase("instock_SecondProduct") || (productType.equalsIgnoreCase("PRODUCT_MATCHITWITH"))) {
                product = productDetailsFromJson.getInStockProductIdWithAvailableVarients();
                productDetailPage.setRequiredProduct(product.get(2));
                navigateUsingURL.navigateToRelativeURL("/p/" + product.get(2));
                productDesc_Instock_SecondProduct= commonFunctions.getTextByCssSelector(pdpProductDescriptionByCSS);
            }else if (productType.equalsIgnoreCase("instock_ThirdProduct") || (productType.equalsIgnoreCase("PRODUCT_MATCHITWITH"))) {
                    product = productDetailsFromJson.getInStockProductIdWithAvailableVarients();
                    productDetailPage.setRequiredProduct(product.get(3));
                    navigateUsingURL.navigateToRelativeURL("/p/" + product.get(3));
                productDesc_instock_ThirdProduct= commonFunctions.getTextByCssSelector(pdpProductDescriptionByCSS);
            }else if (productType.equalsIgnoreCase("instock_FourthProduct") || (productType.equalsIgnoreCase("PRODUCT_MATCHITWITH"))) {
                product = productDetailsFromJson.getInStockProductIdWithAvailableVarients();
                productDetailPage.setRequiredProduct(product.get(4));
                navigateUsingURL.navigateToRelativeURL("/p/" + product.get(4));
                productDesc_instock_FourthProduct= commonFunctions.getTextByCssSelector(pdpProductDescriptionByCSS);
            } else if (productType.equalsIgnoreCase("oneSize")) {
                product = productDetailsFromJson.getInStockOneSizeProductId();
                productDetailPage.setRequiredProduct(product.get(0));
                navigateUsingURL.navigateToRelativeURL("/p/" + product.get(0));
                productDesc_instock_OneSize= commonFunctions.getTextByCssSelector(pdpProductDescriptionByCSS);
            } else if (productType.equalsIgnoreCase("soldOut")) {
                product = productDetailsFromJson.getOutOfStockProductIdWithAvailableVarients();
                productDetailPage.setRequiredProduct(product.get(0));
                navigateUsingURL.navigateToRelativeURL("/p/" + product.get(0));
                productDesc_instock_SoltOut= commonFunctions.getTextByCssSelector(pdpProductDescriptionByCSS);
            }

        }
        commonFunctions.waitForPageToLoad();

    }

    @And("^I add '(.*)' of size '(.*)' to my bag from the PDP$")
    public void iAddQuantityOfSizeToMyBagFromThePDP(String quantity, String size) throws IOException {
          //  if ("false" == getInstockProductsDetail.getProductIdAndSize() || (null == getInstockProductsDetail.getProductIdAndSize())) {
        String pdpSize = "";
              if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
                  pdpSize = size;
        }
        else{
                  pdpSize = productDetailsFromJson.retrieveAvailableProductDetails().get("size");
              }
        productDetailPage.setProductSize(pdpSize);
        commonFunctions.selectFromDropdownByCSS(pdp_SizeDropDown_CSS, pdpSize);
        commonFunctions.clickElementByCSS(addToCartButtonLocatorLocator);
        productDetailPage.selectQtyfromDDonPDPByJSE(quantity);
    }

    @Given("^clicking the 'ADD TO BAG' button causes an error message '(.*)' to appear under the size tab")
    public void verifyErrorMessageUnderSizeDropdounMenu(String errorMessage) {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        commonFunctions.clickElementByCSS(addToCartButtonLocatorLocator);
        productDetailPage.verifySizeMenuError(errorMessage);

    }

    @Then("^I scroll down to the bottom of the page$")
    public void scrolToTheButtomOfPDP() {
        commonFunctions.scrollToPageBottom();
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForPageToLoad();
    }

    @And("^adding '(.*)' of size '(.*)' to bag from the PDP sticky nav wrapper causes the minibag rollover list to appear")
    public void addingProductToCartFromStickyNavCausesChanges(String qty,String size) throws IOException {
        String productSize = "";
        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            productSize = size;
        }else{
            productSize = productDetailsFromJson.retrieveAvailableProductDetails().get("size");
        }
        commonFunctions.selectFromDropdownByCSS(pdp_SizeDropDown_CSS,productSize);
        commonFunctions.clickElementByCSS(addToCartButtonLocatorLocator);
    }

    @Then("^the pdp 'ADD TO BAG' button text is momentarily changed from '(.*)' to '(.*)'$")
    public void changeInTextOnAddingToBag(String text1, String text2){
        CustomVerification verification = new CustomVerification();
        String actualText1 = commonFunctions.getTextByCssSelector(AddToBagButtonStickyNav_CSS);
        text1 = text1.toLowerCase().trim();
        actualText1 = actualText1.toLowerCase().trim();
        verification.verifyTrue("Failure. The initial button text is: " + actualText1 + " -- Expected: " + text1,
                actualText1.equals(text1));
        commonFunctions.waitForElementTextToChangeTo(AddToBagButtonStickyNav_CSS, text2);
        String actualText2 = commonFunctions.getTextByCssSelector(AddToBagButtonStickyNav_CSS);
        actualText2 = actualText2.toLowerCase().trim();
        text2 = text2.toLowerCase().trim();
        verification.verifyTrue(
                "Failure. The button text is momentarily changed to: " + actualText2 + " -- Expected: " + text2,
                actualText2.equals(text2));
        verification.verifyNoErrors();
    }

    @Then("the number next to the mini bag icon on the sticky nav wrapper updates to '(.*)'")
    public void veryfyTheMinibagStickyNavIcon(String expectedminiBagIconItemNumber) {
        commonFunctions.waitForPageToLoad();
        String actualminiBagIconItemNumber = commonFunctions.getTextByCssSelector(minicartIconNumber);
        //Soft Assertion - TestNG
        softAssertions.assertTrue(expectedminiBagIconItemNumber.toLowerCase().trim().contentEquals(actualminiBagIconItemNumber.toLowerCase().trim()),
                "Minicart item number was not the one from example table " + " Expected: "
                        + expectedminiBagIconItemNumber + " Actual: " + actualminiBagIconItemNumber);
        softAssertions.assertAll();

    }

    @Given("^clicking the 'ADD TO BAG' button on the sticky nav wrapper causes an error message '(.*)' to appear under the size tab")
    public void verifyErrorMessegeUnderSizeStickyNav(String errorMessage) {
        commonFunctions.clickElementByCSS(AddToBagButtonStickyNav_CSS);
        productDetailPage.verifySizeMenuError(errorMessage);
    }

    @When("^I click on the PDP 'VIEW FULLSCREEN' button$")
    public void iClickOnTheViewOrCloseFullscreenButton() {
        productDetailPage.clickOnFullScreen(pdp_FullScreen_CSS);
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForPageToLoad();
    }

    @And("I select size '(.*)' from the full screen product overlay size drop down")
    public void selectQtyAndSizeFromFullScreenPDP(String size) throws IOException{
       // if("false"==getInstockProductsDetail.getProductIdAndSize()||(null==getInstockProductsDetail.getProductIdAndSize())) {
        String productSize="";
        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            productSize = size;
        }
        else{
            productSize = productDetailsFromJson.retrieveAvailableProductDetails().get("size");
        }
        commonFunctions.selectFromDropdownByCSS(pdp_SizeDropDown_FullScreen_CSS, productSize);

    }

    @And("^clicking the 'ADD TO BAG' button on the full screen product overlay causes the minibag rollover list to appear$")
    public void addingProductToCartFromFullScreenCausesChanges() {
        productDetailPage.clickOnAddToBagFullScreen(addToBagFullScreen_CSS);
    }

    @Given("clicking the 'ADD TO BAG' button on the full screen product overlay causes an error message '(.*)' to appear under the size tab")
    public void verifyErrorMessageOnFullscreenUnderSizeDropdounMenu(String errorMessage) {
        productDetailPage.clickOnAddToBagFullScreen(addToBagFullScreen_CSS);
        String actualErrorMessage = productDetailPage.getTextFullScreen(errorMessageUnderSizeFullScreenByCSS);
        ;
        //SoftAssertion TestNG
        softAssertions.assertTrue(errorMessage.contentEquals(actualErrorMessage.replaceAll("[\n]", "").trim()),
                "Error messageis not correct. Expected: " + errorMessage + " Actual: " + actualErrorMessage);
        softAssertions.assertAll();

    }

    @Then("^I should see the Description tab expanded$")
    public void iShouldSeeTheDescriptionTabExpanded() {
        boolean isDescriptionExpanded = productDetailPage.isDescriptionAccordionExpanded(pdpAccordionDetailsContainerByCSS);
        //SoftAssertion TestNG
        softAssertions.assertTrue(isDescriptionExpanded, "Failure: Description seems not to be expanded. ");
        softAssertions.assertAll();
    }

    @Then("^I should be able to scroll to the bottom of the Description tab$")
    public void iShouldBeAbleToScrollToTheBottomOfTheDescriptionTab() {
        productDetailPage.scrollToElementWithinPage(pdpDescriptionAccordionBottomLocator);
    }

    @Then("^I should see the Details tab collapsed$")
    public void iShouldSeeTheDetailsTabCollapsed() {
        boolean isDescriptionExpanded = productDetailPage.isDetailsAccordionExpanded(pdpAccordionDetailsContainerByCSS);
        //SoftAssertion TestNG
        softAssertions.assertTrue(isDescriptionExpanded == false, "Failure: Details seems not to be collapsed. ");
        softAssertions.assertAll();
    }

    @Then("^I should see the Size and Fit tab collapsed$")
    public void iShouldSeeTheSizeAndFitTabCollapsed() {
        boolean isSizeAndFitExpanded = productDetailPage.isSizeAndFitAccordionExpanded(pdpAccordionDetailsContainerByCSS);
        //SoftAssertion TestNG
        softAssertions.assertTrue(isSizeAndFitExpanded == false, "Failure: Size and Fit seems not to be collapsed. ");
        softAssertions.assertAll();
    }

    @When("^I expand Details tab$")
    public void iExpandDetailsTab() {

        boolean isDetailsExpanded = productDetailPage.isDetailsAccordionExpanded(pdpAccordionDetailsContainerByCSS);
        if (!isDetailsExpanded) {
            commonFunctions.clickElementByCSS(pdpDetailsAccordionButtonLocator);
        }
    }

    @Then("^I should be able to scroll to the bottom of the Details tab$")
    public void iShouldBeAbleToScrollToTheBottomOfTheDetailsTab() {
        productDetailPage.scrollToElementWithinPage(pdpDetailsAccordionScrollBottomLocator);
    }

    @Then("^I should see the Description tab collapsed$")
    public void iShouldSeeTheDescriptionTabCollapsed() {
        boolean isDescriptionExpanded = productDetailPage.isDescriptionAccordionExpanded(pdpAccordionDetailsContainerByCSS);
        //SoftAssertion TestNG
        softAssertions.assertTrue(isDescriptionExpanded == false, "Failure: Description seems to be expanded.");
        softAssertions.assertAll();
    }

    @When("^I expand the Size and Fit tab$")
    public void iExpandTheSizeAndFitTab() {
        boolean isSizeAndFitExpanded = productDetailPage.isSizeAndFitAccordionExpanded(pdpAccordionDetailsContainerByCSS);
        if (!isSizeAndFitExpanded) {
            commonFunctions.clickElementByCSS(pdpSizeAndFitAccordionButtonLocator);
        }
    }

    @Then("^I should be able to scroll to the bottom of the Size and Fit tab$")
    public void iShouldBeAbleToScrollToTheBottomOfTheSizeAndFitTab() {
        productDetailPage.scrollToElementWithinPage(pdpSizeAndFitAccordionScrollBottomLocator);
    }

    @When("The price displayed on the PDP is '(.*)'")
    public void priceIsTheSameOnPDP(String expectedPrice) {
        String actualProductPrice = commonFunctions.getTextByCssSelector(productPriceOnPDPByCSS);
        //SoftAssertion TestNG
        if(solrApi=true) {
            softAssertions.assertTrue(!actualProductPrice.isEmpty(),
                    "Product Price was not avaialble on the PDP page");
            softAssertions.assertAll();
        }else{
            softAssertions.assertTrue(actualProductPrice.contains(expectedPrice),
                    "PDP Price was not the same with table price " + " Expected: " + expectedPrice + " Actual: "
                            + actualProductPrice);
            softAssertions.assertAll();

        }
    }


    @When("I select the '(.*)' on the PDP")
    public void selectSize(String size)  throws IOException{
         productSize = "";
        if(!productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            productSize = productDetailsFromJson.retrieveAvailableProductDetails().get("size");
        }else
            productSize = size;

        commonFunctions.selectFromDropdownByCSS(pdp_SizeDropDown_CSS, productSize);
    }

    @Then("by clicking on the QUANTITY drop down list I will not be able to open it")
    public void verifyQantityDromdownMenuCannotBeOpened() {
        boolean found = commonFunctions.isElementDisplayed(productInactiveQTYDropDownByCSS);
        softAssertions.assertTrue(found, "Quantity menu on PDP was not inactive");
        softAssertions.assertAll();
    }

    @Then("by clicking on the PDP size drop down list I can select different sizes")
    public void selectDifferentSizesOnPDP(DataTable dataTable) {
        List<List<String>> rawTable = dataTable.asLists();
        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            if(rawTable.size() >= 1) {
                for (int i = 1; i < rawTable.size(); i++) {
                    String size = rawTable.get(i).get(0);
                    commonFunctions.selectFromDropdownByCSS(pdp_SizeDropDown_CSS, size);
                }
            }
        }else{
            List<String> availableSizesList = productDetailsFromJson.retrieveAvailableSizes();
            if(availableSizesList.size() >= 1){
                for(String size : availableSizesList){
                    commonFunctions.selectFromDropdownByCSS(pdp_SizeDropDown_CSS, size);
                }
            }
        }
    }

    @Then("Quantity drop down list becomes active")
    public void verifyQuantityDropDownMenuActive() {
        boolean found = commonFunctions.isElementDisplayed(productAactiveQTYDropDownByCSS);
        //SoftAssertion TestNG
        softAssertions.assertTrue(found, "Quantity menu on PDP was not active");
        softAssertions.assertAll();
    }

    @Then("by clicking on the PDP quantity drop down list I can select up to ten units")
    public void clickOnElementInQantityListByContains(DataTable dataTable) {
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2)
            for (int i = 1; i < rawTable.size(); i++) {
                String quantity = rawTable.get(i).get(0);
                commonFunctions.selectFromDropdownByCSS(productQTYDropDownByCSS, quantity);

            }
    }

    @Then("by clicking on the 'SIZE' drop down list I will not be able to open it")
    public void verifySizeDropDownMenuNotOpened() {
         found = productDetailPage.isSizeEnabledOnPDP(productQTYDropDownByCSS);

        if (found = true) {
            //SoftAssertions TestNG
            softAssertions.assertTrue(found, "Passes: Size drop down menu disabled. ");
        } else if (found = false) {
            //SoftAssertions TestNG
            softAssertions.assertTrue(found, "Failure: Size drop down menu opened. ");
        }
        softAssertions.assertAll();
    }

    @Then("by clicking on PDP 'SIZE' drop down list and selecting size '(.*)' the 'ADD TO BAG' button should disappear")
    public void verifyAddToBagButtonIsNotDisplayed(String size) {
        String productSize = "";
        if(productDetailsFromJson.getOutOfStockProductIdWithAvailableVarients().isEmpty()){
            productSize = size;
        }else{
            productSize = productDetailsFromJson.retrieveSoldOutSize();
        }
        commonFunctions.selectFromDropdownByCSS(pdp_SizeDropDown_CSS, productSize);
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForPageToLoad();
        boolean addToBagButton = commonFunctions.isElementDisplayed(AddToBagButtonStickyNav_CSS);
        softAssertions.assertFalse(addToBagButton, "The add to bag button is visible on page.");
        softAssertions.assertAll();
    }

    @Then("instead of the 'ADD TO BAG' button there should be a text message '(.*)'")
    public void verifySoldOutPDPMessage(String message) {
        String soldOutMessage = commonFunctions.getTextByCssSelector(productAddToBagSoldOutMsgByCSS);
        ////SoftAssertion TestNG
        softAssertions.assertTrue(message.contentEquals(soldOutMessage),
                "The soldout message is not visible on page. Expected: " + message + " Actual: " + soldOutMessage);
        softAssertions.assertAll();

    }

    @Then("by clicking on the 'SIZE' drop down list and selecting size '(.*)' I will not be able to open 'QUANTITY' drop down list")
    public void verifyQtyDropDownMenuNotOpened(String size) {
         found = productDetailPage.isSizeEnabledOnPDP(productQTYDropDownByCSS);
        if (found = true) {
            softAssertions.assertTrue(found, "Passes: Size drop down menu disabled. ");
        } else if (found = false) {
            softAssertions.assertTrue(found, "Failure: Size drop down menu opened.");
        }
        softAssertions.assertAll();
    }

    @Then("by clicking on the PDP 'SIZE' drop down list on the sticky nav wrapper I can select different sizes")
    public void selectSizeOnStickyNavWrapper(DataTable dataTable) {
        selectDifferentSizesOnPDP(dataTable);
    }

    @Then("'QUANTITY' drop down list on the sticky nav wrapper becomes active")
    public void verifySizeDropDownActive() {
        boolean found = commonFunctions.isElementDisplayed(productAactiveQTYDropDownByCSS);
        softAssertions.assertTrue(found, "Quantity menu on PDP was not inactive");
        softAssertions.assertAll();
    }

    @Then("by clicking on the PDP 'QUANTITY' drop down list on the sticky nav wrapper I can select up to ten units")
    public void clickOnWtyDropDownMenuOnStickyNav(DataTable dataTable) {
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2)
            for (int i = 1; i < rawTable.size(); i++) {
                String quantity = rawTable.get(i).get(0);
                commonFunctions.selectFromDropdownByCSS(productQTYDropDownByCSS, quantity);

            }
    }

    @Then("by clicking on the 'QUANTITY' drop down list on the sticky nav wrapper I will not be able to open it")
    public void verifyQtyDropDownMenuNotOpenedOnStickyNav() {
        found = commonFunctions.isElementDisplayed(qtyDropDownStickyNav);
        softAssertions.assertTrue(found, "Quantity menu on PDP was not inactive");
        softAssertions.assertAll();
    }

    @Then("by clicking on the 'SIZE' drop down list on the sticky nav wrapper I will not be able to open it")
    public void verifySizeDropDownMenuNotOpenedOnStickyNav() {
        found = false;
        String sizeDropDown = commonFunctions.isDropDownEnabled(pdp_SizeDropDownInactiveByCSS);
        if (sizeDropDown.contains("no--arrow")) {
            found = true;
        }
        softAssertions.assertTrue(found, "Size menu on PDP was not inactive");
        softAssertions.assertAll();
    }

    @Given("^by clicking on PDP 'SIZE' drop down list on the sticky nav wrapper and selecting size '(.*)' the 'ADD TO BAG' button should dissapear$")
    public void verifyAddtoBagButtonDissapear(String size) {
        String productSize = "";
        if(productDetailsFromJson.getOutOfStockProductIdWithAvailableVarients().isEmpty()){
            productSize = size;
        }else{
            productSize = productDetailsFromJson.retrieveSoldOutSize();
        }
        commonFunctions.selectFromDropdownByCSS(pdp_SizeDropDown_CSS, productSize);
        commonFunctions.waitForPageToLoad();
        boolean AddToBagButton = commonFunctions.isElementDisplayed(productAddToBagSoldOutMsgByCSS);
        softAssertions.assertFalse(AddToBagButton, "Add To Bag button on PDP should not be Displayed");
        softAssertions.assertAll();
    }

    @Then("by clicking on the 'SIZE' drop down list on the sticky nav wrapper and selecting size '(.*)' I will not be able to open 'QUANTITY' drop down list")
    public void verifyQtyDropDownMenuNotOpenedOnStickyNav(String size) {
        commonFunctions.selectFromDropdownByCSS(pdp_SizeDropDown_CSS, size);
        boolean isQTYDropDownOpened = commonFunctions.getDropDownMenuIsOpened(qtyDropDownStickyNav);
        softAssertions.assertFalse(isQTYDropDownOpened, "Failure: Qty drop down menu is clickable. ");
        softAssertions.assertAll();

    }

    @Then("^I should be able to see the same size '(.*)' and quantity '(.*)' on the sticky nav wrapper$")
    public void verifySizeAndQtyLabels(String size, String qty) {
        String actualSize = commonFunctions.retrieveTextWithNoEscapeChars(getStickyNavSizeValue);
        String actualQty = commonFunctions.retrieveTextWithNoEscapeChars(getStickyNavQTYValue);
        actualSize = actualSize.replaceAll("[\n]", "");
        softAssertions.assertTrue(productDetailPage.getProductSize().trim().contentEquals(actualSize.trim()),
                "Size was not the same with the one selected. Expected: " + productDetailPage.getProductSize() + " Actual: " + actualSize);
        softAssertions.assertTrue(qty.trim().contentEquals(actualQty.trim()),
                "Qty was not the same with the one selected. Expected: " + qty + " Actual: " + actualQty);
        softAssertions.assertAll();
    }

    @Then("^I should be able to see the text '(.*)' in the size drop down list$")
    public void verifySizeText(String sizeFrText) {
        String actualText = commonFunctions.getTextByCssSelector(getStickyNavSizeValue);
        softAssertions.assertTrue(actualText.contentEquals(sizeFrText), "Taille text was not the same with the one in the table Expected: " + sizeFrText + " Actual: "
                + actualText);
        softAssertions.assertAll();
    }

    @Then("^I should be able to see sold out message '(.*)' next to ADD TO BAG button disappear$")
    public void verifyMessageNextToAddToBagButtonIsNotThere(String message) {
        String actualStockMessage = commonFunctions.getTextByCssSelector(pdpAddToBagSoldOutText);
        softAssertions.assertTrue(message.trim().contentEquals(actualStockMessage.trim()),
                "Qty was not the same with the one selected. Expected: " + message + " Actual: " + actualStockMessage);
        softAssertions.assertAll();
    }

    @When("^I click on the VIEW VIDEO link$")
    public void iClickOnTheViewVideoLink() {
        commonFunctions.clickElementByCSS(viewVideoLinkLocatorByCSS);

    }

    @Then("^the video should begin playing over the main PDP image$")
    public void theVideoShouldBeginPlayingOverTheMainPdpImage() {
        Boolean isDisplayed = productDetailPage.theVideoIsDisplayed(videoContainerLocatorByCSS);
        softAssertions.assertTrue(isDisplayed, "Failure. The video should be playing over the main PDP image. Actual result: " + isDisplayed
                + " -- Expected result: true");
        softAssertions.assertAll();
    }

    @And("^the video will continue playing until I click close$")
    public void theVideoWillContinuePlayingUntilIClickClose() {
        Boolean isPlaying = productDetailPage.theVideoIsPlaying(videoIsPlayingLocatorByCSS);
        softAssertions.assertTrue(isPlaying,
                "Failure. The video should be playing. Actual result: " + isPlaying + " -- Expected result: true");
        productDetailPage.clickOnCloseVideoButton(videoContainerLocatorByCSS, closeVideoBtnLocatorByCSS);

        Boolean isDisplayed = productDetailPage.theVideoIsDisplayed(videoContainerLocatorByCSS);

        softAssertions.assertFalse(isDisplayed, "Failure. The video should be closed. " +
                "Actual result: false -- Expected: " + isDisplayed);
        softAssertions.assertAll();
    }

    @When("^I click on the video thumbnail image$")
    public void iClickOnTheVideoThumbnailImage() {
        commonFunctions.clickElementByCSS(videoThumbnailLocatorByCSS);

    }

    @When("^I click on the pause button$")
    public void iClickOnThePauseButton() {
        productDetailPage.clickOnThePauseButton(pdpvideoContainerLocatorByCSS, pdpPauseVideoBtnLocator);
    }

    @Then("^the video should be paused$")
    public void theVideoShouldBePause() {
        Boolean isPaused = productDetailPage.theVideoIsPaused(PDPplayPauseVideoBtnDescriptionLocatorByCSS);
        softAssertions.assertTrue(isPaused,
                "Failure. The video should be paused. Actual result: " + isPaused + " -- Expected result: true");
        softAssertions.assertAll();
    }

    @And("^when I click on the play button$")
    public void whenIClickOnThePlayButton() {
        productDetailPage.clickOnThePlayButton(pdpVideoContainerLocatorByCSS, pdpPlayVideoBtnLocatorByCSS);
    }

    @Then("^the video will begin playing$")
    public void theVideoWillBePlaying() {
        Boolean isPaused = productDetailPage.theVideoIsPaused(PDPplayPauseVideoBtnDescriptionLocatorByCSS);
        softAssertions.assertFalse(isPaused,
                "Failure. The video should be playing. Actual result: " + isPaused + " -- Expected result: false");
        softAssertions.assertAll();
    }

    @Then("^clicking on each thumbnail image causes the main PDP image to change$")
    public void clickingOnEachThumbnailImageCausesTheMainPdpImageToChange() {
        Boolean isChanged = productDetailPage.mainImageIsChangedOnArrowClick("", pdpPreviousBtnLocatorByCSS, pdpNextBtnLocatorByCSS, pdpGetmainImageIndexByCSS, pdpThumbImageIndex);
        softAssertions.assertTrue(isChanged,
                "Failure. Clicking on each thumbnail image causes the main PDP image to change. Actual result: "
                        + isChanged + " -- Expected result: true");
        softAssertions.assertAll();
    }

    @And("^clicking on the '(.*)' arrow causes the main PDP image to change$")
    public void iClickOnTheLeftArrow(String arrow) {
        Boolean isChanged = productDetailPage.mainImageIsChangedOnArrowClick("", pdpPreviousBtnLocatorByCSS, pdpNextBtnLocatorByCSS, pdpGetmainImageIndexByCSS, pdpThumbImageIndex);
        softAssertions.assertTrue(isChanged,
                "Failure. Clicking on the " + arrow + "arrow causes the main PDP image to change. Actual result: "
                        + isChanged + " -- Expected result: true");
        softAssertions.assertAll();
    }

    @When("^I select VIEW LARGE THUMBNAILS button$")
    public void iSelectViewLargeThumbnailsButton() {
        commonFunctions.clickElementByCSS(pdpViewLargeThumbnailsLinkLocatorByCSS);
    }

    @Then("^I should see all big images of the product displayed in a single column$")
    public void iShouldSeeAllBigImagesOfTheProductDisplayedInASingleColumn() {
        Boolean columnIsDisplayed = productDetailPage.largeThumbnailsContainerIsDisplayed(pdplargeThumbnailsContainerLocaotrByCSS, pdpActiveLargeThumbnailLocatorByCSS);
        softAssertions.assertTrue(columnIsDisplayed, "Failure. The big images of the product are displayed in a single column. Actual result: "
                + columnIsDisplayed + " -- Expected result: true");
        softAssertions.assertAll();
    }

    @And("^I select VIEW SMALL THUMBNAILS button$")
    public void iSelectViewSmallThumbnailsButton() {
        commonFunctions.clickElementByCSS(pdpViewSmallThumbnailsLinkLocatorByCSS);
    }

    @Then("^I should see only one big image of the first thumbnail displayed$")
    public void iShouldSeeOnlyOneBigImageOfTheFirstThumbnailDisplayed() {
        Boolean oneBigImgDisplayed = productDetailPage.largeImageIsDisplayed(pdpActiveMainImageDivLocatorByCSS);
        softAssertions.assertTrue(oneBigImgDisplayed, "Failure. I should see only one big image of the first thumbnail displayed. Actual result: "
                + oneBigImgDisplayed + " -- Expected result: true");
        softAssertions.assertAll();
    }

    @When("^I click on the main PDP image$")
    public void iClickOnTheMainPdpImage() {
        commonFunctions.clickElementByCSS(pdpActiveMainImageDivLocatorByCSS);
    }

    @Then("^the main PDP image zooms in$")
    public void theImageZoomsIn() {
        Boolean isZoomed = productDetailPage.checkIfMainImageIsZoomedIn(pdpActiveMainImageDivLocatorByCSS);
        softAssertions.assertTrue(isZoomed, "Failure. The image zooms in. " +
                "Actual result: " + isZoomed + " -- Expected result: true");
        softAssertions.assertAll();
    }

    @And("^moving the cursor away from the image causes the image to return to normal$")
    public void movingTheCursorAwayFromTheImageCausesTheImageToReturnToNormal() {
        Boolean isNormal = productDetailPage.checkIfImageReturnsToNormal(pdpGridContainerLocatorByCSS, pdpActiveMainImageListLocatorByCSS);
        softAssertions.assertTrue(isNormal,
                "Failure. Moving the cursor away from the image causes it to return to normal. Actual result: "
                        + isNormal + " -- Expected result: true");
        softAssertions.assertAll();
    }

    @Then("^I should see the main PDP image in full screen$")
    public void iShouldSeeTheMainPdpImageInFullScreen() {
        Boolean isFullscreen = productDetailPage.checkIfFullscreenIsActive(fullscreenContainerLocator);
        softAssertions.assertTrue(isFullscreen, "Failure. I should see the main PDP image in full screen. Actual result: " + isFullscreen
                + " -- Expected result: true");
        softAssertions.assertAll();
    }

    @And("^clicking on each thumbnail causes the main PDP image to change$")
    public void clickingOnEachThumbnailCausesTheMainPdpImageToChange() {
        Boolean isChanged = productDetailPage.mainImageIsChangedOnFullscreenThumbnailClick(pdpFullscreenThumbnailsDivListLocatorByCSS, pdpFullscreenMainImgListLocator);
        softAssertions.assertTrue(isChanged, "Failure. Clicking on each thumbnail causes the main PDP image to change. Actual result: "
                + isChanged + " -- Expected result: true");
        softAssertions.assertAll();
    }

    @And("^with each scrolled image numbers on the bottom of the page should update$")
    public void withEachScrolledImageNumbersOnTheBottomOfThePageShouldUpdate() {
        Boolean isCorrect = productDetailPage.checkIfNumberCorrespondsToFSThumbnail(pdpFullscreenImageNumberLocator, fullscreenThumbnailsListLocator);
        softAssertions.assertTrue(isCorrect, "Failure. The number on the bottom of the page corresponds to the image. Actual result: "
                + isCorrect + " -- Expected result: true");
        softAssertions.assertAll();
    }

    @When("^I click on the PDP 'CLOSE' button$")
    public void iClickOnCloseFullscreenButton() {
        productDetailPage.clickCloseFullScreenButton();
    }

    @Then("^the view should reset to default$")
    public void theViewShouldResetToDefault() {

        Boolean isActive = productDetailPage.checkIfFullscreenIsActive(fullscreenContainerLocator);
        softAssertions.assertFalse(isActive,
                "Failure. The view should reset to default. Actual result: " + isActive + " -- Expected result: true");
        softAssertions.assertAll();
    }

    @Then("^clicking on the fullscreen '(.*)' arrow causes the main PDP image to change$")
    public void clickingOnTheFsArrowCausesTheMainPdpImageToChange(String arrow) {
        Boolean isChanged = productDetailPage.mainImageIsChangedOnFSArrowClick(arrow, pdpFullscreenPrevBtnLocatorByCSS, pdpFullscreenNextBtnLocator, fullscreenThumbnailsDivListLocator, pdpFullscreenMainImgListLocator);
        softAssertions.assertTrue(isChanged,
                "Failure. Clicking on the " + arrow + "arrow causes the main PDP image to change. Actual result: "
                        + isChanged + " -- Expected result: true");
        softAssertions.assertAll();
    }

    @And("^I click on the Returns link on the PDP$")
    public void clickOnReturnsLinkOnThePdp() {
        commonFunctions.clickElementByCSS(pdpReturnsButtonLocatorByCSS);

    }

    @Then("^the Returns overlay should be displayed$")
    public void returnsOverlayIsDisplayed() {
        Boolean isDisplayed = productDetailPage.checkIfOverlayIsDisplayed(pdpRetrunOverlayLocatorByCSS);
        softAssertions.assertTrue(isDisplayed, "Failure. The Returns overlay should be displayed. Actual result: " + isDisplayed
                + " -- Expected: true");
        softAssertions.assertAll();
    }

    @And("^the Returns overlay page title is '(.*)'$")
    public void returnsOverlayPageTitleIs(String title) {
        String actualTitle = commonFunctions.getTextByCssSelector(pdpGetReturnOverlayTitleByCSS);
        softAssertions.assertTrue(actualTitle.trim().toUpperCase().contains(title.toUpperCase()),
                "Failure. The Returns page title is: " + actualTitle + " -- Expected: " + title);
        softAssertions.assertAll();
    }

    @And("^by changing country in the Returns overlay the return shipping charge should be updated$")
    public void changeCountryInReturnsOverlayAndReturnShippingChargeIsUpdated(DataTable dataTable) {
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2)
            for (int i = 1; i < rawTable.size(); i++) {

                String country = rawTable.get(i).get(0);
                String charge = rawTable.get(i).get(1);

                if (country.equals("Etats-Unis")) {
                    country = country.replace("Etats", "\u00C9tats");
                }
                if (country.equals("Republique de Coree")) {
                    country = country.replace("Republique de Coree", "R\u00E9publique de Cor\u00E9e");
                }

                commonFunctions.selectCustomDropdown(pdpCountryDropDownOnReturnOverlayByCSS, country);
                String actualCharge = commonFunctions.getTextByCssSelector(pdpReturnOverlayShippingChargesByCSS).trim();

                softAssertions.assertTrue(actualCharge.equals(charge), "Failure. The return shipping charge should be updated to: " + charge + " -- Actual: "
                        + actualCharge);
                softAssertions.assertAll();
            }
    }

    @And("^I click on the Delivery link on the PDP$")
    public void iClickOnDeliveryLinkOnPdp() {
        commonFunctions.clickElementByCSS(pdpDeliveryButtonLocatorByCSS);

    }

    @Then("^the Delivery overlay should be displayed$")
    public void deliveryOverlayIsDisplayed() {
        Boolean isDisplayed = productDetailPage.checkIfOverlayIsDisplayed(pdpDeliveryOverlayByCSS);
        softAssertions.assertTrue(isDisplayed, "Failure. The Delivery overlay should be displayed. Actual result: " + isDisplayed
                + " -- Expected: true");
        softAssertions.assertAll();
    }

    @And("^the Delivery overlay page title is '(.*)'$")
    public void deliveryOverlayPageTitleIs(String title) {
        String actualTitle = commonFunctions.getTextByCssSelector(pdpDeliverOverlayPageTitleByCSS);
        softAssertions.assertTrue(actualTitle.contains(title),
                "Failure. The Delivery page title is: " + actualTitle + " -- Expected: " + title);
        softAssertions.assertAll();
    }

    @And("^by changing country in the Delivery overlay the delivery shipping charge should be updated$")

    public void changeCountryInDeliveryOverlayAndDeliveryShippingChargeIsUpdated(DataTable dataTable) {

        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2)
            for (int i = 1; i < rawTable.size(); i++) {

                String country = rawTable.get(i).get(0);
                String shipping = rawTable.get(i).get(1);
                String charge = rawTable.get(i).get(2);
                commonFunctions.selectCustomDropdown(pdpCountryDropDownOnReturnOverlayByCSS, country);
                String actualCharge = productDetailPage.getDeliveryShippingCharge(shipping, pdpDeliveryChargeByCSS).replaceAll("[^A-Za-z0-9£]" + "", "");
                softAssertions.assertTrue(actualCharge.equals(charge), "Failure. The Delivery shipping charge for " + country + " should be updated to: " + charge
                        + " -- Actual: " + actualCharge);
                softAssertions.assertAll();
            }
    }

    @When("^I scroll to the bottom of the page$")
    public void iScrollToTheBottomOfThePage() {
        commonFunctions.scrollToPageBottom();
    }

    @Then("^I should be taken to the top of the page$")
    public void waitForPageTop() {
        boolean isDisplayed = commonFunctions.isElementDisplayed(pdpTopPageVerifyByCSS);
        softAssertions.assertTrue(isDisplayed, "Top Page browser position, could not be verified");
        softAssertions.assertAll();
    }

    @Given("^I click on the 'SHOP THE LOOK' button on the PDP$")
    public void iClickOnTheSHOPTHE_LOOKButtonOnThePDP() {
        commonFunctions.getWebDriver().findElement(By.cssSelector(pdpShopTheLookButtonLocatorByCSS)).click();
        commonFunctions.waitForPageToLoad();
    }

    @Given("^the shop look overlay is displayed$")
    public void theShopLookOverlayIsDisplayed() {

        int shopTheLookProductCount = commonFunctions.ElementInListIsDisplayed(pdpShopTheLookDisplayed);
        softAssertions.assertTrue(shopTheLookProductCount >= 1, "Failure: Shop The Look list seems to be empty.");
        softAssertions.assertAll();
    }

    @Given("^clicking on the 'CLOSE' button closes the 'SHOP THE LOOK' overlay$")
    public void verifyShopTheLookCloseButton() {
        commonFunctions.waitForPageToLoad();
        commonFunctions.clickElementByCSS(pdpShopTheLookCloseButtonLocator);
        commonFunctions.waitForPageToLoad();
        boolean found = commonFunctions.isElementDisplayed(pdpShopTheLookButtonLocatorByCSS);
        softAssertions.assertTrue(found, "Shop the look button was not visible.");
        softAssertions.assertAll();
    }

    @Given("^I should be able to scroll to the bottom of the 'SHOP THE LOOK' overlay$")
    public void verifyScrollOnShopTheLookOverlay() {

        productDetailPage.scrollDownTillProductIsVisibleOnOverlay(pdpShopTheLookContainerLocatorByCSS);
        boolean lastElementIsVisibleOnSTLOverlay = commonFunctions.isElementDisplayed(pdpVerifyProductDisplayedOnShopTheLook);
        softAssertions.assertTrue(lastElementIsVisibleOnSTLOverlay, "The scroll on the STL overlay does not work properly");
        softAssertions.assertAll();
    }

    @Given("^I click on the image of the first '(.*)' on the 'SHOP THE LOOK' overlay$")
    public void clickOnFirstProductImageOnSTLOverlay(String text) {
        text = productDetailPage.getRequiredProduct();
        commonFunctions.clickOnSpecificElementByCSS(pdpShopTheLookClickOnProductByCSS, text);
    }

    @Given("^the first 'SHOP THE LOOK' '(.*)' image zooms in$")
    public void verifyImageZoomsInInSTLOverlay(String text) {
        text = productDetailPage.getRequiredProduct();
        String expectedAtributeValue = "opacity: 1;";
        String actualAttributeValue = commonFunctions.getStyleAttributeOfSpecificElement(pdpGetStyleElementByCSS, text);
        softAssertions.assertTrue(actualAttributeValue.contains(expectedAtributeValue), "Image was not zoomed in on click.");
        softAssertions.assertAll();
    }

    @Then("by selecting the size drop down list of product on the 'SHOP THE LOOK' overlay I can select different sizes")
    public void selectSizeDropdownOnOverlay(DataTable dataTable) {
        String text = Constants.INSTOCK;
        pdpSelectSizeForSpecificProductFromShopTheLookByCSS = pdpSelectSizeForSpecificProductFromShopTheLookByCSS.replace("elementtext", text);
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2)
            for (int i = 1; i < rawTable.size(); i++) {
                String size = rawTable.get(i).get(0);

                commonFunctions.selectFromDropdownByCSS(pdpSelectSizeForSpecificProductFromShopTheLookByCSS, size);
            }
    }

    @Then("Quantity drop down list on the 'SHOP THE LOOK' overlay becomes active")
    public void verifySizeDropDownActiveOnShopTheLook() {
        pdpQTYDropDownOnShopTheLookByCSS = pdpQTYDropDownOnShopTheLookByCSS.replace("elementtext", productDetailPage.getRequiredProduct());
        boolean isQtyClickable = commonFunctions.isDropDownEnabledByCSS(pdpQTYDropDownOnShopTheLookByCSS);
        softAssertions.assertTrue(isQtyClickable, "Failure: Qty drop down menu is clickable. ");
        softAssertions.assertAll();
    }

    @Then("by select the quantity drop down list using product on the 'SHOP THE LOOK' overlay I can select up to ten units")
    public void selectQTYDropDownOnShopTheLook(DataTable dataTable) {
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2)
            for (int i = 1; i < rawTable.size(); i++) {

                String quantity = rawTable.get(i).get(0);
                //  String product = rawTable.get(i).get(1);
                String text = Constants.INSTOCK;
                pdpQTYDropDownOnShopTheLookByCSS = pdpQTYDropDownOnShopTheLookByCSS.replace("elementtext", text);
                commonFunctions.selectFromDropdownByCSS(pdpQTYDropDownOnShopTheLookByCSS, quantity);
            }
    }

    @Then("by clicking on the 'QUANTITY' drop down list on the 'SHOP THE LOOK' overlay for product I will not be able to open it")
    public void verifyQuantityDropdownMenuCannotBeOpened() {
        pdpQTYDropDownWithOneSizeProductByCSS = pdpQTYDropDownWithOneSizeProductByCSS.replace("elementtext", productDetailPage.getRequiredProduct());
        boolean found = commonFunctions.isElementDisplayed(pdpQTYDropDownWithOneSizeProductByCSS);
        softAssertions.assertTrue(found, "Quantity menu on PDP was not inactive");
        softAssertions.assertAll();
    }

    @Then("by clicking on the 'SIZE' drop down list on the 'SHOP THE LOOK' overlay and selecting size '(.*)' the 'ADD TO BAG' button should disappear")
    public void verifySTLAddToBagButtonDisappear(String size) {
        String text = productDetailPage.getRequiredProduct();
        pdpSelectSizeForSpecificProductFromShopTheLookByCSS = pdpSelectSizeForSpecificProductFromShopTheLookByCSS.replace("elementtext", text);
        commonFunctions.selectFromDropdownByCSS(pdpSelectSizeForSpecificProductFromShopTheLookByCSS, productDetailsFromJson.retrieveSoldOutSize());

    }

    @Then("on the 'SHOP THE LOOK' overlay should be a text message '(.*)' instead of the 'ADD TO BAG' button")
    public void verifySoldOutMessageOnSTL(String soldOutMessage) {
        pdpShopTheLookOutOfStockMessageByCSS = pdpShopTheLookOutOfStockMessageByCSS.replace("elementtext", productDetailPage.getRequiredProduct());
        String actualSoldOutMessage = commonFunctions.getTextByCssSelector(pdpShopTheLookOutOfStockMessageByCSS);
        softAssertions.assertTrue(soldOutMessage.contentEquals(actualSoldOutMessage),
                "The sold out message is not visible. Expected: " + soldOutMessage + " Actual: " + actualSoldOutMessage);
        softAssertions.assertAll();

    }

    @Then("by clicking on the 'SIZE' drop down list on the 'SHOP THE LOOK' overlay for the product and selecting size '(.*)' I will not be able to open 'QUANTITY' drop down list")
    public void selectSizesOnSTL(String size) {
        String text = Constants.PRODUCT_ONEQTYONLY;
        pdpSelectSizeForSpecificProductFromShopTheLookByCSS = pdpSelectSizeForSpecificProductFromShopTheLookByCSS.replace("elementtext", text);
        commonFunctions.selectFromDropdownByCSS(pdpSelectSizeForSpecificProductFromShopTheLookByCSS, size);
        pdpQTYDropDownWithOneSizeProductByCSS = pdpQTYDropDownWithOneSizeProductByCSS.replace("elementtext", Constants.PRODUCT_ONEQTYONLY);
        boolean isQtyNotClickable = commonFunctions.isElementDisplayed(pdpQTYDropDownWithOneSizeProductByCSS);
        softAssertions.assertTrue(isQtyNotClickable, "Failure: Qty drop down menu is clickable. ");
        softAssertions.assertAll();
    }

    @Then("I should be able to see the text '(.*)' in the size drop down list on the 'SHOP THE LOOK' overlay")
    public void verifySizeTextMenu(String sizeTextMenu) {


    }

    @Given("I add the product size '(.*)' and '(.*)' to my bag from the 'SHOP THE LOOK' overlay")
    public void changeSizeAndQty(String size, String qty) {
        String text= "";
         productSize = "";

        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            text = Constants.INSTOCK;
        }else
        text = product.get(0);
        pdpSelectSizeForSpecificProductFromShopTheLookByCSS = pdpSelectSizeForSpecificProductFromShopTheLookByCSS.replace("elementtext", text);
        pdpQTYDropDownOnShopTheLookByCSS = pdpQTYDropDownOnShopTheLookByCSS.replace("elementtext", text);
        pdpShopTheLookAddToBagByCSS = pdpShopTheLookAddToBagByCSS.replace("elementtext", text);

        if(!productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            productSize = productDetailsFromJson.retrieveAvailableSizes().get(0);
        }else
            productSize = size;
        commonFunctions.waitForPageToLoad();
        commonFunctions.selectFromDropdownByCSS(pdpSelectSizeForSpecificProductFromShopTheLookByCSS, productSize);
        commonFunctions.waitForPageToLoad();
        commonFunctions.selectFromDropdownByCSS(pdpQTYDropDownOnShopTheLookByCSS, qty);
        commonFunctions.waitForPageToLoad();
        commonFunctions.clickElementByCSS(pdpShopTheLookAddToBagByCSS);
    }

    @Given("I add the product size '(.*)' from the 'SHOP THE LOOK' overlay")
    public void changeSize(String size) {
        String text= "";
        productSize = "";

        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            text = Constants.INSTOCK;
        }else
            text = product.get(0);
        pdpSelectSizeForSpecificProductFromShopTheLookByCSS = pdpSelectSizeForSpecificProductFromShopTheLookByCSS.replace("elementtext", text);
        pdpQTYDropDownOnShopTheLookByCSS = pdpQTYDropDownOnShopTheLookByCSS.replace("elementtext", text);
        pdpShopTheLookAddToBagByCSS = pdpShopTheLookAddToBagByCSS.replace("elementtext", text);

        if(!productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            productSize = productDetailsFromJson.retrieveAvailableProductDetails().get("size");
        }
            commonFunctions.waitForPageToLoad();
        commonFunctions.selectFromDropdownByCSS(pdpSelectSizeForSpecificProductFromShopTheLookByCSS, productSize);
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForPageToLoad();
    }

    @Then("^the mini bag rollover list appears$")
    public void theMiniShoppingBagRolloverListAppears() {
        Boolean isDisplayed = commonFunctions.isElementDisplayed(minicartLocator);
        softAssertions.assertTrue(isDisplayed, "Failure. The mini bag rollover list do not appear. Actual result: " + isDisplayed
                + " -- Expected: true");
        softAssertions.assertAll();
    }

    @Then("^I should see the 'RECENTLY VIEWED' section with title '(.*)' on the PDP$")
    public void verifyRecentlyViewedTitle(String title) {
        String actualTitle = commonFunctions.getTextByCssSelector(pdpRecentlyViewedTabByCSS);
        softAssertions.assertTrue(title.contentEquals(actualTitle.toUpperCase()), "Recently viewed " +
                "title is not the one expected. Expected: " + title + " Actual: " + actualTitle);
        softAssertions.assertAll();
    }

    @Given("^I should see the 'YOUR MATCHES' section with title '(.*)' on the PDP")
    public void verifyYourMatchesTitle(String title) {
        commonFunctions.scrollToElement(pdpYourMatchesTabByCSS);
        String actualTitle = commonFunctions.getTextByCssSelector(pdpYourMatchesTabByCSS);
        softAssertions.assertTrue(title.contentEquals(actualTitle.toUpperCase()),
                "Your Matches title is not the one expected. Expected: " + title + " Actual: " + actualTitle);
        softAssertions.assertAll();
    }

    @Given("I should see sub block titles '(.*)' and '(.*)' beneath the block title '(.*)' in the 'YOUR MATCHES' section of the PDP")
    public void verifyYourMatchesTitle(String subTitle1, String subtTitle2, String title) {
        String actualTitle1 = commonFunctions.getTextByCssSelector(pdpMatchWithItTabByCSS);
        softAssertions.assertTrue(subTitle1.trim().equalsIgnoreCase(actualTitle1.trim()),
                "Match it with title is not the one expected. Expected: " + subTitle1 + " Actual: " + actualTitle1);
        String pdpRecentlyViewedTabByCSS = obj_Property.readProperty("pdpRecentlyViewedTabByCSS");

        String actualTitle2 = commonFunctions.getTextByCssSelector(pdpRecentlyViewedTabByCSS);
        softAssertions.assertTrue(subtTitle2.trim().equalsIgnoreCase(actualTitle2.trim()),
                "Match it with title is not the one expected. Expected: " + subtTitle2 + " Actual: " + actualTitle2);
        String pdpYourMatchesTabByCSS = obj_Property.readProperty("pdpYourMatchesTabByCSS");

        String actualTitle3 = commonFunctions.getTextByCssSelector(pdpYourMatchesTabByCSS);
        softAssertions.assertTrue(title.trim().equalsIgnoreCase(actualTitle3.trim()),
                "Match it with title is not the one expected. Expected: " + title + " Actual: " + actualTitle3);
        softAssertions.assertAll();
    }

    @Given("^I click on the 'RECENTLY VIEWED' tab in the 'YOUR MATCHES' section")
    public void clickOnRecentlyViewedTab() {
        commonFunctions.scrollToElement(pdpRecentlyViewedTabByCSS);
        //commonFunctions.scrollScreen500Pixel();
        commonFunctions.clickElementByCSS(pdpRecentlyViewedTabByCSS);

    }

    @Given("^I should see my previously viewed product '(.*)' displayed in the 'RECENTLY VIEWED' tab")
    public void verifyProductRecentlyViewedIsVisible(String productDescription) {
        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()) {
            String actualProductDescription = commonFunctions.getTextByCssSelector(pdpReentlyViewedProductsTextByCSS);
            softAssertions.assertTrue(productDescription.contentEquals(actualProductDescription),
                    "Product description from pdp doesn't match with the one in recently viewed section. Expected: "
                            + productDescription + " Actual: " + actualProductDescription);
            softAssertions.assertAll();
        }else{
            String actualProductDescription = commonFunctions.getTextByCssSelector(pdpReentlyViewedProductsTextByCSS);
            softAssertions.assertTrue((productDesc_instock_ThirdProduct.contentEquals(actualProductDescription)||(actualProductDescription.contentEquals(actualProductDescription))),
                    "Product description from pdp doesn't match with the one in recently viewed section. Expected: "
                            + productDesc_instock_ThirdProduct + " Actual: " + actualProductDescription);
            softAssertions.assertAll();

        }
    }

    @Given("^I click the '(.*)' product link in the 'RECENTLY VIEWED' tab of the PDP")
    public void clickOnProductBasedOnDescriptionInRecentlyViewed(String productDescription) {
        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()) {
            commonFunctions.waitForElementByCssLocator(pdpProductsListOnRecentlyViewedTabByCSS);
            commonFunctions.clickOnElementInList(pdpProductsListOnRecentlyViewedTabByCSS, productDescription);
        }else{

            commonFunctions.waitForElementByCssLocator(pdpProductsListOnRecentlyViewedTabByCSS);
            String actualProductDescription = commonFunctions.getTextByCssSelector(pdpReentlyViewedProductsTextByCSS);
            commonFunctions.clickOnElementInList(pdpProductsListOnRecentlyViewedTabByCSS, actualProductDescription);
        }
    }

    @Given("^I am taken to the correct page '(.*)'")
    public void verifyUrl(String url) throws UnsupportedEncodingException {

        // String actualUrl = URLDecoder.decode(commonFunctions.getCurrentURL(), "UTF-8");
        String actualUrl = commonFunctions.getCurrentURL();
        softAssertions.assertTrue((actualUrl.trim().replaceAll(".*matchesfashion.com", "").contains(product.get(0).trim())||(actualUrl.trim().replaceAll(".*matchesfashion.com", "").contains(product.get(2).trim()))),
                "Url is not the correct one. Expected: " + product.get(0) + " Actual: "
                        + actualUrl.trim().replaceAll(".*matchesfashion.com", ""));
        softAssertions.assertAll();
    }

    @Given("^I click on the 'MATCH IT WITH' tab in the 'YOUR MATCHES' section")
    public void clickOnMatchItWithTab() {
        commonFunctions.scrollToElement(pdpMatchITWithTabByCSS);
        commonFunctions.clickElementByCSS(pdpMatchITWithTabByCSS);

    }

    @Given("^I click the '(.*)' product link in the 'MATCH IT WITH' tab of the PDP")
    public void clickOnProductBasedOnDescriptionInMatchItWith(String productDescription) {
        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()) {
            commonFunctions.clickOnElementInList(pdpProductsListOnMatchITWithByCSS, productDescription);
        }else{
            String actualProductDescription = commonFunctions.getTextByCssSelector(pdpReentlyViewedProductsTextByCSS);
            commonFunctions.clickOnElementInList(pdpProductsListOnMatchITWithByCSS, actualProductDescription);
        }
    }

    @Given("^the first product in the 'MATCH IT WITH' tab of the 'YOUR MATCHES' section is '(.*)'")
    public void verifyFirstProductDescriptionOnMatchItWith(String productDescription) {
        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()) {
            String actualProductDescription = commonFunctions.getTextByCssSelector(pdpGetTextOfFirstProductFromMatchITWithByCSS);
            softAssertions.assertTrue(productDescription.contentEquals(actualProductDescription),
                    "Product description from pdp doesn't match with the one in recently viewed section. Expected: "
                            + productDescription + " Actual: " + actualProductDescription);
            softAssertions.assertAll();
        }else{
            String actualProductDescription = commonFunctions.getTextByCssSelector(pdpGetTextOfFirstProductFromMatchITWithByCSS);
            softAssertions.assertTrue(productDesc_Instock_SecondProduct.equalsIgnoreCase(actualProductDescription),
                    "Product description from pdp doesn't match with the one in recently viewed section. Expected: "
                            + productDesc_Instock_SecondProduct + " Actual: " + actualProductDescription);
            softAssertions.assertAll();
        }

    }

    @Then("'DESCRIPTION' tab of the Product Details section of the PDP contains '(.*)'")
    public void verifyProductDescriptionInPDP(String expectedDescription) {
        String actualProductDescription = commonFunctions.getTextByCssSelector(pdpGetProductDescriptionByCSS);
        softAssertions.assertTrue(actualProductDescription.contentEquals(actualProductDescription),
                "Product description from pdp doesn't match with the one in recently viewed section. Expected: "
                        + expectedDescription + " Actual: " + actualProductDescription);
        softAssertions.assertAll();

    }

    @Then("^each social icon on PDP opens new window with the correct URL and item description in query string$")
    public void eachSocialIconOpensNewWindowWithCorrectUrlAndQueryString(DataTable dataTable) {
        CustomVerification verification = new CustomVerification();
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2) {
            for (int i = 1; i < rawTable.size(); i++) {

                List<String> row = rawTable.get(i);
                String icon = row.get(0);
                String url = row.get(1);
                // String query = row.get(2);
                String entireUrl = productDetailPage.getNewWindowUrl(icon, pdpSocilaLinkByCSS);
                // String actualDomain = entireUrl.split("\\?")[0];
                verification.verifyTrue("Failure. Icon " + icon + " opens window with correct URL. Actual url: " + entireUrl + " -- Should contain: " + url,
                        entireUrl.contains(url));
                if(product.get(0).isEmpty()){
                    verification.verifyTrue("Failure. Icon " + icon + " opens window with correct query string. Actual url: " + entireUrl + " -- Should contain: " + Constants.INSTOCK, entireUrl.contains(Constants.INSTOCK));
                }else{
                    verification.verifyTrue("Failure. Icon " + icon + " opens window with correct query string. Actual url: " + entireUrl + " -- Should contain: " + product.get(0), entireUrl.contains(product.get(0)));
                }

            }
        }
        verification.verifyNoErrors();
    }

    @When("^I click on the 'Email a friend' icon on PDP$")
    public void iClickOnEmailAFriendOnPdp() {
        commonFunctions.clickElementByCSS(pdpEmailAFriendByCSS);

    }

    @Then("^the 'TELL A FRIEND' overlay is displayed with page title '(.*)'$")
    public void theTellAFriendOverlayIsDisplayedWithCorrectTitle(String title) {
        Boolean isDisplayed = commonFunctions.isElementDisplayed(pdpShareByEmailOverlayByCSS);
        softAssertions.assertTrue(isDisplayed,
                "Failure. The 'Tell a friend' overlay is displayed. Actual: " + isDisplayed + " -- Expected: true");
        String pdpOverlayTitleByCSS = obj_Property.readProperty("pdpOverlayTitleByCSS");
        String actualTitle = commonFunctions.getTextByCssSelector(pdpOverlayTitleByCSS);
        softAssertions.assertTrue(title.equalsIgnoreCase(actualTitle),
                "Failure. The 'Tell a friend' overlay actual title is: " + actualTitle + " -- Expected: " + title);
        softAssertions.assertAll();
    }

    @And("^clicking on the close button closes the 'TELL A FRIEND' overlay$")
    public void clickingCloseButtonClosesOverlay() {
        commonFunctions.clickElementByCSS(pdpShareByEmailCloseOverlayByCSS);
        Boolean isDisplayed = commonFunctions.isElementDisplayed(pdpShareByEmailOverlayByCSS);
        softAssertions.assertFalse(isDisplayed,
                "Failure. The 'Tell a friend' overlay is displayed. Actual: false -- Expected: " + isDisplayed);
        softAssertions.assertAll();
    }

    @And("^I click on the 'Size Guide' link next to the size dropdown$")
    public void iClickOnSizeGuideLink() {
        commonFunctions.clickElementByCSS(pdpSizeGuideLinkLocatorByCSS);

    }

    @And("^the 'SIZE AND FIT GUIDE' overlay is displayed$")
    public void sizeAndFitOverlayIsDisplayed() {
        commonFunctions.waitForElementByCssLocator(pdpSizeAndFitGuideOverlayLocatorByCSS);
        Boolean isDisplayed = commonFunctions.isElementDisplayed(pdpSizeAndFitGuideOverlayLocatorByCSS);
        softAssertions.assertTrue(isDisplayed, "Failure. The 'SIZE AND FIT GUIDE' overlay is displayed." +
                "Actual result: " + isDisplayed + " -- Expected: true");
        softAssertions.assertAll();
    }

    @And("^clicking on the 'CLOSE' button closes the 'SIZE AND FIT GUIDE' overlay$")
    public void clickingOnCloseButtonClosesSizeAndFitOverlay() {
        commonFunctions.clickElementByCSS(pdpSizeGuidecloseButtonLocatorByCSS);
        Boolean isDisplayed = commonFunctions.isElementDisplayed(pdpSizeAndFitGuideOverlayLocatorByCSS);
        softAssertions.assertFalse(isDisplayed, "Failure. The 'SIZE AND FIT GUIDE' overlay is closed." +
                "Actual result: false. -- Expected: " + isDisplayed);
        softAssertions.assertAll();
    }

    @And("^I click on the 'Size Guide' link in the 'SIZE AND FIT' tab$")
    public void clickOnSizeGuideInSizeAndFitTab() {
        iExpandTheSizeAndFitTab();
        commonFunctions.clickElementByCSS(pdpSizeGuideButtonLocatorInAccordianByCSS);
    }

    @And("^the 'SIZE' field in the Product Details section of the 'SIZE AND FIT GUIDE' overlay has size '(.*)' selected$")
    public void theCorrectSizeFieldInTheProductDetailsSizeAndFitGuideIsSelected(String size) {
        String expectedSize="";
        String actualSize = commonFunctions.retrieveTextWithNoEscapeChars(pdpGetSelectedValueFromSizeGuideDropDown);
        if(product.get(0).isEmpty()){
            expectedSize = size;
        }else{
            expectedSize = productDetailsFromJson.retrieveAvailableProductDetails().get("size");
        }
        softAssertions.assertTrue(actualSize.equalsIgnoreCase(expectedSize), "Failure." +
                "The actual size is: " + actualSize + " -- Expected: " + expectedSize);
        softAssertions.assertAll();
    }

    @And("^the 'SIZE' field in the 'MEASURING GUIDE' section of the 'SIZE AND FIT GUIDE' overlay has size '(.*)' selected$")
    public void theCorrectSizeFieldInTheMeasuringGuideSizeAndFitGuideIsSelected(String size) {
        String expectedSize="";
        String actualSize = commonFunctions.retrieveTextWithNoEscapeChars(pdpGetSelectedValueFromMeasuringGuideDD);
        if(product.get(1).isEmpty()){
            expectedSize = size;
        }else{
            expectedSize = productDetailsFromJson.retrieveAvailableProductDetails().get("size");
        }
        softAssertions.assertTrue(actualSize.equalsIgnoreCase(expectedSize), "Failure. " +
                "The actual size is: " + actualSize + " -- Expected: " + expectedSize);
        softAssertions.assertAll();
    }

    @And("^I click on the 'Colour Options' thumbnail image$")
    public void iClickOnColourOptionsThumbnail() {
        productDetailPage.clickOnFirstColourThumbnail(pdpClickOnFirstColourThumbnailsListLocatorByCSS);
    }

    @And("^the link on the thumbnail image should take me to the correct URL '(.*)'$")
    public void theThumbnailLinkTakesMeToCorrectUrl(String url) {
        String actualUrl = commonFunctions.getCurrentURL();
        softAssertions.assertTrue(actualUrl.contains(url), "Failure." +
                "The link on the thumbnail image should take me to the correct URL: " + url
                + " -- Actual url: " + actualUrl);
        softAssertions.assertAll();
    }

    @And("^clicking on each PDP breadcrumb link should take me to the correct page$")
    public void clickingOnEachPdpBreadcrumbLinkTakesMeToTheCorrectPage(DataTable dataTable) {
        CustomVerification verification = new CustomVerification();
        String actualUrl="";
        String expectedUrl="";
        int breadCrumbs;
        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            List<List<String>> rawTable = dataTable.asLists();
            if (rawTable.size() >= 2) {
                for (int i = 1; i < rawTable.size(); i++) {
                    List<String> row = rawTable.get(i);
                    String pdp = row.get(0);
                    String bread = row.get(1);
                    String url = row.get(2);
                    if (navigateUsingURL.getrelativeURLJenkins() == null) {
                        commonFunctions.navigateTo(Constants.BASE_URL + pdp);
                    } else {
                        String relativeURLJenkins = navigateUsingURL.getrelativeURLJenkins();
                        commonFunctions.navigateTo(relativeURLJenkins + pdp);
                    }
                    if (commonFunctions.getCurrentURL().contains("/fr/")) {
                        bread = bread.replace("Womens", "Femme");
                        bread = bread.replace("Designer", "Cr\u00E9ateur");
                    }
                    productDetailPage.clickOnBreadcrumb(bread, pdpBreadCrumbItemListLocatorByCSS);
                    commonFunctions.waitForPageToLoad();
                    actualUrl = commonFunctions.getCurrentURL();
                    verification
                            .verifyTrue("Failure. Clicking on each PDP breadcrumb link should take me to the correct page: "
                                    + url + " -- Actual page: " + actualUrl, actualUrl.contains(url));
                }
                verification.verifyNoErrors();
            }
        }else{
            String designerName = "";
            String productID = "" ;
            productID = productDetailsFromJson.getInStockProductIdWithAvailableVarients().get(0);
            if (navigateUsingURL.getrelativeURLJenkins() == null) {
                commonFunctions.navigateTo(Constants.BASE_URL + "/p/"+productID);
            } else {
                String relativeURLJenkins = navigateUsingURL.getrelativeURLJenkins();
                commonFunctions.navigateTo(relativeURLJenkins + "/p/"+productID);
            }
            breadCrumbs = commonFunctions.waitForListElementByCssLocator(pdpBreadCrumbs).size()-1;
            for(int index=0 ; index < breadCrumbs ; index++) {
                List<WebElement> breadCrumbList = commonFunctions.waitForListElementByCssLocator(pdpBreadCrumbs);
                commonFunctions.waitForPageToLoad();
                if(index == 0){
                    expectedUrl = breadCrumbList.get(0).getAttribute("href");
                }else if(index == 1){
                    expectedUrl = breadCrumbList.get(1).getAttribute("href");
                }else{
                    expectedUrl = breadCrumbList.get(2).getAttribute("href");
                }
                commonFunctions.waitForPageToLoad();
                commonFunctions.clickWebElement(breadCrumbList.get(index));
                commonFunctions.waitForPageToLoad(); // for IE and Edge
                actualUrl = commonFunctions.getCurrentURL();
                verification.verifyTrue("Failure. Clicking on each PDP breadcrumb link should take me to the correct page. Expected "
                        + expectedUrl + " -- Actual page: " + actualUrl, actualUrl.equalsIgnoreCase(expectedUrl));
                if (navigateUsingURL.getrelativeURLJenkins() == null) {
                    commonFunctions.navigateTo(Constants.BASE_URL + "/p/"+productID);
                } else {
                    String relativeURLJenkins = navigateUsingURL.getrelativeURLJenkins();
                    commonFunctions.navigateTo(relativeURLJenkins+ "/p/"+productID);
                }
            }
        }
    }

    @And("^clicking the first 'VIEW ALL' link should take me to the correct page '(.*)'$")
    public void clickingTheFirstViewAllLinkTakesMeToCorrectPage(String url) {
        String expectedURL = "";
        String designerName="";
        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            expectedURL = url;
        }else{
            expectedURL = commonFunctions.getWebElement(pdpClickOnViewAllDesignerLinkLocator).getAttribute("href");
        }
        commonFunctions.clickElementByCSS(pdpClickOnViewAllDesignerLinkLocator);
        commonFunctions.waitForPageToLoad();
        commonFunctions.waitForPageToLoad();
        String actualUrl = commonFunctions.getCurrentURL();
        softAssertions.assertTrue(actualUrl.equalsIgnoreCase(expectedURL), "Failure. Clicking the first 'VIEW ALL' link should take me to the correct page: " + expectedURL
                + " -- Actual: " + actualUrl);
        softAssertions.assertAll();
    }

    @And("^clicking the second 'VIEW ALL' link should take me to the correct page '(.*)'$")
    public void clickingTheSecondViewAllLinkTakesMeToCorrectPage(String url) {
        String expectedUrl="";
        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            expectedUrl = url;
        }else{
            expectedUrl = commonFunctions.getWebElement(pdpClickOnViewAllTopsLinkLocatorByCSS).getAttribute("href");
        }
        commonFunctions.clickElementByCSS(pdpClickOnViewAllTopsLinkLocatorByCSS);
        commonFunctions.waitForPageToLoad();
        String actualUrl = commonFunctions.getCurrentURL();
        softAssertions.assertTrue(actualUrl.contains(expectedUrl), "Failure. Clicking the first 'VIEW ALL' link should take me to the correct page: " + expectedUrl
                + " -- Actual: " + actualUrl);
        softAssertions.assertAll();
    }

    @When("^I add products to my cart$")
    public void addProductsToShoppingBagViaUrl() {
        String currentURL = commonFunctions.getCurrentURL();
        String baseURL = null;
        if (navigateUsingURL.getrelativeURLJenkins() == null) {
            baseURL=Constants.BASE_URL;
        } else {
            baseURL = navigateUsingURL.getrelativeURLJenkins();
        }
        if(productDetailsFromJson.getInStockProductSKUCode().isEmpty()) {
            String product = Constants.INSTOCK + "000003";
            commonFunctions.navigateTo(baseURL + "/at/cart/add/" + product + "/" + "1");
        }else{
          productcodewithVarientCode= productDetailsFromJson.getInStockProductSKUCode();
          commonFunctions.navigateTo(baseURL + "/at/cart/add/" + productcodewithVarientCode.get(0) + "/" + "1");
            if(commonFunctions.isSuccessStatus(Constants.API_FAILED_MESSAGE)){
                for(String productCode : productcodewithVarientCode){
                    commonFunctions.navigateTo(baseURL + "/at/cart/add/" + productCode + "/" + "1");
                    if(commonFunctions.isSuccessStatus(Constants.API_SUCCESS_MESSAGE)) {
                        break;
                    }
                }
            }
        }
        softAssertions.assertTrue(commonFunctions.isSuccessStatus(Constants.API_SUCCESS_MESSAGE), "Item " + product + " could not be added to cart; out of stock?");
        softAssertions.assertAll();
        commonFunctions.navigateTo(currentURL);
    }

    @When("^I click on the 'ADD TO WISHLIST' button on the PDP$")
    public void iClickOnTheAddToWishlistButtonOnThePDP() {
       commonFunctions.waitForPageToLoad();
        commonFunctions.clickElementByCSS(addToWishListOnPDPByCSS);
    }

    @Then("^the 'ADD TO WISHLIST' button text changes to '(.*)'$")
    public void verifyAddedTOWishListButton(String first_change) {
        String addToWishListOnPDP = commonFunctions.getTextByCssSelector(addToWishListOnPDPByCSS);
        softAssertions.assertTrue(addToWishListOnPDP.toLowerCase().trim().contains(first_change.toLowerCase().trim()),
                "The button " + addToWishListOnPDP + " is not changed from Add to Wishlist" + "Expected: " + first_change
                        + " Actual: " + addToWishListOnPDP);
        softAssertions.assertAll();


    }

    @Then("^the mini Wishlist appears$")
    public void verifyMiniWishlistRolloverIsDisplayed() {
        Boolean isDisplayed = commonFunctions.isElementDisplayed(wishlistMiniRollOverByCSS);
        softAssertions.assertFalse(isDisplayed,
                "Failure. The 'Mini Wishlist' should be displayed. Actual result: false -- Expected: " + isDisplayed);
        softAssertions.assertAll();
    }

    @Then("^the mini Wishlist rollover list disappears$")
    public void verifyMiniWishlistRolloverDisappears() {
        Boolean isDisplayed = commonFunctions.isElementDisplayed(wishlistMiniRollOverByCSS);
        softAssertions.assertFalse(isDisplayed, "Failure. The 'Mini Wishlist' should not be displayed. " +
                "Actual result: false -- Expected: " + isDisplayed);
        softAssertions.assertAll();
    }

    @When("^the button 'ADD TO WISHLIST' changes to 'GO TO WISHLIST' '(.*)'$")
    public void verifyGoToWishlistButtonText(String expectedButtonText) {
        String goToWishlistOnPDP = commonFunctions.getTextGoToWishList(goToWishlistOnPDPByCSS);
        softAssertions.assertTrue(goToWishlistOnPDP.toLowerCase().trim().equalsIgnoreCase(expectedButtonText.toLowerCase().trim()),
                "The button " + goToWishlistOnPDP + " is not changed from Add to Wishlist" + "Expected: " + expectedButtonText
                        + " Actual: " + goToWishlistOnPDP);
        softAssertions.assertAll();
    }

    @When("^I click on the 'ADD TO WISHLIST' button on the Shop The Look overlay$")
    public void iClickOnTheADDTOWISHLISTButtonOnTheShopTheLookOverlay() {
        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            productId = Constants.INSTOCK;
        }else {
             productId = product.get(0);
        }
        shopTheLookAddToWishlistByCSS = shopTheLookAddToWishlistByCSS.replace("elementtext", productId);
        commonFunctions.clickElementByCSS(shopTheLookAddToWishlistByCSS);
    }

    @When("the first change '(.*)' happens to Shop The look 'ADD TO WISHLIST' text button for '(.*)'")
    public void verifyAddedToWishlistButtonText(String expectedButtonText, String text) {
        text = Constants.INSTOCK;


        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            shopTheLookGetTextAddToWishlist = shopTheLookGetTextAddToWishlist.replace("elementtext", text);
        }else {
            productId = product.get(0);
            shopTheLookGetTextAddToWishlist = shopTheLookGetTextAddToWishlist.replace("elementtext", productId);
        }


        String actualTextAddToWishlist = commonFunctions.getTextByCssSelector(shopTheLookGetTextAddToWishlist);
        softAssertions.assertTrue(actualTextAddToWishlist.toLowerCase().trim().equalsIgnoreCase(expectedButtonText.toLowerCase().trim()),
                "The button " + actualTextAddToWishlist + " is not changed from Add to Wishlist" + "Expected: " + expectedButtonText
                        + " Actual: " + actualTextAddToWishlist);
        softAssertions.assertAll();

    }

    @When("the second change '(.*)' happens to Shop The look 'ADD TO WISHLIST' text button for '(.*)'")
    public void verifyGoToWishlistButtonText(String expectedButtonText, String text) {
        text = Constants.INSTOCK;

        if(productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()){
            shopTheLookGoToWishlistByCSS = shopTheLookGoToWishlistByCSS.replace("elementtext", text);
        }else {
            productId = product.get(0);
            shopTheLookGoToWishlistByCSS = shopTheLookGoToWishlistByCSS.replace("elementtext", productId);
        }


        String actualTextGoToWishlist = commonFunctions.getTextByCssSelector(shopTheLookGoToWishlistByCSS);
        softAssertions.assertTrue(actualTextGoToWishlist.toLowerCase().trim().equalsIgnoreCase(expectedButtonText.toLowerCase().trim()),
                "The button " + actualTextGoToWishlist + " is not changed from Add to Wishlist" + "Expected: " + expectedButtonText
                        + " Actual: " + actualTextGoToWishlist);
        softAssertions.assertAll();
    }

    @Given("^I select size '(.*)' from the sticky nav wrapper size drop down$")
    public void iSelectSizeFromTheStickyNavWrapperSizeDropDown(String size) {
        if(!productDetailsFromJson.getInStockProductIdWithAvailableVarients().isEmpty()) {
                    productSize = productDetailsFromJson.retrieveAvailableSizes().get(0);
        }else
            productSize = size;
            commonFunctions.selectFromDropdownByCSS(pdp_SizeDropDown_CSS, productSize);
    }

    @When("^I click on the 'ADD TO WISHLIST' button on the PDP sticky nav wrapper$")
    public void iClickOnTheADDTOWISHLISTButtonOnThePDPStickyNavWrapper() {
        commonFunctions.clickOnElement(commonFunctions.getWebElement(stickyNavAddToWishListByCSS));
    }

    @When("^the 'ADD TO WISHLIST' sticky nav button text changes to '(.*)'$")
    public void verifyAddedToWishlistButtonText(String expectedButtonText) {
        String stickyNavAddToWishlistActualText = commonFunctions.getTextByCssSelector(stickyNavAddToWishListByCSS);
        softAssertions.assertTrue(stickyNavAddToWishlistActualText.toLowerCase().trim().equalsIgnoreCase(expectedButtonText.toLowerCase().trim()),
                "The button " + stickyNavAddToWishlistActualText + " is not changed from Add to Wishlist" + "Expected: " + expectedButtonText
                        + " Actual: " + stickyNavAddToWishlistActualText);
        softAssertions.assertAll();
    }

    @When("^the sticky nav button 'ADD TO WISHLIST' changes to 'GO TO WISHLIST' '(.*)'$")
    public void verifyGoToWishlistStickyNavButtonText(String expectedButtonText) {

        //productDetailsSteps.verifyGoToWishlistButtonText(expectedButtonText);
    }

    @And("I click on the 'ADD TO WISHLIST' button on the full screen product overlay")
    public void clickAddToWishlistButtonFromFullScreedPDP() {
        commonFunctions.clickElementByCSS(fullscreenAddToWishListByCSS);
    }

    @When("^the first full screen product overlay 'ADD TO WISHLIST' button text changes to '(.*)'$")
    public void verifyFullScreenAddedToWishlistButtonText(String expectedButtonText) {
        String fullscreenAddedToWishlistActualText = commonFunctions.getTextByCssSelector(fullscreenAddTowishlist);
        softAssertions.assertTrue(fullscreenAddedToWishlistActualText.toLowerCase().trim().equalsIgnoreCase(expectedButtonText.toLowerCase().trim()),
                "The button " + fullscreenAddedToWishlistActualText + " is not changed from Add to Wishlist" + "Expected: " + expectedButtonText
                        + " Actual: " + fullscreenAddedToWishlistActualText);
        softAssertions.assertAll();
    }

    @When("^the second full screen product overlay 'ADD TO WISHLIST' button text changes to '(.*)'$")
    public void verifyFullScreenGoToWishlistButtonText(String expectedButtonText) {
        String fullscreenGoToWishlistActualText = commonFunctions.getTextByCssSelector(fullscreenGoToWishlistByCSS);
        softAssertions.assertTrue(fullscreenGoToWishlistActualText.toLowerCase().trim().equalsIgnoreCase(expectedButtonText.toLowerCase().trim()),
                "The button " + fullscreenGoToWishlistActualText + " is not changed from Add to Wishlist" + "Expected: " + expectedButtonText
                        + " Actual: " + fullscreenGoToWishlistActualText);
        softAssertions.assertAll();
    }

    @Then("^the error message '(.*)' is displayed in red text below the PDP size drop down$")
    public void theErrorMessagePleaseSelectASizeIsDisplayedInRedTextBelowThePdpSizeDropDown(String errormsg) {
        String errorMessages = productDetailPage.grabErrorMessage();
        softAssertions.assertTrue(errormsg.trim().equalsIgnoreCase(errorMessages.trim()),
                "Failure: Size error message not displayed, Actual: "+errorMessages+"  Expected: "+errormsg);
        softAssertions.assertAll();
    }

    @Given("^I scroll to the bottom of the PDP$")
    public void iScrollToTheBottomOfThePDP() {
        productDetailPage.scrollToPageBottom();
    }

    @Given("^the sticky nav wrapper appears on the top of the PDP with the product details$")
    public void theStickyNavWrapperAppearsOnTheTopOfThePDPWithTheProductDetails() {
        boolean isPresent = productDetailPage.isStickyNavPresent();
        softAssertions.assertTrue(isPresent, "Product Sticky Nav was not found.");
        softAssertions.assertAll();
    }

    @Then("^the error message '(.*)' is displayed in red text below the sticky nav wrapper size drop down$")
    public void theErrorMessagePleaseSelectASizeIsDisplayedInRedTextBelowTheStickyNavWrapperSizeDropDown(
            String expectedErrorMsg) {
        String message = productDetailPage.grabSizeErrorMessage();
        softAssertions.assertTrue(message.contains(expectedErrorMsg),
                "Product Sticky Nav. Size error message was not displayed for Add to Wishlist action. Expected: "
                        + expectedErrorMsg + " -- Actual: " + message);
        softAssertions.assertAll();
    }

    @When("^I add a product with '(.*)' to bag$")
    public void selectProductAndToBag(String size){
        String productSize ="";
        if(size.isEmpty()){
            productSize = productDetailsFromJson.retrieveAvailableSizes().get(0);
        }
        else {
            productSize = size;
        }
        productDetailPage.setProductSize(productSize);
        productDetailPage.requiredProductSize = productSize;
        commonFunctions.selectFromDropdownByCSS(pdp_SizeDropDown_CSS,productSize);
        commonFunctions.clickElementByCSS(addToCartButtonLocatorLocator);
    }

    @And("^I add same product with '(.*)' to bag$")
    public void selectproductofDifferentSizeandAddtoBag(String size){
        String productSize ="";
        if(size.isEmpty())
            productSize = productDetailsFromJson.retrieveAvailableSizes().get(1);
        else
            productSize = size;
        selectProductAndToBag(productSize);
    }

}
