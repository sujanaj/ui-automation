package stepdefinition.footer;

import commonfunctions.CommonFunctions;
import commonfunctions.NavigateUsingURL;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.testng.asserts.SoftAssert;
import pages.CheckOutPage;
import pages.GiftCardPage;

public class GiftCardDef extends CommonFunctions {

    CommonFunctions commonFunctions = new CommonFunctions();
    CheckOutPage checkOutPage = new CheckOutPage();
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    String shopGiftCardByCSS = obj_Property.readProperty("shopGiftCardByCSS");
    GiftCardPage giftCardPage = new GiftCardPage();
    String maxFiveErrorMsgByCSS = obj_Property.readProperty("maxFiveErrorMsgByCSS");
    String promoCodeErrorMsgByCSS = obj_Property.readProperty("promoCodeErrorMsgByCSS");
    SoftAssert assertion = new SoftAssert();

    @Given("^I click Shop Gift Cards button$")
    public void iClickShopGiftCardButton() {
        commonFunctions.clickElementByCSS(shopGiftCardByCSS);
    }

    @Then("^I select gift card amount$")
    public void iSelectGiftCardAmount() {
        giftCardPage.selectGiftCard();
    }

    @When("^I add message to gift card$")
    public void iAddMsgToGiftCard() {
        giftCardPage.addMessageToGiftCard();
    }

    @When("^I add same amount gift card '(.*)' times to my cart$")
    public void iAddSameFiveGiftCard(String value) throws InterruptedException {
        giftCardPage.addSameFiveGiftCard(value);
        String currentURL = commonFunctions.getCurrentURL();
        commonFunctions.navigateTo(currentURL);
    }

    @When("^I click add gift card button")
    public void iClickAddGiftCardButton() {
        giftCardPage.addGiftCardButton();
    }

    @When("^I verify button is not displayed$")
    public void iVerifyButtonIsNotDisplayed() {
        giftCardPage.checkedIfButtonNotDisplayed();
    }

    @Then("^I should see '(.*)' when i add more than five giftcard$")
    public void iShouldSeeMax5MsgDisplay(String msgBox) {
        waitForPageToLoad();
        String expected = getTextByCssSelector(maxFiveErrorMsgByCSS);
        assertion.assertTrue(expected.equals(msgBox), "The maximum number of gift cards you can purchase in one order is 5: "
                + expected + " -- Expected: " + msgBox);
        assertion.assertAll();
    }

    @And("^I enter promo code and submit voucher$")
    public void iEnterPromoCodeAndSubmit() {
        waitForPageToLoad();
        giftCardPage.enterPromoCode();
    }

    @And("^I verify '(.*)' displayed$")
    public void iVerifyPromoError(String promoError) {
        waitForPageToLoad();
        String expected = getTextByCssSelector(promoCodeErrorMsgByCSS);
        assertion.assertTrue(expected.equals(promoError), "Promo code is not entered: "
                + expected + " -- Expected: " + promoError);
    }

    @And("^I add '(.*)' of gift card$")
    public void iAddDiffValueGiftCard(String value) {
        giftCardPage.addDiffAmountGifCard(value);
    }

    @And("^I add gift card for Korea & Japan$")
    public void iAddGiftCardKoreaJapan() {
        giftCardPage.addDiffAmountGiftCardKoreaJapan();
    }
}
