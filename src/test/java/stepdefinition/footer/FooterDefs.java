package stepdefinition.footer;

import commonfunctions.CommonFunctions;
import commonfunctions.NavigateUsingURL;
import config.Constants;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.cucumber.datatable.DataTable;
import org.openqa.selenium.WebElement;
import org.testng.asserts.SoftAssert;
import pages.FooterPage;
import pages.GiftCardPage;
import pages.HeaderSettingsPage;
import pages.auth.AuthenticationPage;
import pages.signin.SignInPage;
import utility.CustomStringUtils;

import java.io.IOException;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.List;

public class FooterDefs {

    HeaderSettingsPage headerSettingsPage = new HeaderSettingsPage();
    SoftAssert softAssert = new SoftAssert();
    FooterPage footerPage = new FooterPage();
    GiftCardPage giftCardPage = new GiftCardPage();
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    CommonFunctions commonFunctions = new CommonFunctions();
    SignInPage signInPage = new SignInPage();
    private NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    String shippingCountryHeaderLocator = obj_Property.readProperty("shippingCountrySettingsHeaderById");
    String shippingCountryFooterByCSS = obj_Property.readProperty("shippingCountryFooterByCSS");
    String countryDropDownSettingsPageById = obj_Property.readProperty("countryDropDownSettingsPageById");
    String cssLocatorSaveSettings = obj_Property.readProperty("saveSettingsButtonSettingsPageByClassName");
    String emailManageRegistrationTitle = obj_Property.readProperty("emailManageRegistrationTitleByClassName");
    String loyaltyRewardsTitle = obj_Property.readProperty("loyaltyRewardsTitleByCSS");
    String deliveryPageTitle = obj_Property.readProperty("returnsTitleByCSS");
    String returnsPageTitle = obj_Property.readProperty("returnsPageTitleByCSS");
    String titleofPageFromFooter = obj_Property.readProperty("titleOfPageRedirectedFromFooterByCSS");
    String careersPageTitle = obj_Property.readProperty("careersPageTitleByCSS");
    String baseURL ;
    AuthenticationPage authenticationPage = new AuthenticationPage();


    @Then("^clicking the links for App store should open and switch my view to a new tab containing the correct page url\\.$")
    public void verifyRedirectionPagesForAppLinks(DataTable dataTable) throws IOException {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2) {
            for (int i = 1; i < rawTable.size(); i++) {
                //logger.info("Row data: " + rawTable.get(i));
                String link = CustomStringUtils.getExpectedPageData(rawTable.get(i).get(0), headerSettingsPage.grabHeaderLanguage());
                String expectedUrl = rawTable.get(i).get(1);
                String frlink = rawTable.get(i).get(2);

                String countryLabel = headerSettingsPage.grabCountryLabel();
                if (countryLabel.contentEquals("France")) {
                    footerPage.clickOnOurAppsTextLinks(frlink);
                    footerPage.switchTabVerifyUrlAndClose(expectedUrl);
                    if (link.contentEquals("apple") || link.contentEquals("google")) {
                        footerPage.clickOnAppStoreImagesLinks(link);
                        footerPage.switchTabVerifyUrlAndClose(expectedUrl);
                    }
                } else {
                    if (link.contentEquals("apple") || link.contentEquals("google")) {
                        footerPage.clickOnAppStoreImagesLinks(link);
                        footerPage.switchTabVerifyUrlAndClose(expectedUrl);
                    } else {
                        footerPage.clickOnOurAppsTextLinks(link);
                        footerPage.switchTabVerifyUrlAndClose(expectedUrl);
                    }
                }
            }
        }
    }

    @Then("^on clicking on the icons for 'social networking sites' should open and switch my view to a new tab containing the correct page url.$")
    public void verifyRedirectionPagesForSocialIcons(DataTable dataTable) {
       // logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2) {
            for (int i = 1; i < rawTable.size(); i++) {
               // logger.info("Row data: " + rawTable.get(i));
                String icon = rawTable.get(i).get(0);
                String expectedUrl = rawTable.get(i).get(1);

                icon = icon.replace("pinterest", "pin-interest");

                footerPage.clickOnSocialIcon(icon);
                footerPage.switchTabVerifyUrlAndClose(expectedUrl);
            }
        }
    }

    @Then("^the email address field of Email Preferences form should contain the correct placeholder text '(.*)'$")
    public void verifyEmailPlaceHolderText(String expectedText) {
       // logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        expectedText = CustomStringUtils.getExpectedPageData(expectedText, headerSettingsPage.grabHeaderLanguage());
        String countryLabel = headerSettingsPage.grabCountryLabel();
        String actualPlaceHolderText = footerPage.grabEmailPlaceHolderText();
        softAssert.assertTrue(expectedText.contentEquals(actualPlaceHolderText),"The Email placeholder text was not the expected one. Expected: " + expectedText
                + " Actual: " + actualPlaceHolderText);
        softAssert.assertAll();
    }

    @Then("^clicking 'SIGN UP' on the Email Preferences form should trigger an error message$")
    public void verifyFooterEmailErrorMessage(DataTable dataTable) {
       // logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 2) {
            for (int i = 1; i < rawTable.size(); i++) {
               // logger.info("Row data: " + rawTable.get(i));
                String email = rawTable.get(i).get(0);
                String expectedErrorMessage = CustomStringUtils.getExpectedPageData(rawTable.get(i).get(1), headerSettingsPage.grabHeaderLanguage());
                footerPage.writeInFooterEmailField(email);
                footerPage.clickOnSignUpButton();
                String actualErrorMessage = footerPage.grabFooterInvalidErrorMessage();
                softAssert.assertTrue(expectedErrorMessage.contentEquals(actualErrorMessage),
                        "The error message is not visible or is incorrect. Expected: " + expectedErrorMessage
                                + " Actual: " + actualErrorMessage);
                softAssert.assertAll();
            }
        }
    }

    @When("^I use the footer Email Preferences form to sign up for emails using '(.*)'$")
    public void signUpFromFooterEmail(String email) {
        navigateUsingURL.navigateToRelativeURL("/login");
        signInPage.enterCredentials(Constants.CHROME_USERNAME, Constants.CHROME_PASSWORD);
        authenticationPage.clickAccountLink("Sign Out");
        footerPage.writeInFooterEmailField(CustomStringUtils.getExpectedPageData(email, headerSettingsPage.grabHeaderLanguage()));
        footerPage.clickOnSignUpButton();
    }

    @Then("^I should be redirected to Email registration page containing the pagetitle '(.*)' and subheader '(.*)'$")
    public void verifyRedirectedPageTitleAndSubheader(String pageTitle, String subHeader) {

        String registerForEmailUpdates = obj_Property.readProperty("registerForEmailUpdatesByCSS");
        String currentCustomerHeader = obj_Property.readProperty("currentCustomerHeaderByClassName");
        pageTitle = CustomStringUtils.getExpectedPageData(pageTitle, headerSettingsPage.grabHeaderLanguage());
        subHeader = CustomStringUtils.getExpectedPageData(subHeader, headerSettingsPage.grabHeaderLanguage());
//        logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + " - pageTitle: " + pageTitle
//                + ", subHeader: " + subHeader);
        String actualPageTitle = commonFunctions.getUppercaseText(registerForEmailUpdates);
        String actualSubHeader = commonFunctions.getUppercaseText(currentCustomerHeader);
        softAssert.assertTrue(pageTitle.contentEquals(actualPageTitle.trim()),
                "The error message is not visible Expected: " + pageTitle + " Actual: " + actualPageTitle.trim()  );
        softAssert.assertTrue(subHeader.contentEquals(actualSubHeader.trim()),
                "The error message is not visible Expected: " + subHeader + " Actual: " + actualSubHeader.trim());
        softAssert.assertAll();
    }

    @When("^changing the country via the footer link causes the '(.*)' and '(.*)' to be updated in the footer$")
    public void verifyPhoneNumberChanges(String phoneNumber, String landlineNumber) {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        String telephoneNumberByCSS = obj_Property.readProperty("telephoneNumberFooterByCSS");
        String landlineNumberByCSS = obj_Property.readProperty("landlineNumberFooterByCSS");
        String countryLabel = commonFunctions.getTextByCssSelector(shippingCountryHeaderLocator);
        phoneNumber = Normalizer.normalize(phoneNumber,Normalizer.Form.NFKC);
        if (countryLabel.contentEquals("United Arab Emirates")) {
                    String actualEmiratesNumber = commonFunctions.getTextByCssSelector(telephoneNumberByCSS);
                    softAssert.assertTrue(phoneNumber.contentEquals(actualEmiratesNumber), "The actual United Arab Emirates phone number is not the corect one: Expected: "
                            + phoneNumber + " Actual: " + actualEmiratesNumber);
                    softAssert.assertAll();
                } else {
                    String actualNumber = commonFunctions.getTextByCssSelector(telephoneNumberByCSS).trim();
                    actualNumber = Normalizer.normalize(actualNumber, Normalizer.Form.NFKC);
                    String actualLandLineNumber = commonFunctions.getTextByCssSelector(landlineNumberByCSS);
                    softAssert.assertTrue(phoneNumber.contentEquals(actualNumber), "The actual phone number is not the corect one: Expected: "
                            + phoneNumber + " Actual: " + actualNumber);
                    softAssert.assertTrue(landlineNumber.contentEquals(actualLandLineNumber),"The actual land line number is not the corect one: Expected: " + landlineNumber
                            + " Actual: " + actualLandLineNumber);
                    softAssert.assertAll();
                }
    }

    @Given("changing the country via the footer link causes my shipping country to be updated in the footer")
    public void changeCountryViaHeaderLink(DataTable dataTable) {
        // logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() >= 1)
            for (int i = 1; i < rawTable.size(); i++) {
                String country = rawTable.get(i).get(0);
                changeCountryFromFooter(country);
                String footerCountry = commonFunctions.getTextByCssSelector(shippingCountryFooterByCSS);
                softAssert.assertTrue(country.contentEquals(footerCountry),"The actual shipping country is not the corect. Expected: " + country
                        + " Actual: " + footerCountry);
                softAssert.assertAll();
            }
    }

    @Given("changing the country via the footer link always returns me to the same page")
    public void verifyPageRedirectionViaTheCountryFooterLink(DataTable dataTable) {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        List<List<String>> rawTable = dataTable.asLists();
        if (navigateUsingURL.getrelativeURLJenkins() == null) {
            baseURL=Constants.BASE_URL;
        } else {
            baseURL = navigateUsingURL.getrelativeURLJenkins();
        }
        if (rawTable.size() >= 2)
            for (int i = 1; i < rawTable.size(); i++) {
                //logger.info("Row data: " + rawTable.get(i));
                String navigateToRelativeURL = rawTable.get(i).get(0) ;
                commonFunctions.navigateTo(baseURL+navigateToRelativeURL);
                String country = rawTable.get(i).get(1);
                commonFunctions.refreshPage();
                changeCountryFromFooter(country);
                String currentUrl = commonFunctions.getCurrentURL().replaceAll(baseURL,"");
                softAssert.assertTrue(currentUrl.contains(navigateToRelativeURL),"The actual shipping country is not the corect one: Expected: "
                                + navigateToRelativeURL + " Actual: " + currentUrl  );
                softAssert.assertAll();
            }

    }

    @Then("^I should be redirected to Manage Preferences page containing the pagetitle '(.*)'$")
    public void verifyManagePreferanceTitleAndSubheader(String pageTitle) {
        pageTitle = CustomStringUtils.getExpectedPageData(pageTitle, headerSettingsPage.grabHeaderLanguage());
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName() + " - pageTitle: " + pageTitle
             //   + ", subHeader: " + subHeader);
        String actualPageTitle = commonFunctions.getUppercaseText(emailManageRegistrationTitle);
        softAssert.assertTrue(pageTitle.contentEquals(actualPageTitle.trim()),
                "The error message is not visible Expected: " + pageTitle + " Actual: " + actualPageTitle.trim());
        softAssert.assertAll();
    }

    @Then("^I click on the 'Email preferences' link$")
    public void clickOnEmailPreferences() {
        //logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        footerPage.clickOnEmailPreferences();
    }

     public void changeCountryFromFooter(String country){
        commonFunctions.focusElement(shippingCountryFooterByCSS);
        commonFunctions.clickElementByCSS(shippingCountryFooterByCSS);
        commonFunctions.selectFromDropdownByCSS(countryDropDownSettingsPageById,country);
        commonFunctions.clickElementByCSS(cssLocatorSaveSettings);
        commonFunctions.refreshPage();
     }

    @Then("^clicking the gift cards links should take me to the right page with sections '(.*)' and '(.*)'$")
    public void verifyFooterGiftCardsLinks(String shopGC, String activateGC) {
        // logger.info(Thread.currentThread().getStackTrace()[1].getMethodName());
        String expectedLink1 = shopGC;
        String expectedLink2 = activateGC;
        navigateUsingURL.navigateToRelativeURL(Constants.GIFTCARDLINKSFOOTER);
        String actualLink1 = giftCardPage.grabShopGiftCards();
        String actualLink2 = giftCardPage.grabActivateYourGiftCards();
        softAssert.assertTrue(expectedLink1.contentEquals(actualLink1.trim()),
                "Shop Gift cards link is not visible. Expected link: " + expectedLink1 + " Actual Link: " + actualLink1.trim());
        softAssert.assertTrue(expectedLink1.contentEquals(actualLink1.trim()),
                "Activate Your Gift Card link is not visible. Expected link: " + expectedLink2 + " Actual Link: " + actualLink2.trim());
        softAssert.assertAll();

    }

    @Then("footer links should take me to the right page {string}")
    public void footer_links_should_take_me_to_the_right_page(String link, DataTable dataTable) {
        List<List<String>> rawTable = dataTable.asLists();
        String expectedPageTitle = "";
        String actualPageTitle = "";
        WebElement we;
        String eachLink;
        if(link.equalsIgnoreCase("matchesfashion")){
            for(int i=0; i< rawTable.size(); i++){
                expectedPageTitle = rawTable.get(i).get(0);
                eachLink = Constants.MATCHESFASHIONLINKSFOOTER.get(i);
                navigateUsingURL.navigateToRelativeURL(eachLink);
                if(eachLink.contains("eco-age") || eachLink.contains("careers"))
                    actualPageTitle = footerPage.retrieveText(careersPageTitle);
                else
                    actualPageTitle = footerPage.retrieveText(titleofPageFromFooter);
                softAssert.assertTrue(actualPageTitle.toLowerCase().trim().contentEquals(expectedPageTitle.toLowerCase().trim()),
                        String.format("Page Title wasn't correctly displayed.\n" +
                                "Expected: '%1s'.\nActual:'%2s'", expectedPageTitle, actualPageTitle));
                softAssert.assertAll();
            }
        }
        else if(link.equalsIgnoreCase("help") || link.equalsIgnoreCase("Aide") ||
                link.equalsIgnoreCase("고객 지원") || link.equalsIgnoreCase("ヘルプ")){
            for(int i=0; i< rawTable.size(); i++){
                expectedPageTitle = rawTable.get(i).get(0);
                eachLink = Constants.HELPLINKSFOOTER.get(i);
                navigateUsingURL.navigateToRelativeURL(eachLink);
                if(eachLink.contains("delivery"))
                    actualPageTitle = footerPage.retrieveText(deliveryPageTitle);
                else if(eachLink.contains("returns"))
                    actualPageTitle = footerPage.retrieveText(returnsPageTitle);
                else
                    actualPageTitle = footerPage.retrieveText(titleofPageFromFooter);
                softAssert.assertTrue(actualPageTitle.toLowerCase().trim().contentEquals(expectedPageTitle.toLowerCase().trim()),
                        String.format("Page Title wasn't correctly displayed.\n" +
                                "Expected: '%1s'.\nActual:'%2s'", expectedPageTitle, actualPageTitle));
                softAssert.assertAll();
            }
        }else if(link.equalsIgnoreCase("services") || link.equalsIgnoreCase("프리미엄 서비스")||
                link.equalsIgnoreCase("サービス")){
            for(int i=0; i< rawTable.size(); i++){
                expectedPageTitle = rawTable.get(i).get(0);
                eachLink = Constants.SERVICELINKSFOOTER.get(i);
                navigateUsingURL.navigateToRelativeURL(eachLink);
                if(eachLink.contains("loyalty"))
                    actualPageTitle = footerPage.retrieveText(loyaltyRewardsTitle);
                else
                    actualPageTitle = footerPage.retrieveText(titleofPageFromFooter);

                softAssert.assertTrue(actualPageTitle.toLowerCase().trim().contentEquals(expectedPageTitle.toLowerCase().trim()),
                        String.format("Page Title wasn't correctly displayed.\n" +
                                "Expected: '%1s'.\nActual:'%2s'", expectedPageTitle, actualPageTitle));
                softAssert.assertAll();
            }

        } else if(link.equalsIgnoreCase("Retrouvez-nous") || link.equalsIgnoreCase("Visit Us") ||
                link.equalsIgnoreCase("스토어") || link.equalsIgnoreCase("店舗情報")){
            String activeTab;
            boolean isActive=false;
            for(int i=1; i<=rawTable.size(); i++ ){
                expectedPageTitle = rawTable.get(i-1).get(0);
                eachLink = Constants.STORELINKSFOOTER.get(i-1);
                navigateUsingURL.navigateToRelativeURL(eachLink);
                if(i != 4){
                    we = commonFunctions.getWebElement(".stores__tabs__wrapper a:nth-child("+i+")");
                    activeTab = we.getAttribute("class");
                    actualPageTitle = we.getText();
                    if(activeTab.contains("active"))
                        isActive=true;
                    softAssert.assertTrue(isActive &&(actualPageTitle.equalsIgnoreCase(expectedPageTitle)),
                            "Expected and actual Stores are not same. Expected:"+expectedPageTitle+ "Actual page title: "+actualPageTitle);
                    softAssert.assertAll();
                }else{
                    activeTab = commonFunctions.getCurrentURL();
                    softAssert.assertTrue(activeTab.contains(eachLink),
                            "Expected and actual Store links are not same. Expected link:"+eachLink+ "Actual link: "+activeTab);
                    softAssert.assertAll();

                }
            }

        }

    }

}
