package stepdefinition.signIn;

import java.text.Normalizer;
import java.util.List;

import baseclass.WebDriverFactory;
import commonfunctions.CommonFunctions;
import commonfunctions.NavigateUsingURL;
import config.Constants;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import datamodel.account.RegistrationErrorModel;
import io.cucumber.datatable.DataTable;
import org.testng.asserts.SoftAssert;
import pages.signin.GuestSignInPage;
import pages.signin.RegisterSignInPage;
import pages.account.AddressBookPage;
import pages.auth.AuthenticationPage;
import pages.auth.LoginPage;
import pages.signin.SignInPage;
import utility.CustomStringUtils;


public class SignInDefs {
    CommonFunctions commonFunctions = new CommonFunctions();
    NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    AuthenticationPage authenticationPage = new AuthenticationPage();
    WebDriverFactory webDriverFactory = new WebDriverFactory();
    AddressBookPage addressBookPage = new AddressBookPage();
    LoginPage loginPage = new LoginPage();
    RegisterSignInPage registerSignInPage = new RegisterSignInPage();
    SoftAssert assertion = new SoftAssert();
    CustomStringUtils customStringUtils = new CustomStringUtils();
    GuestSignInPage guestSignInPage = new  GuestSignInPage();
    SignInPage signInPage = new SignInPage();


    public void enterCredentialsWithStaySignedInSelected(String userName, String password){
        signInPage.enterCredentialsToLogin(userName,password);
        registerSignInPage.selectStaySignedIn();
        signInPage.clickOnLoginButton();
    }

    @And("^I log in with the following credentials$")
    public void iLogInWithTheFollowingCredentials() {
        if (webDriverFactory.browser.equalsIgnoreCase(Constants.CHROME)) {
            commonFunctions.waitForPageToLoad();
            signInPage.enterCredentials(Constants.CHROME_USERNAME, Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.SAFARI)) {
            commonFunctions.waitForPageToLoad();
            signInPage.enterCredentials(Constants.CHROME_USERNAME, Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.FIREFOX)) {
            commonFunctions.waitForPageToLoad();
            signInPage.enterCredentials(Constants.CHROME_USERNAME, Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.IE)) {
            commonFunctions.waitForPageToLoad();
            signInPage.enterCredentials(Constants.CHROME_USERNAME, Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.EDGE)) {
            commonFunctions.waitForPageToLoad();
            signInPage.enterCredentials(Constants.CHROME_USERNAME, Constants.CHROME_PASSWORD);
        }
    }


    @Given("^I am signed in to the website as a customer$")
    public void signInAsACustomer() {
        if (!authenticationPage.checkIfSignedIn()) {
            authenticationPage.setUserCredentials();
            authenticationPage.clickOnSignInButton();
            String userName = authenticationPage.getUserName();
            String UserPassword = authenticationPage.getUserPassword();
            loginPage.logIn(userName, UserPassword);
        }
    }

    @And("^I log in with following user which has no wishlist$")
    public void nowishlistUserSignIn(DataTable dataTable) {
        List<List<String>> rawTable = dataTable.asLists();
        if (rawTable.size() > 0) {
            for (int i = 1; i < rawTable.size(); i++) {
                List<String> row = rawTable.get(i);
                String email = row.get(0);
                String password = row.get(1);
                loginPage.logIn(email, password);
            }
        }
    }

    @Then("^I should be signed in$")
    public void iShouldBeSignedIn() {
        String accountLabel = authenticationPage.grabMyAccountLabel();
        assertion.assertTrue(!accountLabel.toUpperCase().contains("SIGN IN"), "Failure: Sign in is still displayed on homepage. ");
        assertion.assertAll();
    }

    @Then("^account should not be created$")
    public void accountShouldNotBeCreated() {
        assertion.assertTrue(commonFunctions.getCurrentURL().contains("login/register"), "Failure: URL did not contain 'login/register' pattern. Actual: " + commonFunctions.getCurrentURL());
        assertion.assertAll();
    }

    @Then("^I should see title as '(.*)'$")
    public void iShouldSeeCreateAnAccount(String title) {

        String actualTitle = registerSignInPage.grabCreateAnAccountTitle();
        assertion.assertTrue(actualTitle.equalsIgnoreCase(title), "Failure: Expected account title not found. Expected: " + "CREATE AN ACCOUNT" + " -- Actual: "
                + actualTitle);
        assertion.assertAll();
    }


    @Then("^the fields in the 'CREATE AN ACCOUNT' section should contain the correct placeholder text '(.*)' '(.*)' '(.*)' '(.*)' '(.*)' '(.*)' '(.*)' '(.*)'$")
    public void theFieldsInTheCreateAnAccountSectionShouldContainTheCorrectPlaceholderText(String title, String firstName, String lastName, String email, String password, String confirmPassword, String country, String phone) {

        assertion.assertTrue(registerSignInPage.getTitlePlaceholder().contains(title), "Failure: title not as expected. Expected: " + title + " -- Actual: " + registerSignInPage.getTitlePlaceholder());
        assertion.assertTrue(registerSignInPage.getFirstNamePlaceholder().contains(firstName), "Failure: first name not as expected. Expected: " + firstName + " -- Actual: " + registerSignInPage.getFirstNamePlaceholder());
        assertion.assertTrue(registerSignInPage.getLastNamePlaceholder().contains(lastName), "Failure: last name not as expected. Expected: " + lastName + " -- Actual: " + registerSignInPage.getLastNamePlaceholder());
        assertion.assertTrue(registerSignInPage.getEmailPlaceholder().contains(email), "Failure: email not as expected. Expected: " + email + " -- Actual: " + registerSignInPage.getEmailPlaceholder());
        assertion.assertTrue(registerSignInPage.getPasswordPlaceholder().contains(password), "Failure: password not as expected. Expected: " + password + " -- Actual: " + registerSignInPage.getPasswordPlaceholder());
        assertion.assertTrue(registerSignInPage.getConfirmPasswordPlaceholder().contains(confirmPassword), "Failure: confirm password not as expected. Expected: " + confirmPassword + " -- Actual: " + registerSignInPage.getConfirmPasswordPlaceholder());
        assertion.assertTrue(registerSignInPage.getMobileNumberPlaceholder().contains(phone), "Failure: phone not as expected. Expected: " + phone + " -- Actual: " + registerSignInPage.getMobileNumberPlaceholder());
        assertion.assertTrue(registerSignInPage.getCountryPlaceholder().contains(country), "Failure: country not as expected. Expected: " + country + " -- Actual: " + registerSignInPage.getCountryPlaceholder());
        assertion.assertAll();
    }


    @Then("^I should see following the error messages '(.*)' '(.*)' '(.*)' '(.*)' '(.*)' '(.*)' '(.*)'$")
    public void iShouldSeeFollowingTheErrorMessages(String title, String firstName, String lastName, String email, String password, String confirmPassword, String phone) {


        RegistrationErrorModel actualErrorMessages = registerSignInPage.setRegistrationErrorMessage();

        assertion.assertTrue(actualErrorMessages.getTitle().equalsIgnoreCase(title), "Failure: title error message not as expected. Expected: " + title + " -- Actual: " + actualErrorMessages.getTitle());
        assertion.assertTrue(actualErrorMessages.getFirstName().equalsIgnoreCase(firstName), "Failure: first name error message not as expected. Expected: " + firstName + " -- Actual: " + actualErrorMessages.getFirstName());
        assertion.assertTrue(actualErrorMessages.getLastName().equalsIgnoreCase(lastName), "Failure: last name error message not as expected. Expected: " + lastName + " -- Actual: " + actualErrorMessages.getLastName());
        assertion.assertTrue(actualErrorMessages.getEmail().equalsIgnoreCase(email), "Failure: email error message not as expected. Expected: " + email + " -- Actual: " + actualErrorMessages.getEmail());
        assertion.assertTrue(Normalizer.normalize(actualErrorMessages.getPassword(), Normalizer.Form.NFKC).equalsIgnoreCase(password), "Failure: password error message not as expected. Expected: " + password + " -- Actual: " + actualErrorMessages.getPassword());
        assertion.assertTrue(actualErrorMessages.getCheckPassword().equalsIgnoreCase(confirmPassword), "Failure: confirm password error message not as expected. Expected: " + confirmPassword + " -- Actual: " + actualErrorMessages.getCheckPassword());
        assertion.assertTrue(actualErrorMessages.getPhone().equalsIgnoreCase(phone), "Failure: phone error message not as expected. Expected: " + phone + " -- Actual: " + actualErrorMessages.getPhone());
        assertion.assertAll();
    }

    @Then("^I should see the '(.*)' Error Message$")
    public void iShouldSeeTheAnAccountAlreadyExistsForThisEmailAddressErrorMessage(String expectedMessage) {
        assertion.assertTrue(Normalizer.normalize(registerSignInPage.getEmailErrorExistingAccount().trim(), Normalizer.Form.NFKC).equalsIgnoreCase(expectedMessage), "Failure: email error message not as expected. Expected: " + expectedMessage + " -- Actual: " + registerSignInPage.getEmailErrorExistingAccount());
        assertion.assertAll();
    }

    @And("^I should see the '(.*)' validation Error Message to not enter non-latin characters$")
    public void iShouldSeeTheError_messageValidationErrorMessageToNotEnterNonLatinCharacters(String expectedMessage) {
        RegistrationErrorModel actualErrorMessages = registerSignInPage.setRegistrationErrorMessage();

        assertion.assertTrue(actualErrorMessages.getFirstName().equalsIgnoreCase(expectedMessage), "Failure: first name error message not as expected. Expected: " + expectedMessage + " -- Actual: " + actualErrorMessages.getFirstName());
        assertion.assertTrue(actualErrorMessages.getLastName().equalsIgnoreCase(expectedMessage), "Failure: last name error message not as expected. Expected: " + expectedMessage + " -- Actual: " + actualErrorMessages.getLastName());
        assertion.assertAll();
    }

    @And("^I log in with the following credentials and select 'Stay signed in' checkbox$")
    public void iLogInWithTheFollowingCredentialsAndSelectStaySignedInCheckbox() {
        if (webDriverFactory.browser.equalsIgnoreCase(Constants.CHROME)) {
            enterCredentialsWithStaySignedInSelected(Constants.CHROME_USERNAME, Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.SAFARI)) {
            enterCredentialsWithStaySignedInSelected(Constants.CHROME_USERNAME, Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.FIREFOX)) {
            enterCredentialsWithStaySignedInSelected(Constants.CHROME_USERNAME, Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.IE)) {
            enterCredentialsWithStaySignedInSelected(Constants.CHROME_USERNAME, Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.EDGE)) {
            enterCredentialsWithStaySignedInSelected(Constants.CHROME_USERNAME, Constants.CHROME_PASSWORD);
        }
    }

    @And("^I delete the JSessionId from cookies and reload page$")
    public void iDeleteTheJsessionIdFromCookiesAndReloadPage()  {
       commonFunctions.deleteJSessionID();
       commonFunctions.refreshPage();
       commonFunctions.waitForPageToLoad();
    }

    @Then("^I sign out when county '(.*)' and language '(.*)'$")
    public void signOut(String country, String language) {
        navigateUsingURL.navigateToRelativeURL("");
        loginPage.clickOnSignOutLink(country,language);
    }

    @When("^I login as a guest user '(.*)'$")
    public void iLoginAsAGuest(String firstName) {
        commonFunctions.waitForPageToLoad();
        String randomEmail = customStringUtils.generateRandomEmail(firstName);
        guestSignInPage.inputGuestEmail(randomEmail);
        guestSignInPage.clickContinueGuest();
    }

}
