#Author: Ade
@regression @addgiftcard
Feature: Place order with GiftCard
  In order to generate revenue
  The users of the website and impersonation
  Should be able to place orders using gift cards

  Scenario Outline: Add diff amount of gift card for UK & France
    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to '/gift-cards/giftcard'
    And I add '<values>' of gift card
    And I go to shopping bag clicking on minicart
    Examples:
      | country           | language | currency          | values |  |
      | United Kingdom    |          |                   | 2      |  |
      | France            | English| British Pound (£) | 2      |  |

  Scenario Outline: Add diff amount of gift card for Korea & Japan
    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to '/gift-cards/giftcard'
    And I add gift card for Korea & Japan
    And I go to shopping bag clicking on minicart
    Examples:
      | country           | language | currency          | values |  |
      | Korea Republic of | English  | British Pound (£) | 2      |  |
      | Japan             | English  | British Pound (£) | 2      |  |

  Scenario Outline: Add same amount of Gift Card more than five times + verify promo code  error
    Given I am on the website with '<country>' '<language>' '<currency>'
    Given I navigate to '/login'
    And I log in with the following credentials
    And I navigate to '/gift-cards/giftcard'
    And I add same amount gift card '5' times to my cart
    And I navigate to '/gift-cards/giftcard'
    When I select gift card amount
    And I add message to gift card
    And I click add gift card button
    And I should see 'The maximum number of gift cards you can purchase in one order is 5' when i add more than five giftcard
    And I verify button is not displayed
    And I go to shopping bag clicking on minicart
    And I enter promo code and submit voucher
    Then I verify 'sorry, that code cannot be used to purchase giftcards' displayed
    Examples:
      | country           | language | currency          | values |
      | United Kingdom    |          | British Pound (£) | 5      |
      | France            | English | British Pound (£) | 5      |
      | Korea Republic of | English      | British Pound (£) | 5      |
      | Japan             | English      | British Pound (£) | 5      |


  Scenario Outline: Place order with Gift and Product
    Given I am on the website with '<country>' '<language>' '<currency>'
    Given I navigate to '/login'
    And I log in with the following credentials
    And I navigate to '/gift-cards/giftcard'
    And I add gift card for Korea & Japan
    And I add products to my cart
    And I check out
    When I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1      | address_line_2      | town_city          | postcode_zip | pccc          |
      | DO NOT DELETE LINE1 | DO NOT DELETE LINE2 | DO NOT DELETE Town | 12345        | P111111111111 |
    And I select the '<shipping_method>' billing address
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
    And I select the saved '<card_type>' credit card
    And I select the '<billing_address>' billing address
    And I place the order
    Then I should see the order confirmation page
    Examples:
      | country           | language | currency          | card_type | shipping_method | card_number      | card_name  | expiry_month | expiry_year | security_code | values |
      | United Kingdom    |          | British Pound (£) | Visa      | EXPRESS         | 4444333322221111 | ORDER TEST | 01           | 2026        | 123           | 2      |
      | France            | English | British Pound (£) | Visa      | EXPRESS         | 4444333322221111 | ORDER TEST | 01           | 2026        | 123           | 2      |
      | Korea Republic of | English      | British Pound (£) | Visa      | EXPRESS         | 4444333322221111 | ORDER TEST | 01           | 2026        | 123           | 2      |
      | Japan             | English      | British Pound (£) | Visa      | EXPRESS         | 4444333322221111 | ORDER TEST | 01           | 2026        | 123           | 2      |

  Scenario Outline: Place order with only Gift Card
    Given I am on the website with '<country>' '<language>' '<currency>'
    Given I navigate to '/login'
    And I log in with the following credentials
    And I navigate to '/gift-cards/giftcard'
    And I add gift card for Korea & Japan
    And I check out
    When I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1      | address_line_2      | town_city          | postcode_zip | pccc          |
      | DO NOT DELETE LINE1 | DO NOT DELETE LINE2 | DO NOT DELETE Town | 12345        | P111111111111 |
    And I select the '<shipping_method>' billing address
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
   # And I select the saved '<card_type>' credit card
   # And I select the '<billing_address>' billing address
    And I place the order
    Then I should see the order confirmation page
    Examples:
      | country           | language | currency          | card_type | shipping_method | card_number      | card_name  | expiry_month | expiry_year | security_code | values |
      | United Kingdom    |          | British Pound (£) | Visa      | EXPRESS         | 4444333322221111 | ORDER TEST | 01           | 2026        | 123           | 2      |
      | France            | English | British Pound (£) | Visa      | EXPRESS         | 4444333322221111 | ORDER TEST | 01           | 2026        | 123           |        |
      | Korea Republic of | English      | British Pound (£) | Visa      | EXPRESS         | 4444333322221111 | ORDER TEST | 01           | 2026        | 123           |        |
      | Japan             | English      | British Pound (£) | Visa      | EXPRESS         | 4444333322221111 | ORDER TEST | 01           | 2026        | 123           |        |

