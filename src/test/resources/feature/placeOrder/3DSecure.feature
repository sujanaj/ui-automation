#Author: Mona Agrawal
@regression @teamSecure
Feature: Place order with 3D-Secure cards
  In order to generate revenue
  The users of the website and impersonation
  Should be able to place orders


   ########### 3d-Secure Prototype ###########

  @3dSecure @regression
  Scenario Outline: (1) 475 Website, 3d-Secure, single-line, existing customer, saved shipping address, new Visa card, saved billing address, customer should be able to place order as a registered user with 3D-Secure challenge screen.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I check out
    And I select the '<shipping_method>' billing address
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
#    And Secure SessionId should exist
    And I unselect the checkbox to not save the card by default
    And I place the order
    And I will get One Time Password pop-up enter One Time Password
    And I submit  One Time Password
    Then I should see the order confirmation page

    Examples:
      | country        | language | currency | shipping_method | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code |
      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001091 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001117 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001125 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001133 | ORDER TEST | 01           | 2022        | 123           |
      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001096 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001112 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001120 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001138 | ORDER TEST | 01           | 2022        | 123           |
      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001098  | ORDER TEST | 01           | 2022        | 1234           |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001114  | ORDER TEST | 01           | 2022        | 1234           |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001122  | ORDER TEST | 01           | 2022        | 1234           |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001130 | ORDER TEST | 01           | 2022        | 1234           |


  @3dSecure @regression
  Scenario Outline: (2) 475 Website, 3d-Secure, single-line, existing customer, saved shipping address, new Visa card, saved billing address, customer should not be able to place order as a registered user with 3D-Secure challenge screen.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I check out
    And I select the '<shipping_method>' billing address
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
#    And Secure SessionId should exist
    And I unselect the checkbox to not save the card by default
    And I place the order
    And I will get One Time Password pop-up enter One Time Password
    And I submit  One Time Password
    Then I should get the error message '<error>' and order placement should get failed

    Examples:
      | country        | language | currency | shipping_method | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code | error |
      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001109 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001104 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001106  | ORDER TEST | 01           | 2022        | 1234          |       |



  @3dSecure @regression
  Scenario Outline: (3) 476 Website, 3d-Secure, single-line, existing customer, saved shipping address, new Visa card, saved billing address, customer should not be able to place order.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    When I check out
    And I select the '<shipping_method>' billing address
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
#    And Secure SessionId should exist
    And I unselect the checkbox to not save the card by default
    And I place the order
    Then I should get the error message '<error>' and order placement should get failed

    Examples:
      | country        | language | currency | shipping_method | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code | error |
      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001018 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001042 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001013 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001047 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001015  | ORDER TEST | 01           | 2022        | 1234           |       |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001049  | ORDER TEST | 01           | 2022        | 1234           |       |



  @3dSecure @regression
  Scenario Outline: (4) 100 Website, 3d-Secure, single-line, existing customer, saved shipping address, new Visa card, saved billing address, customer should be able to place order without 3D-Secure challenge screen .

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I check out
    And I select the '<shipping_method>' billing address
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
#    And Secure SessionId should exist
    And I unselect the checkbox to not save the card by default
    And I place the order
    Then I should see the order confirmation page

    Examples:
      | country        | language | currency | shipping_method | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code |
      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001000 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001026 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001034 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001059 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001067 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001075 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001083 | ORDER TEST | 01           | 2022        | 123           |
      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001005 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001021 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001039 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001054 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001062 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001070 | ORDER TEST | 01           | 2022        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001088 | ORDER TEST | 01           | 2022        | 123           |
      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001007  | ORDER TEST | 01           | 2022        | 1234           |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001023  | ORDER TEST | 01           | 2022        | 1234           |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001031  | ORDER TEST | 01           | 2022        | 1234          |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001056  | ORDER TEST | 01           | 2022        | 1234           |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001064  | ORDER TEST | 01           | 2022        | 1234           |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001072  | ORDER TEST | 01           | 2022        | 1234           |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001080  | ORDER TEST | 01           | 2022        | 1234           |


    ##################################


  @3dSecure @regression

  Scenario Outline: (5) 100 Website, 3d-Secure, single-line, new shipping address, new Visa card & new billing address,customer should be able to place order as a guest user.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I add products to my cart
    When I navigate to the page '<url>'
    When I proceed to checkout from the shopping bag
    Then I should be navigated to '<loginURL>'
    When I login as a guest user '<first_name>'
    And I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter '<phone_number>' in the Phone Number field
    And I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
      | 1 Test Street  | Test Road      | Test town | 1234         | P111111111111 |
    And the 'SAVE ADDRESS' button text is changed to '<address_saved>'
    And I click 'CONTINUE' then I am navigated to the 'REVIEW and PAY' page
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
    And I place the order
    Then I should see the order confirmation page

    Examples:
      | country        | language | currency | url           | loginURL          | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code | title | first_name | last_name | phone_number | address_saved |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Visa             | 4000000000001000 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Visa             | 4000000000001026 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Visa             | 4000000000001034 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Visa             | 4000000000001059 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Visa             | 4000000000001067 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Visa             | 4000000000001075 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Visa             | 4000000000001083 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Mastercard       | 5200000000001005 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Mastercard       | 5200000000001021 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Mastercard       | 5200000000001039 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Mastercard       | 5200000000001054 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Mastercard       | 5200000000001062 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Mastercard       | 5200000000001070 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | Mastercard       | 5200000000001088 | ORDER TEST | 01           | 2022        | 123           | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | American Express | 340000000001007  | ORDER TEST | 01           | 2022        | 1234          | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | American Express | 340000000001023  | ORDER TEST | 01           | 2022        | 1234          | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | American Express | 340000000001031  | ORDER TEST | 01           | 2022        | 1234          | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | American Express | 340000000001056  | ORDER TEST | 01           | 2022        | 1234          | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | American Express | 340000000001064  | ORDER TEST | 01           | 2022        | 1234          | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | American Express | 340000000001072  | ORDER TEST | 01           | 2022        | 1234          | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | American Express | 340000000001080  | ORDER TEST | 01           | 2022        | 1234          | Mr    | GuestUser  | Test      | 0765432198   | ADDRESS SAVED |


  @3dSecure @regression

  Scenario Outline: (6) 476 Website, 3d-Secure, single-line, new shipping address, new Visa card & new billing address,customer should NOT be able to place order as a guest user.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I add products to my cart
    When I navigate to the page '<url>'
    When I proceed to checkout from the shopping bag
    Then I should be navigated to '<loginURL>'
    When I login as a guest user '<first_name>'
    And I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter '<phone_number>' in the Phone Number field
    And I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
      | 1 Test Street  | Test Road      | Test town | 1234         | P111111111111 |
    And the 'SAVE ADDRESS' button text is changed to '<address_saved>'
    And I click 'CONTINUE' then I am navigated to the 'REVIEW and PAY' page
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
    And I place the order
    Then I should get the error message '<error>' and order placement should get failed

    Examples:
      | country        | language | currency | url           |  loginURL         | first_name | title |last_name |phone_number  |address_saved| card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code | error |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in |  GuestUser |   Mr  | Test     | 0765432198   |ADDRESS SAVED| Visa             | 4000000000001018 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          |/shopping-bag  | /checkout/sign-in |  GuestUser |   Mr  | Test     | 0765432198   |ADDRESS SAVED| Visa             | 4000000000001042 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in |  GuestUser |   Mr  | Test     | 0765432198   |ADDRESS SAVED| Mastercard       | 5200000000001013 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in |  GuestUser |   Mr  | Test     | 0765432198   |ADDRESS SAVED| Mastercard       | 5200000000001047 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in |  GuestUser |   Mr  | Test     | 0765432198   |ADDRESS SAVED| American Express | 340000000001015  | ORDER TEST | 01           | 2022        | 1234           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in |  GuestUser |   Mr  | Test     | 0765432198   |ADDRESS SAVED| American Express | 340000000001049  | ORDER TEST | 01           | 2022        | 1234          |       |


  @3dSecure @regression

  Scenario Outline: (7) 475 Website, 3d-Secure, single-line, new shipping address, new card & new billing address,customer should be able to place order as a guest user with 3D-Secure challenge screen.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I add products to my cart
    When I navigate to the page '<url>'
    When I proceed to checkout from the shopping bag
    Then I should be navigated to '<loginURL>'
    When I login as a guest user '<first_name>'
    And I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter '<phone_number>' in the Phone Number field
    And I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
      | 1 Test Street  | Test Road      | Test town | 1234         | P111111111111 |
    And the 'SAVE ADDRESS' button text is changed to '<address_saved>'
    And I click 'CONTINUE' then I am navigated to the 'REVIEW and PAY' page
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
    And I place the order
    And I will get One Time Password pop-up enter One Time Password
    And I submit  One Time Password
    Then I should see the order confirmation page

    Examples:
      | country        | language | currency | url           | loginURL          | first_name | title | last_name | phone_number | address_saved | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code | error |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001091 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001117 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001125 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001133 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001096 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001112 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001120 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001138 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | American Express | 340000000001098  | ORDER TEST | 01           | 2022        | 1234          |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | American Express | 340000000001114  | ORDER TEST | 01           | 2022        | 1234          |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | American Express | 340000000001122  | ORDER TEST | 01           | 2022        | 1234          |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | American Express | 340000000001130  | ORDER TEST | 01           | 2022        | 1234          |       |



  @3dSecure @regression

  Scenario Outline: (8) 475 Website, 3d-Secure, single-line, new shipping address, new card & new billing address,customer should not be able to place order as a guest user with 3D-Secure challenge screen.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I add products to my cart
    When I navigate to the page '<url>'
    When I proceed to checkout from the shopping bag
    Then I should be navigated to '<loginURL>'
    When I login as a guest user '<first_name>'
    And I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter '<phone_number>' in the Phone Number field
    And I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
      | 1 Test Street  | Test Road      | Test town | 1234         | P111111111111 |
    And the 'SAVE ADDRESS' button text is changed to '<address_saved>'
    And I click 'CONTINUE' then I am navigated to the 'REVIEW and PAY' page
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
    And I place the order
    And I will get One Time Password pop-up enter One Time Password
    And I submit  One Time Password
    Then I should get the error message '<error>' and order placement should get failed

    Examples:
      | country        | language | currency | url           | loginURL          | first_name | title | last_name | phone_number | address_saved | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code | error |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001109 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001104 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | GuestUser  | Mr    | Test      | 0765432198   | ADDRESS SAVED | American Express | 340000000001106  | ORDER TEST | 01           | 2022        | 1234          |       |


    ##################################

  @3dSecure @regression
  Scenario Outline: (9) 475 Website, 3d-Secure, single-line, customer should be able to place order as a new user with 3D-Secure challenge screen.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I add products to my cart
    When I navigate to the page '<url>'
    When I proceed to checkout from the shopping bag
    Then I should be navigated to '<loginURL>'
    Given I continue with Create An Account
    When I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter new email '<first_name>' in the Email field
    And I enter '<password>' in the Password field
    And I enter '<password>' in the Confirm Password field
    And I enter '<phone_number>' in the Phone Number field
    And I click Create An Account
    And I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
      | 1 Test Street  | Test Road      | Test town | 1234         | P111111111111 |
    And the 'SAVE ADDRESS' button text is changed to '<address_saved>'
    And I click 'CONTINUE' then I am navigated to the 'REVIEW and PAY' page
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
    And I place the order
    And I will get One Time Password pop-up enter One Time Password
    And I submit  One Time Password
    Then I should see the order confirmation page

    Examples:
      | country        | language | currency | url           | loginURL          | first_name | title | last_name |  password  | phone_number  | address_saved | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code | error |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 |  0765432198   | ADDRESS SAVED | Visa             | 4000000000001091 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 |  0765432198   | ADDRESS SAVED | Visa             | 4000000000001117 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 |  0765432198   | ADDRESS SAVED | Visa             | 4000000000001125 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 |  0765432198   | ADDRESS SAVED | Visa             | 4000000000001133 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 |  0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001096 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 |  0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001112 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 |  0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001120 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 |  0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001138 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 |  0765432198   | ADDRESS SAVED | American Express | 340000000001098  | ORDER TEST | 01           | 2022        | 1234          |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 |  0765432198   | ADDRESS SAVED | American Express | 340000000001114  | ORDER TEST | 01           | 2022        | 1234          |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 |  0765432198   | ADDRESS SAVED | American Express | 340000000001122  | ORDER TEST | 01           | 2022        | 1234          |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 |  0765432198   | ADDRESS SAVED | American Express | 340000000001130  | ORDER TEST | 01           | 2022        | 1234          |       |


  @3dSecure @regression
  Scenario Outline: (10) 475 Website, 3d-Secure, single-line, customer should not be able to place order as a new user with 3D-Secure challenge screen.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I add products to my cart
    When I navigate to the page '<url>'
    When I proceed to checkout from the shopping bag
    Then I should be navigated to '<loginURL>'
    Given I continue with Create An Account
    When I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter new email '<first_name>' in the Email field
    And I enter '<password>' in the Password field
    And I enter '<password>' in the Confirm Password field
    And I enter '<phone_number>' in the Phone Number field
    And I click Create An Account
    And I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
      | 1 Test Street  | Test Road      | Test town | 1234         | P111111111111 |
    And the 'SAVE ADDRESS' button text is changed to '<address_saved>'
    And I click 'CONTINUE' then I am navigated to the 'REVIEW and PAY' page
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
    And I place the order
    And I will get One Time Password pop-up enter One Time Password
    And I submit  One Time Password
    Then I should get the error message '<error>' and order placement should get failed

    Examples:
      | country        | language | currency | url           | loginURL          | first_name | title | last_name | password   | phone_number | address_saved | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code | error |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001109 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001104 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | American Express | 340000000001106  | ORDER TEST | 01           | 2022        | 1234          |       |



  @3dSecure @regression
  Scenario Outline: (11) 476 Website, 3d-Secure, single-line, customer should not be able to place order as a NEW USER.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I add products to my cart
    When I navigate to the page '<url>'
    When I proceed to checkout from the shopping bag
    Then I should be navigated to '<loginURL>'
    Given I continue with Create An Account
    When I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter new email '<first_name>' in the Email field
    And I enter '<password>' in the Password field
    And I enter '<password>' in the Confirm Password field
    And I enter '<phone_number>' in the Phone Number field
    And I click Create An Account
    And I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
      | 1 Test Street  | Test Road      | Test town | 1234         | P111111111111 |
    And the 'SAVE ADDRESS' button text is changed to '<address_saved>'
    And I click 'CONTINUE' then I am navigated to the 'REVIEW and PAY' page
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
    And I place the order
    Then I should get the error message '<error>' and order placement should get failed

    Examples:
      | country        | language | currency | url           | loginURL          | first_name | title | last_name | password   | phone_number | address_saved | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code | error |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001018 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001042 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001013 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001047 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | American Express | 340000000001015  | ORDER TEST | 01           | 2022        | 1234          |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | American Express | 340000000001049  | ORDER TEST | 01           | 2022        | 1234          |       |



  @3dSecure @regression
  Scenario Outline: (12) 100 Website, 3d-Secure, single-line, customer should be able to place order as a NEW USER without 3D-Secure challenge screen.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I add products to my cart
    When I navigate to the page '<url>'
    When I proceed to checkout from the shopping bag
    Then I should be navigated to '<loginURL>'
    Given I continue with Create An Account
    When I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter new email '<first_name>' in the Email field
    And I enter '<password>' in the Password field
    And I enter '<password>' in the Confirm Password field
    And I enter '<phone_number>' in the Phone Number field
    And I click Create An Account
    And I use the 'ADD ADDRESS' form to create a new address for '<country>'
      | address_line_1 | address_line_2 | town_city | postcode_zip | pccc          |
      | 1 Test Street  | Test Road      | Test town | 1234         | P111111111111 |
    And the 'SAVE ADDRESS' button text is changed to '<address_saved>'
    And I click 'CONTINUE' then I am navigated to the 'REVIEW and PAY' page
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
    And I place the order
    Then I should see the order confirmation page

    Examples:
      | country        | language | currency | url           | loginURL          | first_name | title | last_name | password   | phone_number | address_saved | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code | error |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001000 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001026 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001034 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001059 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001067 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001075 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Visa             | 4000000000001083 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001005 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001021 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001039 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001054 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001062 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001070 | ORDER TEST | 01           | 2022        | 123           |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | Mastercard       | 5200000000001088 | ORDER TEST | 01           | 2022        | 123           |       |
      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | American Express | 340000000001007  | ORDER TEST | 01           | 2022        | 1234          |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | American Express | 340000000001023  | ORDER TEST | 01           | 2022        | 1234          |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | American Express | 340000000001031  | ORDER TEST | 01           | 2022        | 1234          |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | American Express | 340000000001056  | ORDER TEST | 01           | 2022        | 1234          |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | American Express | 340000000001064  | ORDER TEST | 01           | 2022        | 1234          |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | American Express | 340000000001072  | ORDER TEST | 01           | 2022        | 1234          |       |
#      | United Kingdom |          |          | /shopping-bag | /checkout/sign-in | matches    | Mr    | Test      | Matches123 | 0765432198   | ADDRESS SAVED | American Express | 340000000001080  | ORDER TEST | 01           | 2022        | 1234          |       |

##################################

  @3dSecure @regression 
  Scenario Outline: (13) 475 Website, 3d-Secure, single-line, existing customer should be able to place order with SAVED CARD and 3D-Secure challenge screen.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Payment Cards page
    And I have saved '<card_type>' credit card '<card_digits>' or place an order with '<card_number>' and '<expiry_year>' and '<security_code>' and '<challenge>'
    And I add products to my cart
    And I check out
    And I select the '<shipping_method>' billing address
    And I select the saved '<card_type>' credit card '<card_digits>'
    And I place the order
    And I will get One Time Password pop-up enter One Time Password
    And I submit  One Time Password
    Then I should see the order confirmation page

    Examples:
      | country        | language | currency | shipping_method | card_type        | card_number      | expiry_year | security_code | card_digits | challenge |
      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001091 | 2022        | 123           | 1091        | true      |
      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001096 | 2022        | 123           | 1096        | true      |
      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001098  | 2022        | 1234          | 1098        | true      |

  @3dSecure @regression
  Scenario Outline: (14) 100 Website, 3d-Secure, single-line, existing customer should be able to place order with SAVED CARD and without 3D-Secure challenge screen.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the Payment Cards page
    And I have saved '<card_type>' credit card '<card_digits>' or place an order with '<card_number>' and '<expiry_year>' and '<security_code>' and '<challenge>'
    And I add products to my cart
    And I check out
    And I select the '<shipping_method>' billing address
    And I select the saved '<card_type>' credit card '<card_digits>'
    And I place the order
    Then I should see the order confirmation page

    Examples:
      | country        | language | currency | shipping_method | card_type        | card_number      | expiry_year | security_code | card_digits | challenge|
      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001000 | 2022        | 123           | 1000        | false    |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001026 | 2022        | 123           | 1026        | false    |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001034 | 2022        | 123           | 1034        | false    |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001059 | 2022        | 123           | 1059        | false    |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001067 | 2022        | 123           | 1067        | false    |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001083 | 2022        | 123           | 1083        | false    |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000001075 | 2022        | 123           | 1075        | false    |
      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001005 | 2022        | 123           | 1005        | false    |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001021 | 2022        | 123           | 1021        | false    |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001039 | 2022        | 123           | 1039        | false    |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001054 | 2022        | 123           | 1054        | false    |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001062 | 2022        | 123           | 1062        | false    |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001070 | 2022        | 123           | 1070        | false    |
#      | United Kingdom |          |          | PREMIUM         | Mastercard       | 5200000000001088 | 2022        | 123           | 1088        | false    |
      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001007  | 2022        | 1234          | 1007        | false    |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001023  | 2022        | 1234          | 1023        | false    |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001031  | 2022        | 1234          | 1031        | false    |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001056  | 2022        | 1234          | 1056        | false    |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001064  | 2022        | 1234          | 1064        | false    |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001072  | 2022        | 1234          | 1072        | false    |
#      | United Kingdom |          |          | PREMIUM         | American Express | 340000000001080  | 2022        | 1234          | 1080        | false    |


  ##################################


  @3dSecure @regression @mona
  Scenario Outline: (15) 475 Website, 3d-Secure V1, single-line, existing customer, saved shipping address, new Visa card, saved billing address, customer should be able to place order as a registered user with 3D-Secure challenge screen.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I check out
    And I select the '<shipping_method>' billing address
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
#    And Secure SessionId should exist
    And I unselect the checkbox to not save the card by default
    And I place the order
    And I will get One Time Password pop-up for threeDSecureVone and enter One Time Password
    And I submit  One Time Password for threeDSecureVone
    Then I should see the order confirmation page

    Examples:
      | country        | language | currency | shipping_method | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code |
      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000000002 | ORDER TEST | 01           | 2020        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000000036 | ORDER TEST | 01           | 2020        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000180000000002 | ORDER TEST | 01           | 2020        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000260000000002 | ORDER TEST | 01           | 2020        | 123           |
      | United Kingdom |          |          | PREMIUM         | Mastercard             | 5200000000000007 | ORDER TEST | 01           | 2020        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Mastercard             | 5200000000000122 | ORDER TEST | 01           | 2020        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Mastercard             | 5200000000000031 | ORDER TEST | 01           | 2020        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Mastercard             | 5200180000000007 | ORDER TEST | 01           | 2020        | 123           |
#      | United Kingdom |          |          | PREMIUM         | Mastercard             | 5200260000000007 | ORDER TEST | 01           | 2020        | 123           |
      | United Kingdom |          |          | PREMIUM         | American Express             | 371449111020228 | ORDER TEST | 01           | 2020        | 1234           |
#      | United Kingdom |          |          | PREMIUM         | American Express             | 344400000000569 | ORDER TEST | 01           | 2020        | 1234          |





  @3dSecure @regression
  Scenario Outline: (16) 476 Website, 3d-Secure V1, single-line, existing customer, saved shipping address, new Visa card, saved billing address, customer should not be able to place order as a registered user with 3D-Secure challenge screen.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I check out
    And I select the '<shipping_method>' billing address
    And I select the 'Enter Card Details' radio button
    And I select '<card_type>' from the Select Card Type Dropdown List
    And I enter '<card_number>' in the Card Number field
    And I enter '<card_name>' in the Name On Card field
    And I select '<expiry_month>' and '<expiry_year>' from the Expires End Dropdown Lists
    And I enter '<security_code>' in the Security Code field
#    And Secure SessionId should exist
    And I unselect the checkbox to not save the card by default
    And I place the order
    And I will get One Time Password pop-up for threeDSecureVone and enter One Time Password
    And I submit  One Time Password for threeDSecureVone
    Then I should get the error message '<error>' and order placement should get failed

    Examples:
      | country        | language | currency | shipping_method | card_type        | card_number      | card_name  | expiry_month | expiry_year | security_code |error|
      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000000010 | ORDER TEST | 01           | 2020        | 123           |     |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000000028 | ORDER TEST | 01           | 2020        | 123           |     |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000008531947799 | ORDER TEST | 01           | 2020        | 123           |     |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000000000000093 | ORDER TEST | 01           | 2020        | 123           |     |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000180000000028 | ORDER TEST | 01           | 2020        | 123           |     |
#      | United Kingdom |          |          | PREMIUM         | Visa             | 4000260000000028 | ORDER TEST | 01           | 2020        | 123           |     |
      | United Kingdom |          |          | PREMIUM         | Mastercard             | 5200000000000015 | ORDER TEST | 01           | 2020        | 123           |     |
#      | United Kingdom |          |          | PREMIUM         | Mastercard             | 5200000000000023 | ORDER TEST | 01           | 2020        | 123           |     |
#      | United Kingdom |          |          | PREMIUM         | Mastercard             | 5641821000010028 | ORDER TEST | 01           | 2020        | 123           |     |
#      | United Kingdom |          |          | PREMIUM         | Mastercard             | 5200000000000098 | ORDER TEST | 01           | 2020        | 123           |     |
#      | United Kingdom |          |          | PREMIUM         | Mastercard             | 5200180000000023 | ORDER TEST | 01           | 2020        | 123           |     |
#      | United Kingdom |          |          | PREMIUM         | Mastercard             | 5200260000000023 | ORDER TEST | 01           | 2020        | 123           |     |
      | United Kingdom |          |          | PREMIUM         | American Express             | 340000000006022 | ORDER TEST | 01           | 2020        | 1234           |     |
#      | United Kingdom |          |          | PREMIUM         | American Express             | 340000000009299 | ORDER TEST | 01           | 2020        | 1234           |     |


