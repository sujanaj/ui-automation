#Author: Mona Agrawal
@essential @teamSecure
Feature: Manage Preference Page
  In order to update manage preferences
  A customer logged into the website
  Should be able to use the ManagePreference page

  @done @essential
  Scenario Outline: (1) An existing user should able to log into website and select and deselect the Manage Preferences consent

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I navigate to the My Account page
    And I click on 'MANAGE PREFERENCES' link on My Account page
    When I select all the Manage Preferences consent displayed
    And I click on Save Other Preferences button on Manage Preferences page
    Then I should able to save the Other Preferences Options as selected
    When I deselect all the Manage Preferences consent displayed
    And I click on Save Other Preferences button on Manage Preferences page
    Then I should able to save the Other Preferences Options as deselected

    Examples:
      | country           | language | currency |
      | United Kingdom    |          |          |
      | France            | Français |          |
      | France            | English  |          |
      | Korea Republic of | 한국어     |          |
      | Korea Republic of | English  |          |
      | Japan             | 日本語    |          |
      | Japan             | English  |          |


  @done @essential @failedTSEReg474
  Scenario Outline: (2) As a new user I should be able to create an account with promotional checkbox as selected.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    When I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter new email '<first_name>' in the Email field
    And I enter '<password>' in the Password field
    And I enter '<password>' in the Confirm Password field
    And I enter '<Mobile_number>' in the Phone Number field
    And I select the promotional email checkbox
    And I click Create An Account
    And I should be redirected to the homepage
    And I should be signed in
    And I navigate to the My Account page
    And I click on 'MANAGE PREFERENCES' link on My Account page
    Then Account should be created with Other Preferences Options as selected

    Examples:
      | country           | language | currency | title | first_name | last_name | password   | Mobile_number |
      | United Kingdom    |          |          | Mr    | matches    | autotest  | Matches123 | 5522244788    |
      | France            | English  |          | Mr    | matches    | autotest  | Matches123 | 5522244788    |
      | France            | Français |          | Mme   | matches    | autotest  | Matches123 | 5522244788    |
      | Korea Republic of | English  |          | Mrs   | matches    | autotest  | Matches123 | 5522244788    |
      | Korea Republic of | 한국어     |          | Mrs   | matches    | autotest  | Matches123 | 5522244788    |
      | Japan             | English  |          | Mrs   | matches    | autotest  | Matches123 | 5522244788    |
      | Japan             | 日本語    |          | Mrs   | matches    | autotest  | Matches123 | 5522244788    |

  @done @essential @failedTSEReg474
  Scenario Outline: (3) As a new user I should be able to create an account with promotional checkbox as deselected.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    When I select '<title>' from the Title Dropdown List for language '<language>'
    And I enter '<first_name>' in the First Name field
    And I enter '<last_name>' in the Last Name field
    And I enter new email '<first_name>' in the Email field
    And I enter '<password>' in the Password field
    And I enter '<password>' in the Confirm Password field
    And I enter '<Mobile_number>' in the Phone Number field
    And I deselect the promotional email checkbox
    And I click Create An Account
    And I should be redirected to the homepage
    And I should be signed in
    And I navigate to the My Account page
    And I click on 'MANAGE PREFERENCES' link on My Account page
    Then Account should be created with Other Preferences Options as deselected

    Examples:
      | country           | language | currency | title | first_name | last_name | password   | Mobile_number |
      | United Kingdom    |          |          | Mr    | matches    | autotest  | Matches123 | 5522244788    |
      | France            | English  |          | Mr    | matches    | autotest  | Matches123 | 5522244788    |
      | France            | Français |          | Mme   | matches    | autotest  | Matches123 | 5522244788    |
      | Korea Republic of | English  |          | Mrs   | matches    | autotest  | Matches123 | 5522244788    |
      | Korea Republic of | 한국어     |          | Mrs   | matches    | autotest  | Matches123 | 5522244788    |
      | Japan             | English  |          | Mrs   | matches    | autotest  | Matches123 | 5522244788    |
      | Japan             | 日本語    |          | Mrs   | matches    | autotest  | Matches123 | 5522244788    |
