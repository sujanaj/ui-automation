#Author: Mona Agrawal
@regression @account @myAccount @teamSecure
Feature: My Account
  In order to review personal information and order history
  A customer logged into the website
  Should be able to navigate through the My Account area of the website

  @done @essential @failedTSEReg474
  Scenario Outline: (1) If a customer is logged into the website then each of the links in the drop down list under their name should take them to the correct page.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    Then each dropdown link should take me to the correct page
      | link               | url                        |
      | Account            | /account                   |
      | Contact Details    | /account/contactdetails    |
      | Address Book       | /account/addressbook       |
      | Order History      | /account/orderhistory      |
      | Wishlist           | /account/wishlist          |
      | Manage Preferences | /account/managepreferences |
      | Cards              | /account/mycards           |
      | Credits            | /account/credits           |
      | Refer A Friend     | /account/referafriend      |
      | Sign Out           |                            |

    Examples:
      | country           | language | currency |
      | United Kingdom    |          |          |
      | France            | Français |          |
      | France            | English  |          |
      | Korea Republic of | 한국어     |          |
      | Korea Republic of | English  |          |
      | Japan             | 日本語    |          |
      | Japan             | English  |          |


  @done @essential @failedTSEReg474
  Scenario Outline: (2) If a customer is logged into the website then each of the links on the My Account page should take them to the correct page.

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    When I navigate to the My Account page
    Then each link should take me to the correct page
      | link               | url                        |
      | Contact Details    | /account/contactdetails    |
      | Address Book       | /account/addressbook       |
      | Order History      | /account/orderhistory      |
      | Wishlist           | /account/wishlist          |
      | Manage Preferences | /account/managepreferences |
      | Cards              | /account/mycards           |
      | Credits            | /account/credits           |

    Examples:
      | country           | language | currency |
      | United Kingdom    |          |          |
      | France            | Français |          |
      | France            | English  |          |
      | Korea Republic of | 한국어     |          |
      | Korea Republic of | English  |          |
      | Japan             | 日本語    |          |
      | Japan             | English  |          |