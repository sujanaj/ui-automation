# Author @Himaja
@regression @homepage @teamContent
Feature: Homepage
 In order to get more information exclusively about Mens or Womens section of the website
 A customer landing on the home page
 Should be able to see various contents and navigate to others sections of the website


 @regression
 Scenario Outline: Validation of the section 'Instant Outfit' on the homepage

  Given I am on the website with '<country>' '<language>' '<currency>'
  Then I navigate to the home page to see the 'INSTANT OUTFIT' section with each product having product image, designer name, product description and price
   | url     | title   | number_of_products   |
   | /mens   | <title> | <number_of_products> |
   | /womens | <title> | <number_of_products> |

  Examples:
   | country        | language |currency| title          |
   | United Kingdom | English  |        |INSTANT OUTFIT |
   | Korea Republic of          | 한국어      |        |데일리 스타일링 |
   | France         | Français |        |LE LOOK MINUTE |
   | Japan          | 日本語      |        |STYLING IDEA   |
##
 @regression
 Scenario Outline: Validation of the section 'The Style Report' on the homepage

  Given I am on the website with '<country>' '<language>' '<currency>'
  Then I navigate to the home page to see the 'The Style Report' section with a scrollbar
   | url     |
   | /mens   |
   | /womens |
  Examples:
   | country            |language|currency|
   | United Kingdom     |        |        |
   | United States      |        |        |
   | Japan              |        |        |
   | Japan              |日本語   |        |
   | Korea Republic of  |        |        |
   | Korea Republic of  |한국어    |        |
   | France             |        |        |
   | France             |Français|        |
#
#
 @regression
 Scenario Outline: Validation of the section 'The Style Report' on the homepage

  Given I am on the website with '<country>' '<language>' '<currency>'
  Then I navigate to the home page to see the 'The Style Report' section with a scrollbar
   | url     |
   | /mens   |
   | /womens |
  Examples:
   | country            |language|currency|
   | United Kingdom     |        |        |
   | United States      |        |        |
   | Japan              |        |        |
   | Japan              |日本語   |        |
   | Korea Republic of  |        |        |
   | Korea Republic of  |한국어    |        |
   | France             |        |        |
   | France             |Français|        |
##
###
### # Not done -> carousel arrows are sliding a little, will have to address this later.
# Scenario Outline: Validation of the sliding carousel on the homepage
#
#  Given I am on the website with '<country>' '<language>' '<currency>'
#  Then the carousel contains three counters, three images, next and previous arrows from which can be clicked to change the current image
#   | url 	    |  action  |
#   | /mens  	|	 next  |
#   | /mens 	| previous |
#   | /womens   | previous |
#   | /womens  	|	 next  |
#
#
#  Examples:
#   | country        |language|currency|
#   | United Kingdom |        |        |
#   | Korea Republic of  |    |        |
#   | France         |        |        |
#   | Japan          |        |        |

