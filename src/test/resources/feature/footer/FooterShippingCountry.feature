#Author : Himaja
@regression @footer @footerShippingCountry @teamCustomer
Feature: Shipping country validation on the footer
  In order to get information about the current shipping country and ability to change to other country from the their current page
  A visitor to the website
  Should be able to scroll down to the bottom of the page to know about the current shipping country and customer care numbers

  ############ SMOKE TEST ##########

  @smoke
  Scenario: Validation of changing the shipping country on the footer of the page

    Given I am on the website
    Then changing the country via the footer link causes my shipping country to be updated in the footer
      | country           |
      | Hong Kong         |
      | United States     |
      | United Kingdom    |
      | Korea Republic of |
      | France            |
      | Japan             |


  #############################################



  @regression
  Scenario Outline: Validation of shipping country and customer care numbers on the footer of the page

    Given I am on the website with '<country>' '<language>' '<currency>'
    Then changing the country via the footer link causes the '<phone_number>' and '<landline_number>' to be updated in the footer

    Examples:
      | country              | language | currency | phone_number                        | landline_number                            |
      | United Kingdom       |          |          | 0800 009 4123 (UK Toll Free)        | +44 (0)20 7022 0828                        |
      | United States        |          |          | 1-877-782-7317 (USA Toll Free)      | +44 (0)20 7022 0828                        |
      | Australia            |          |          | 1-800-836-284 (Australia Toll Free) | +44 (0)20 7022 0828                        |
      | Korea Republic of    |          |          | 080 822 0307                        | +44 (0)20 7022 0828                        |
      | France               |          |          | 0800 945 810 (numéro gratuit)       | +44 (0)20 7022 0828 (numéro international) |
      | Japan                | 日本語      |          | 0800 919 1268（日本国内フリーダイヤル、日本語対応)    | +44 (0)20 7022 0828                        |
      | Japan                |          |          | 0800 919 1268 (Japan Toll Free)     | +44 (0)20 7022 0828                        |


  @regression
  Scenario: Validation of landing on the same webpage during change in shipping country on the footer of the page

    Given I am on the website
    Then changing the country via the footer link always returns me to the same page
      | starting_page               | country           |
      | /mens                       | United Kingdom    |
      | /designers                  | United States     |
      | /just-in/just-in-this-month | Korea Republic of |
      | /gift-cards                 | France            |
