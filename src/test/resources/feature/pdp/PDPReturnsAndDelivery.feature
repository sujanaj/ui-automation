#Author: Dilbag
@regression @PDP @PDPReturnsAndDelivery @teamProduct
Feature: PDP Returns and Delivery
  In order to check returns and delivery costs plus other options
  A customer landed on PDP
  Should be able to access returns and delivery links/overlays

  @regression
  Scenario Outline: (1) Landing on the PDP of a product I should be able to access returns link and check the returns options for various countries and close the overlay.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the Returns link on the PDP
    Then the Returns overlay should be displayed
    And the Returns overlay page title is '<title>'
    And by changing country in the Returns overlay the return shipping charge should be updated
      | country            | charge    |
      | <deliveryCountry1> | <charge1> |
      | <deliveryCountry2> | <charge2> |
      | <deliveryCountry3> | <charge3> |

    Examples:
      | country           | language | currency          | title   | deliveryCountry1 | charge1 | deliveryCountry2    | charge2 | deliveryCountry3 | charge3 |
      | United Kingdom    |          | British Pound (£) | RETURNS | United Kingdom   | FREE    | Korea Republic of   | FREE    | Spain            | £8      |
      | France            | Français | British Pound (£) | RETOURS | Etats-Unis       | GRATUIT | Republique de Coree | GRATUIT | Espagne          | 8 £     |
      | Korea Republic of | 한국어      | US Dollar ($)     | 반품      | 대한민국             | 무료      | Republique de Coree | 무료 | Espagne          | 8 £     |

   @regression
  Scenario Outline: (2) Landing on the PDP of a product I should be able to access delivery link and check the delivery options for various countries.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I click on the Delivery link on the PDP
    Then the Delivery overlay should be displayed
    And the Delivery overlay page title is '<title>'
    And by changing country in the Delivery overlay the delivery shipping charge should be updated
      | country            | shipping          | charge    |
      | <deliveryCountry1> | <shippingMethod1> | <charge1> |
      | <deliveryCountry1> | <shippingMethod2> | <charge2> |
      | <deliveryCountry1> | <shippingMethod3> | <charge3> |
      | <deliveryCountry2> | <shippingMethod2> | <charge4> |

    Examples:
      | country        | language | currency          | currency | title     | deliveryCountry1 | shippingMethod1   | charge1 | shippingMethod2 | charge2 | shippingMethod3 | charge3 | deliveryCountry2 | charge4 |
      | United Kingdom |          | British Pound (£) | GBP      | DELIVERY  | Spain            | NEXT BUSINESS DAY | £15     | EXPRESS         | £12     | STANDARD        | £8      | Panama           | £20     |
      | France         | Français | British Pound (£) | GBP      | LIVRAISON | Espagne          | 24H               | £15     | EXPRESS         | £12     | STANDARD        | £8      | Panama           | £20     |

  @regression
  Scenario Outline: (3) If a customer is on a PDP, clicking the Back To Top link should scroll to the top of the page.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    When I scroll to the bottom of the page

    Examples:
      | country           | language          | currency         |
      | France            | Français          | US Dollar ($)    |
      | Korea Republic of | 한국어               | US Dollar ($)    |
      | Japan             | 日本語               | Japanese Yen (¥) |
      | United Kingdom    |                     | US Dollar ($)    |


			
