#Author: Dilbag Singh
@regression @pdp @pdpProductDetails @teamProduct
Feature: Product details
  In order to see more information about a product
  A customer landing on a product detail page
  Should be able to see product description, details and size and fit


  ########## SMOKE Test - Please donot remove############
  @smoke
  Scenario Outline: Landing on the PDP of a product I should see the Description tab expanded and be able to scroll to the bottom of it while Details and Size and Fit tabs are collapsed.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    Then I should see the Description tab expanded
    And I should see the Details tab collapsed
    And I should see the Size and Fit tab collapsed

    When I expand Details tab
    And I should see the Description tab collapsed
    And I should see the Size and Fit tab collapsed

    When I expand the Size and Fit tab
    And I should see the Description tab collapsed
    And I should see the Details tab collapsed

    Examples:
      | country           | language | currency         |
      | France            | Français | US Dollar ($)    |
#      | Korea Republic of | 한국어      | US Dollar ($)    |
#      | Japan             | 日本語      | Japanese Yen (¥) |
#      | United Kingdom    |          | Japanese Yen (¥) |


  #######################



  @regression
  Scenario Outline: Landing on the PDP of a product I should see the Description tab expanded and be able to scroll to the bottom of it while Details and Size and Fit tabs are collapsed.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    Then I should see the Description tab expanded
    And I should see the Details tab collapsed
    And I should see the Size and Fit tab collapsed

    When I expand Details tab
    And I should see the Description tab collapsed
    And I should see the Size and Fit tab collapsed

    When I expand the Size and Fit tab
    And I should see the Description tab collapsed
    And I should see the Details tab collapsed

    Examples:
      | country           | language | currency         |
      | France            | Français | US Dollar ($)    |
      | Korea Republic of | 한국어      | US Dollar ($)      |
      | Japan             | 日本語      | Japanese Yen (¥) |
      | United Kingdom    |          | British Pound (£) |
