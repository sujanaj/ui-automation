#Author: Dilbag Singh
@regression @PDP @PDPContactUs @teamCustomer
Feature: 'Contact us' on Product details page
  In order to get more information about a product by email or phone
  A customer landing on a product details page
  Should be able to fill out a personal details form and submit it to be contacted later
  #This scenario do not work on REGRESSION as there must be a change done by FE team to prevent default redirection.
  #which is yet to be done by them on REGRESSION.


  ######## SMOKE Test - Please donot remove #######
  #@smoke
  Scenario Outline: (3) Successful form submission for both Email and Phone options

    When I am on the website with '<country>' '<language>' '<currency>'
    And I go to 'instock' product page
    Then opening and submitting the Contact us form with valid details should display a success message

      | select | first_name | surname | emailaddress         | phone_number | type_of_enquiry             | subject | message | size | success_message                            | france_success_message             | france_select | countryplaceholder_text | france_type_of_enquiry       |
      | Email  | Matches    | test    | mfautotest@gmail.com | 07777777777  | Designer Collection Queries | test    | test    | S    | THANK YOU Your enquiry has been submitted. | MERCI Votre demande a été envoyée. | E-mail        | <country>               | Questions Sur Une Collection |
      | Phone  | Matches    | test    | mfautotest@gmail.com | 07777777777  | Size & Fit Queries          | test    | test    | S    | THANK YOU Your enquiry has been submitted. | MERCI Votre demande a été envoyée. | Téléphone     | <country>               | À propos des tailles         |
#   Please implement the last step by clicking the 'Contact us' link on PDP, entering data from the table above, click on Submit, validate success message and closing the overlay

    Examples:
      | country        | language | currency |
      | United Kingdom |          |          |
#      | United States  |          |          |
#      | France         |          |          |


  ##########################


  @regression
  Scenario Outline: (1) Validation of placeholder text on the 'Contact us' form for both Email and Phone options

  #please note that second test in the data table will currently fail as the phone_number field for phone option is not shown as a mandatory and is a live bug that requires developlment work. JIRA reference - EK-739

    When I am on the website with '<country>' '<language>' '<currency>'
    And I go to 'instock' product page
    And I click on the Contact us link
    Then the fields on the Contact us form should contain the correct placeholder text

      | select | first_name_placeholdertext   | surname_placeholdertext   | emailaddress_placeholdertext   | phone_number_placeholdertext         | country_placeholdertext   | enquiry_placeholdertext   | subject_placeholdertext   | message_placeholdertext   | sizeselector_placeholdertext   | france_select |
      | Email  | <first_name_placeholdertext> | <surname_placeholdertext> | <emailaddress_placeholdertext> | <phone_number_placeholdertext_phone> | <country_placeholdertext> | <enquiry_placeholdertext> | <subject_placeholdertext> | <message_placeholdertext> | <sizeselector_placeholdertext> | E-mail        |
      | Phone  | <first_name_placeholdertext> | <surname_placeholdertext> | <emailaddress_placeholdertext> | <phone_number_placeholdertext_phone> | <country_placeholdertext> | <enquiry_placeholdertext> | <subject_placeholdertext> | <message_placeholdertext> | <sizeselector_placeholdertext> | Téléphone     |

    And clicking on CLOSE should close the PDP Contact us overlay


    Examples:
      | country           | language | currency | first_name_placeholdertext | surname_placeholdertext | emailaddress_placeholdertext | phone_number_placeholdertext_phone | country_placeholdertext | enquiry_placeholdertext | subject_placeholdertext | message_placeholdertext  | sizeselector_placeholdertext |
      | United Kingdom    |          |          | First name*                | Surname*                | Your Email Address*          | Phone                              | United Kingdom          | Type of Enquiry         | Subject*                | Type message here        | SELECT SIZE                  |
      | France            | Français |          | Prénom*                    | Nom*                    | Votre adresse e-mail*        | Téléphone                          | France                  | Type de requête         | Objet*                  | Saisir votre message ici | CHOISIR UNE TAILLE           |
#      | Japan             | 日本語      |          | 名*                         | 姓*                      | メールアドレス*                     | 電話番号を入力してください                      | 日本                      | お問い合わせ内容*               | 件名*                     | メッセージを入力*                | サイズ選択                        |
      | Korea Republic of | 한국어      |          | 이름*                        | 성*                      | 이메일 주소*                      | 전화번호                               | 대한민국                    | 문의 유형                   | 제목*                     | 메시지를 입력해 주세요.            | 사이즈 선택                       |

  @regression
  Scenario Outline: (2) Error validation for blank and erroneous form submission for both Email and Phone options

  # please note that there are incorrect texts for few error messages that needs development work and which are tracked JIRA reference - EK-709

    When I am on the website with '<country>' '<language>' '<currency>'
    And I go to 'instock' product page
    And I click on the Contact us link
    And I click on SUBMIT on the Contact us form
    Then it should trigger the following messages
      | emailaddress_incorrect | select | first_name_error   | surname_error   | emailaddress_error       | phone_number_error   | subject_error   | message_error   | sizeselector_error   | countryplaceholder_text | france_select |
      |                        | Email  | <first_name_error> | <surname_error> | <emailaddress_error>     |                      | <subject_error> | <message_error> | <sizeselector_error> | <country>               | E-mail        |
      |                        | Phone  | <first_name_error> | <surname_error> | <emailaddress_error>     | <phone_number_error> | <subject_error> | <message_error> | <sizeselector_error> | <country>               | Téléphone     |
      | a.com                  | Email  | <first_name_error> | <surname_error> | <emailaddress_incorrect> |                      | <subject_error> | <message_error> | <sizeselector_error> | <country>               | E-mail        |
      | a.com                  | Phone  | <first_name_error> | <surname_error> | <emailaddress_incorrect> | <phone_number_error> | <subject_error> | <message_error> | <sizeselector_error> | <country>               | Téléphone     |

    Examples:
      | country           | language | currency | first_name_error          | surname_error           | emailaddress_incorrect                         | emailaddress_error                 | phone_number_error                     | subject_error           | message_error           | sizeselector_error         |
      | United Kingdom    |          |          | Please enter first name   | This field is required. | Email should be in valid format                | Please enter email                 | Please enter phone                     | This field is required. | This field is required. | Please enter size          |
      | France            | Français |          | Veuillez saisir un prénom | Champ obligatoire       | Le format de l’adresse e-mail n’est pas valide | Veuillez saisir une adresse e-mail | Veuillez saisir un numéro de téléphone | Champ obligatoire       | Champ obligatoire       | Veuillez saisir une taille |
      | Korea Republic of | 한국어      |          | 이름을 입력해 주세요.              | 필수 입력 정보입니다.            | 유효한 양식의 이메일을 입력해 주세요.                          | 이메일을 입력해 주세요.                      | Veuillez saisir un numéro de téléphone | 필수 입력 정보입니다.            | 필수 입력 정보입니다.            | 사이즈를 입력해 주세요.              |


  Scenario Outline: (3) Successful form submission for both Email and Phone options

    When I am on the website with '<country>' '<language>' '<currency>'
    And I go to 'instock' product page
    Then opening and submitting the Contact us form with valid details should display a success message

      | select | first_name | surname | emailaddress         | phone_number | type_of_enquiry             | subject | message | size | success_message                            | france_success_message             | france_select | countryplaceholder_text | france_type_of_enquiry       |
      | Email  | Matches    | test    | mfautotest@gmail.com | 07777777777  | Designer Collection Queries | test    | test    | S    | THANK YOU Your enquiry has been submitted. | MERCI Votre demande a été envoyée. | E-mail        | <country>               | Questions Sur Une Collection |
      | Phone  | Matches    | test    | mfautotest@gmail.com | 07777777777  | Size & Fit Queries          | test    | test    | S    | THANK YOU Your enquiry has been submitted. | MERCI Votre demande a été envoyée. | Téléphone     | <country>               | À propos des tailles         |
#   Please implement the last step by clicking the 'Contact us' link on PDP, entering data from the table above, click on Submit, validate success message and closing the overlay

    Examples:
      | country        | language | currency |
      | United Kingdom |          |          |
      | United States  |          |          |
      | France         |          |          |

   @regression
  Scenario Outline: (4) Product details validation on 'Contact us' form for Email and Phone options

    When I am on the website with '<country>' '<language>' '<currency>'
    And I go to 'instock' product page
    And The price displayed on the PDP is '<product_price>'
    And I select the '<size>' on the PDP
    And I click on the Contact us link
    Then The product image on the Contact us overlay should be displayed
    And The '<designer_name>' should be displayed on the Contact us overlay
    And Product Description '<product_description>' should be displayed on the Contact us overlay
    And The price displayed on the Contact us overlay is '<product_price>'
    And The text '<product_code>' followed by '<product>' should be displayed on the Contact us overlay
    And Size '<size>' should be preselected in the Contact us overlay size dropdown list

    Examples:
      | country           | language | currency          | product | size | designer_name | product_description               | product_price   | product_code  |
      | United Kingdom    |          | British Pound (£) | 1009197 | L    | ATM           | Slub cotton-jersey T-shirt        | £68             | Product Code: |
      | France            | Français | Euro (€)          | 1009197 | L    | ATM           | T-shirt en jersey de coton flammé | €74          | Code produit: |
      | Korea Republic of | 한국어      | Euro (€)          | 1009197 | L    | ATM           | 슬러브 코튼 저지 티셔츠                     | ₩ 77,590 / €61 | 상품 번호:        |
      | Japan             | 日本語      | Japanese Yen (¥)  | 1009197 | L    | ATM           | Slub cotton-jersey T-shirt        | ¥ 9,104          | 商品番号:         |
##
#
   @regression
  Scenario Outline: (5) Contact details validation on 'Contact us' form for Email and Phone options


    Given I am on the website with '<country>' '<language>' '<currency>'
    And I go to 'instock' product page
    And I click on the Contact us link
    Then I should see phone number '<phone_number>' on the Contact us overlay
    And I should see landline number '<landline_number>' on the Contact us overlay
    And I should see Contact us email address '<email_support>' option

    Examples:
      | country           | language | currency | phone_number                                                          | landline_number            | email_support                          |
      | United Kingdom    |          |          | Tel. 0800 009 4123 (UK Toll Free)                                     | Tel. +44 (0) 20 7022 0828  | Or email mystylist@matchesfashion.com  |
      | United States     |          |          | Tel. 1-877-782-7317 (USA Toll Free)                                   | Tel. +44 (0) 20 7022 0828  | Or email mystylist@matchesfashion.com  |
      | Australia         |          |          | Tel. 1-800-836-284 (Australia Toll Free)                              | Tel. +44 (0) 20 7022 0828  | Or email mystylist@matchesfashion.com  |
      | France            | Français |          | Tél. 0800 945 810 (Numéro d’appel gratuit basé au Royaume-Uni)        | Tél. +44 (0) 20 7022 0828  | Ou e-mail mystylist@matchesfashion.com |
      | Korea Republic of | 한국어      |          | 전화번호: 080 822 0307 (한국어 및 영어 상담 가능, 한국 내 지역 요금)                       | 전화번호: +44 (0) 20 7022 0828 | 이메일: mystylist@matchesfashion.com      |
      | Japan             | 日本語      |          | Tel： 0800 919 1268（日本国内フリーダイヤル、日本語対応)                                 | Tel： +44 (0)20 7022 0828   | メール： mystylist@matchesfashion.com      |