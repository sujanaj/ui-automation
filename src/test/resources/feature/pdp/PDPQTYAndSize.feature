#Author: Dilbag Singh
@regression @PDP @PDPQTYAndSize @regression @teamProduct
Feature: QTY and Size on PDP
  In order to add products to the bag or wishlist
  A customer landed on PDP
  Should be able to interact with quantity and size drop down lists

  @regression
  Scenario Outline: (1) Landing on the PDP of specific products I should be able to select size and quantity
    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    Then by clicking on the QUANTITY drop down list I will not be able to open it
    Then by clicking on the PDP size drop down list I can select different sizes
      | size |
      | XS   |
      | S    |
      | M    |
      | L    |
    And Quantity drop down list becomes active
    Then by clicking on the PDP quantity drop down list I can select up to ten units
      | quantity |
      | 1        |
      | 2        |
      | 3        |

    Examples:
      | country           | language | currency         |
      | France            | Français | US Dollar ($)    |
      | Korea Republic of | 한국어      | US Dollar ($)    |
      | Japan             | 日本語      | Japanese Yen (¥) |
      | United Kingdom    |          |  British Pound (£)|

   @regression
  Scenario Outline: (2) Landing on the PDP of a product which has only one size I should not be able to interact with size drop down list.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'oneSize' product page
    Then by clicking on the 'SIZE' drop down list I will not be able to open it

    Examples:
      | country           | language | currency         |
      | United Kingdom    |          |                  |
      | France            | Français | US Dollar ($)    |
      | Korea Republic of | 한국어      | US Dollar ($)    |
      | Japan             | 日本語      | Japanese Yen (¥) |


  @regression
  Scenario Outline: (3) Landing on the PDP of a product which has one of its sizes sold out I should be able to select that size and observe 'ADD TO BAG' button disappear.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'soldOut' product page
    Then by clicking on PDP 'SIZE' drop down list and selecting size '<size>' the 'ADD TO BAG' button should disappear
    And instead of the 'ADD TO BAG' button there should be a text message '<message>'

    Examples:
      | country           | language | currency      | size      | message                                                                                                           |
      | United Kingdom    |          |               | M-Sold    | Add sold out items to your wishlist and we'll email you if more stock becomes available.                          |
      | France            | Français |               | M-Article | Ajoutez les articles épuisés à votre wishlist et nous vous informerons par e-mail s'ils sont de nouveau en stock. |

  @regression
  Scenario Outline: (4) Landing on the PDP of a product which has only one unit in stock of a specific size I should not be able to interact with quantity drop down list.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    Then by clicking on the 'SIZE' drop down list and selecting size '<size>' I will not be able to open 'QUANTITY' drop down list

    Examples:
      | country           | language | currency         | size |
      | France            | Français | US Dollar ($)    | XS   |
      | Korea Republic of | 한국어      | US Dollar ($)    | XS   |
      | Japan             | 日本語      | Japanese Yen (¥) | XS   |


  @regression
  Scenario Outline: (5) Landing on the PDP of specific products I should be able to select size and quantity on the sticky nav wrapper

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I scroll down to the bottom of the page
    Then by clicking on the PDP 'SIZE' drop down list on the sticky nav wrapper I can select different sizes
      | size |
      | XS   |
      | S    |
      | L    |
    And 'QUANTITY' drop down list on the sticky nav wrapper becomes active
    Then by clicking on the PDP 'QUANTITY' drop down list on the sticky nav wrapper I can select up to ten units
      | quantity |
      | 1        |
      | 2        |
      | 3        |

    Examples:
      | country           | language | currency          |
      | France            | Français | US Dollar ($)     |
      | Korea Republic of | 한국어      | US Dollar ($)     |
      | Japan             | 日本語      | Japanese Yen (¥)  |
      | United Kingdom    |          | British Pound (£) |



  @regression
  Scenario Outline: (6) Landing on the PDP of a product which has more than one size I should not be able to interact with quantity drop down list before selecting the size on the sticky nav wrapper.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I scroll down to the bottom of the page
    Then by clicking on the 'QUANTITY' drop down list on the sticky nav wrapper I will not be able to open it

    Examples:
      | country           | language | currency         |
      | France            | Français | US Dollar ($)    |
      | Korea Republic of | 한국어      | US Dollar ($)    |
      | Japan             | 日本語      | Japanese Yen (¥) |
      | United Kingdom    |          |  British Pound (£)|

  @regression
  Scenario Outline: (7) Landing on the PDP of a product which has only one size I should not be able to interact with size drop down list on the sticky nav wrapper.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'oneSize' product page
    And I scroll down to the bottom of the page
    Then by clicking on the 'SIZE' drop down list on the sticky nav wrapper I will not be able to open it

    Examples:
      | country           | language | currency         |
      | France            | Français | US Dollar ($)    |
      | Korea Republic of | 한국어      | US Dollar ($)    |
      | Japan             | 日本語      | Japanese Yen (¥) |
      | United Kingdom    |          |  British Pound (£)|

  @regression
  Scenario Outline: (8) Landing on the PDP of a product which has one of its sizes sold out I should be able to select that size and observe 'ADD TO BAG' button disappear on the sticky nav wrapper.

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'soldOut' product page
    And I scroll down to the bottom of the page
    Then by clicking on PDP 'SIZE' drop down list on the sticky nav wrapper and selecting size '<size>' the 'ADD TO BAG' button should dissapear


    Examples:
      | country           | language | currency         | size |
      | France            | Français | US Dollar ($)    |      |
      | Korea Republic of | 한국어      | US Dollar ($)    |      |
      | Japan             | 日本語      | Japanese Yen (¥) |      |
      | United Kingdom    |          |  British Pound (£)|     |


  @regression
  Scenario Outline: (9) Landing on the PDP of a product which has only one unit in stock of a specific size I should not be able to interact with quantity drop down list on the sti
    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'soldOut' product page
    When I scroll down to the bottom of the page
    Then by clicking on the 'SIZE' drop down list on the sticky nav wrapper and selecting size '<size>' I will not be able to open 'QUANTITY' drop down list

    Examples:
      | country           | language | currency         | size |
      | France            | Français | US Dollar ($)    |      |
      | Korea Republic of | 한국어      | US Dollar ($)    |      |
      | Japan             | 日本語      | Japanese Yen (¥) |      |
      | United Kingdom    |          |  British Pound (£)|     |


  @regression
  Scenario Outline: (10) Landing on the PDP of a product and interacting with size and quantity should reflect the changes on the sticky nav wrapper

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I add '<quantity>' of size '<size>' to my bag from the PDP
    When I scroll down to the bottom of the page
    Then I should be able to see the same size '<size>' and quantity '<quantity>' on the sticky nav wrapper

    Examples:
      | country           | language | currency         | size | quantity |
      | France            | Français | US Dollar ($)    | L   | 2        |
      | Korea Republic of | 한국어      | US Dollar ($)    | XS   | 1        |
      | Japan             | 日本語      | Japanese Yen (¥) | S   | 2        |
      | United Kingdom    |          |  British Pound (£)|  M   |         1 |


  @regression
  Scenario Outline: (11) Landing on the PDP of a product on the France website in French language I should be able to see text'SELECT SIZE' in the size drop down list in French

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    Then I should be able to see the text '<text>' in the size drop down list
    And I scroll down to the bottom of the page
    Then I should be able to see the text '<text>' in the size drop down list

    Examples:
      | country           | language | currency          | text               |
      | United Kigdom     | English  | British Pound (£) | SELECT SIZE        |
      | Korea Republic of | 한국어      | US Dollar ($)     | 사이즈 선택             |
      | Japan             | 日本語      | Japanese Yen (¥)  | サイズ選択              |

  @regression
  Scenario Outline: (12) Landing on the PDP of a product which has only one size, I should be able to see text 'ONE SIZE' in the size drop down list

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'oneSize' product page
    Then I should be able to see the text '<text>' in the size drop down list
    And I scroll down to the bottom of the page
    Then I should be able to see the text '<text>' in the size drop down list

    Examples:
      | country           | language | currency          | text          |
      | United Kigdom     | English  | British Pound (£) | ONE SIZE   |
      | Korea Republic of | 한국어      | US Dollar ($)     | ONE SIZE       |
      | Japan             | 日本語      | Japanese Yen (¥)  | ONE SIZE         |