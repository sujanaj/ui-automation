#Author: Himaja
@regression @wishlist @wishlistSignInRedirect @teamProduct
Feature: Wishlist Sign In Redirect
	As a customer
	I want to be forced to sign into Matchesfashion Website when I try to access my Wishlist
	So that my Wishlist details is secured

 This feature focuses on the redirect to the login page when the user is not signed in and tries to access their wishlist page
 It also contains some negative scenario's when a size has not been selected and the error message 'Please select a size'is displayed

########## SMOKE Test- Please donot remove #####

  @smoke
  Scenario Outline: Add a product from PDP to wishlist when not signed in

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
    And I select the '<size>' on the PDP
    When I click on the 'ADD TO WISHLIST' button on the PDP
    Then I am redirected to the Sign In page
    And I log in with the following credentials
    And the rollover mini Wishlist list appears
    And I navigate to the Wishlist page
    When I click on the link '<remove_link>' for the first product in my Wishlist
    Then the first product is removed from my Wishlist

    Examples:
      | country| language| currency  | size | remove_link                |
      | United Kingdom    | English  |          | S    | Remove item from wishlist  |
      | France            | Français | US Dollar ($)         | S    | Supprimer de la wishlist  |
      | Japan             | 日本語      | Japanese Yen (¥)     | S    | 削除  |
      | Korea Republic of | 한국어      | Euro (€)    |S    | 위시리스트에서 상품 삭제 |



  ###################

 Scenario Outline: Access the Wishlist link from the homepage when not signed in

   Given I am on the website with '<country>' '<language>' '<currency>'
     When I click on the 'Wishlist' link in the header
     Then I am redirected to the Sign In page

    Examples:
      | country           | language |currency|
      | United Kingdom    | English  |        |
      | France            | Français |        |
      | Japan             | 日本語    |        |
      | Korea Republic of | 한국어     |        |

  Scenario Outline: Access the Wishlist link from the Shopping Bag when not signed in

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I add products to my cart
    And I proceed to my shopping bag
    When I click on the '<add_to_wishlist_link>' link on the Shopping Bag
    Then I am redirected to the Sign In page

    Examples:
      | country           | language | currency         | add_to_wishlist_link|
      | United Kingdom    | English  | US Dollar ($)    | Add To Wishlist     |
      | France            | Français | Euro (€)         | Ajouter à la wishlist|
      | Japan             | 日本語      | Japanese Yen (¥) |  欲しいものリストに登録 |
      | Korea Republic of | 한국어      | US Dollar ($)    | 위시리스트 담기|

 Scenario Outline: Error message when a size has not been selected on the PDP when a product is added to the Wishlist

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
     When I click on the 'ADD TO WISHLIST' button on the PDP
     Then the error message '<error_msg>' is displayed in red text below the PDP size drop down

    Examples:

     | country| language| currency |  error_msg|
     | United Kingdom    | English  |          |   Please select a size|
     | France            | Français | US Dollar ($)         |Veuillez sélectionner une taille|
     | Japan             | 日本語     | Japanese Yen (¥)     | サイズ選択 |
     | Korea Republic of | 한국어    | Euro (€)    |  사이즈를 선택해 주세요.     |



  Scenario Outline: Add a product from PDP to wishlist when not signed in

    Given I am on the website with '<country>' '<language>' '<currency>'
    When I go to 'instock' product page
      And I select the '<size>' on the PDP
     When I click on the 'ADD TO WISHLIST' button on the PDP
     Then I am redirected to the Sign In page
      And I log in with the following credentials
      And the rollover mini Wishlist list appears
      And I navigate to the Wishlist page
     When I click on the link '<remove_link>' for the first product in my Wishlist
     Then the first product is removed from my Wishlist

  Examples:
    | country| language| currency  | size | remove_link                |
    | United Kingdom    | English  |          | S    | Remove item from wishlist  |
    | France            | Français | US Dollar ($)         | S    | Supprimer de la wishlist  |
    | Japan             | 日本語      | Japanese Yen (¥)     | S    | 削除  |
    | Korea Republic of | 한국어      | Euro (€)    |S    | 위시리스트에서 상품 삭제  |


  Scenario Outline: Error message when a product is added to the Wishlist via the shop look overlay on the PDP

    Given I am on the website with '<country>' '<language>' '<currency>'
      And I go to 'instock' product page
      And I click on the 'SHOP THE LOOK' button on the PDP
      And the shop look overlay is displayed
     When I click on the 'ADD TO WISHLIST' button on the Shop The Look overlay
    Then the error message '<error_msg>' is displayed in red text below the PDP size drop down

    Examples:
      | country| language| currency |  error_msg|
     | United Kingdom    | English  |          |   Please select a size|
     | France            | Français | US Dollar ($)         |Veuillez sélectionner une taille|
     | Japan             | 日本語     | Japanese Yen (¥)     | サイズ選択 |
     | Korea Republic of | 한국어    | Euro (€)    |  사이즈를 선택해 주세요.     |


  Scenario Outline: Add a product via PDP shop the look overlay when not signed in

    Given I am on the website with '<country>' '<language>' '<currency>'
      And I go to 'instock' product page
      And I click on the 'SHOP THE LOOK' button on the PDP
      And the shop look overlay is displayed
      And I select size '<size>' from the Shop The Look size drop down
     When I click on the 'ADD TO WISHLIST' button on the Shop The Look overlay
     Then I am redirected to the Sign In page

    Examples:
      | country| language| currency | product | size |
      | United Kingdom    | English  |          | 1009197 | M    |
      | France            | Français | US Dollar ($)| 1009197|M  |
      | Japan             | 日本語      | Japanese Yen (¥) | 1009197 | M|
      | Korea Republic of | 한국어      | Euro (€)    | 1009197 | M|


  Scenario Outline: Error message when a product is added to the Wishlist via the sticky nav wrapper on the PDP

    Given I am on the website with '<country>' '<language>' '<currency>'
      And I go to 'instock' product page
      And I scroll to the bottom of the PDP
      And the sticky nav wrapper appears on the top of the PDP with the product details
     When I click on the 'ADD TO WISHLIST' button on the PDP sticky nav wrapper
     Then the error message '<error_msg>' is displayed in red text below the sticky nav wrapper size drop down

    Examples:
      | country           | language | currency         |error_msg|
      | United Kingdom    | English  | US Dollar ($)    |Please select a size|
      | France            | Français | Euro (€)         | Veuillez sélectionner une taille|
      | Japan             | 日本語      | Japanese Yen (¥) | サイズ選択 |
      | Korea Republic of | 한국어      | US Dollar ($)    | 사이즈를 선택해 주세요.     |


  Scenario Outline: Add a product via PDP sticky nav wrapper on the PDP when not signed in

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I go to 'instock' product page
    And I scroll to the bottom of the PDP
    And the sticky nav wrapper appears on the top of the PDP with the product details
    And I select size '<size>' from the sticky nav wrapper size drop down
    When I click on the 'ADD TO WISHLIST' button on the PDP sticky nav wrapper
    Then I am redirected to the Sign In page

    Examples:
      | country           | language | currency         | size |
      | United Kingdom    | English  | US Dollar ($)    | M    |
      | France            | Français | Euro (€)         | M    |
      | Japan             | 日本語      | Japanese Yen (¥) | M  |
      | Korea Republic of | 한국어      | US Dollar ($)    | M    |
