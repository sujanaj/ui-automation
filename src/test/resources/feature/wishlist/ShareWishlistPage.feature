#Author: Mahwish Ahmed
@regression @wishlist @shareWishlistPage @teamProduct
Feature: Share Wishlist Page
  As a customer
  I want to share my Wishlist with friends and family
  So that they can buy me something fabulous from Matches Fashion

# This feature tests the sharing of the Wishlist

#  @done @essential
#  Scenario Outline: (1) Preview of Share Wishlist email on the Share Wishlist page
#    Given I am on the website with '<country>' '<language>' '<currency>'
#    And I navigate to '/login'
#    And I log in with the following credentials
#    And I add products to my cart
#    And I navigate to '/shopping-bag'
#    When I click on the 'Add to wishlist' link on the Shopping Bag
#    When I navigate to the Wishlist page
#    Then sharing the first product in my wishlist displays the product details in the Share Wishlist email preview
#    Examples:
#      | country           | language | currency         |
#      | United States     |          | US Dollar ($)    |
#      | United Kingdom    |          |                  |
#      | France            | Français | US Dollar ($)    |
#      | Japan             | 日本語      | Japanese Yen (¥) |
#      | Korea Republic of | 한국어      | Euro (€)         |


#  @done @essential @Dilbag
#  Scenario Outline: (1) Preview of Share Wishlist email on the Share Wishlist page
#    Given I am on the website with '<country>' '<language>' '<currency>'
#    And I am signed in to the website as a customer
#    When I navigate to the Wishlist page
#    Then sharing the first product in my wishlist displays the product details in the Share Wishlist email preview
#    Examples:
#      | country           | language | currency         |
#      | United States     |          | US Dollar ($)    |
#      | United Kingdom    |          |                  |
#      | France            | Français | US Dollar ($)    |
#      | Japan             | 日本語      | Japanese Yen (¥) |
#      | Korea Republic of | 한국어      | Euro (€)         |


#  @done @essential @smoke
#  Scenario Outline:(2) Send Share Wishlist email using send button at the top of the Share Wishlist page
#
#    Given I am on the website with '<country>' '<language>' '<currency>'
#    And I am signed in to the website as a customer
#    And I navigate to the Wishlist page
#    And I tick the first product from my Wishlist
#    And I select '<wishlist_action>' from the drop down list
#    And I enter the following credentials in the Share Wishlist page
#	#TODO captcha check(disable flip.wishlistCaptcha in the meantime)
#    When I click on the 'SEND' button on the top of the Share Wishlist page
#    Then the Wishlist Sent overlay appears with title '<wishlist_sent_title>' and message '<message>'
#    And I click anywhere on the page outside of the 'WISHLIST SENT' overlay
#    And after a few seconds I am navigated back to my Wishlist page
#>>>>>>> ac098adb51043386944a5c87d846099aeea6f901

#  @done @essential @smoke
#  Scenario Outline:(2) Send Share Wishlist email using send button at the top of the Share Wishlist page
#
#    Given I am on the website with '<country>' '<language>' '<currency>'
#    And I navigate to '/login'
#    And I log in with the following credentials
#    And I add products to my cart
#    And I navigate to '/shopping-bag'
#    When I click on the 'Add to wishlist' link on the Shopping Bag
#    And I navigate to the Wishlist page
#    And I tick the first product from my Wishlist
#    And I select '<wishlist_action>' from the drop down list
#    And I enter the following credentials in the Share Wishlist page
#	#TODO captcha check(disable flip.wishlistCaptcha in the meantime)
#    When I click on the 'SEND' button on the top of the Share Wishlist page
#    Then the Wishlist Sent overlay appears with title '<wishlist_sent_title>' and message '<message>'
#    And I click anywhere on the page outside of the 'WISHLIST SENT' overlay
#    And after a few seconds I am navigated back to my Wishlist page
#
#    Examples:
#      | country        | language | currency | wishlist_action         | wishlist_sent_title | message                                                                                                                                              |
#      | United Kingdom |          |          | Share Wishlist          | WISHLIST SENT       | Any moment now, your friend will be able to see exactly what's on your MATCHESFASHION.COM wishlist.We hope you get what you wished for!              |
#      | France         | Français | Euro (€) | Partager votre wishlist | WISHLIST ENVOYÉE    | Dans quelques instants, votre ami(e) pourra découvrir votre wishlist MATCHESFASHION.COM. En vous souhaitant de recevoir les pièces dont vous rêvez ! |
#
#  @done @essential
#  Scenario: (3) Cancel button on the Share Wishlist section on the top of the Share Wishlist page
#
#    Given I am on the website with '<country>' '<language>' '<currency>'
#    And I navigate to '/login'
#    And I log in with the following credentials
#    And I add products to my cart
#    And I navigate to '/shopping-bag'
#    When I click on the 'Add to wishlist' link on the Shopping Bag
#	And I navigate to the Wishlist page
#	And I tick the first product from my Wishlist
#	And I click on the 'SHARE SELECTED ITEMS' button at the bottom of the Wishlist page
#	When I click on the 'CANCEL' button at the top of the Share Your Wishlist page
#	Then I am navigated back to the Wishlist page
#	And the first product that I ticked is no longer ticked in my Wishlist
#
#  @done @essential
#  Scenario: (4) Edit button on the share Wishlist section on the top of the Share Wishlist page
#
#    Given I am on the website with '<country>' '<language>' '<currency>'
#    And I navigate to '/login'
#    And I log in with the following credentials
#    And I add products to my cart
#    And I navigate to '/shopping-bag'
#    When I click on the 'Add to wishlist' link on the Shopping Bag
#	And I navigate to the Wishlist page
#	And I tick the first product from my Wishlist
#	And I click on the 'SHARE SELECTED ITEMS' button at the bottom of the Wishlist page
#	When I click on the 'EDIT' button at the top of the Share Your Wishlist page
#	Then I am navigated back to the Wishlist page
#	And the first product that I ticked is still ticked in my Wishlist
##
#  @done @detailed
#  Scenario Outline: (5) Input fields on the Share Wishlist page should have the correct placeholder text
#
#    Given I am on the website with '<country>' '<language>' '<currency>'
#    And I navigate to '/login'
#    And I log in with the following credentials
#    When I go to 'instock' product page
#    And I select the '<size>' on the PDP
#    When I click on the 'ADD TO WISHLIST' button on the PDP
#    And I navigate to the Wishlist page
#    And I tick the first product from my Wishlist
#    When I click on the 'SHARE SELECTED ITEMS' button at the bottom of the Wishlist page
#    Then I should see placeholder text '<recipients_name_placeholder>' , '<recipients_email_placeholder>' and '<message_placeholder>' in the fields in the form
#    And the Share Your Wishlist page should contain the text '<form_title>'
#    When I scroll to the bottom of the page
#
#    Examples:
#      | country           | language | currency | size | recipients_name_placeholder | recipients_email_placeholder    | message_placeholder | form_title           |
#      | United Kingdom    |          |          | S    | Recipient's name*           | Recipient's email address*      | Message*            | Share your wishlist  |
#      | France            | Français |          | S    | Nom du destinataire*        | Adresse e-mail du destinataire* | Message*            | Partager la wishlist |
#      | France            | English  |          | S    | Recipient's name*           | Recipient's email address*      | Message*            | Share your wishlist  |
#      | Korea Republic of | 한국어     |          | S    | 받는 사람의 이름*                  | 받는 사람의 이메일 주소*                  | 메시지*                | 위시리스트 공유하기           |
#      | Japan             | 日本語    |          | S    |                             | 受取人のメールアドレス*                    | メッセージは必須です          | 欲しいものリストをシェアする       |

#
#
#  @done @detailed
#  Scenario Outline: (5) Error message when a product has not been selected to be shared on the Wishlist page
#
#    Given I am on the website with '<country>' set as delivery country
#    And I navigate to '/login'
#    And I log in with the following credentials
#    When I go to 'instock' product page
#    And I select the '<size>' on the PDP
#    And I navigate to the Wishlist page
#    When I click on the 'SHARE SELECTED ITEMS' button at the bottom of the Wishlist page
#    Then the error message '<error_message>' is displayed in red text
#
#    Examples:
#      | country           |size| error_message                               |
#      | United Kingdom    | S   |Please select an item to share              |
#      | France            | S   |Veuillez sélectionner un article à partager |
#      | Korea Republic of | S   |공유할 상품을 선택해 주세요.                       |
#      | Japan             | S   |シェアするアイテムを選択してください             |
#
#  @done @detailed
#  Scenario Outline: (6) Error message when details have not been entered on the Share Wishlist page
#
#	Given I am on the website with '<country>' set as delivery country
#    And I navigate to '/login'
#    And I log in with the following credentials
#    When I go to 'instock' product page
#    And I select the '<size>' on the PDP
#	And I navigate to the Wishlist page
#	And I tick the first product from my Wishlist
#	And I click on the 'SHARE SELECTED ITEMS' button at the bottom of the Wishlist page
#	When I click 'SEND' button on the Share Wishlist form
#	Then I should see error messages '<name_error>' , '<email_error>' and '<message_error>' above the fields in the form
#
#	Examples:
#	  | country        | name_error                                 | email_error                                         | message_error                                      |
#	  | United Kingdom | Recipient Name Required                    | Recipient Email Required                            | Recipient Message Required                         |
#	  | France         | Vous devez indiquer le nom du destinataire | Vous devez indiquer l’adresse e-mail du destinataire | Vous devez rédiger un message pour le destinataire|
#
#  @done @detailed
#  Scenario Outline: Close Wishlist sent overlay message via the close icon
#
#    Given I am on the website with '<country>' set as delivery country
#    And I navigate to '/login'
#    And I log in with the following credentials
#    When I go to 'instock' product page
#    And I select the '<size>' on the PDP
#	And I navigate to the Wishlist page
#	And I tick the first product from my Wishlist
#	And I click on the 'SHARE SELECTED ITEMS' button at the bottom of the Wishlist page
#	And I enter the following credentials in the Share Wishlist page
#	  | Recipient_name | email_address             | message                |
#	  | Mahwish        | mfautotest+3001@gmail.com | Please buy this for me |
#		#TODO captcha check(disable flip.wishlistCaptcha in the meantime)
#	When I click on the 'SEND' button on the top of the Share Wishlist page
#	And the 'WISHLIST SENT' overlay appears
#	When I click on the 'CLOSE X' icon link on the right corner of the 'WISHLIST SENT' overlay
#	Then I am navigated back to my Wishlist page
#    Examples:
#      | country           |size| error_message                               |
#      | United Kingdom    | S   |Please select an item to share              |
#      | France            | S   |Veuillez sélectionner un article à partager |
#      | Korea Republic of | S   |공유할 상품을 선택해 주세요.                       |
#      | Japan             | S   |シェアするアイテムを選択してください             |
      
