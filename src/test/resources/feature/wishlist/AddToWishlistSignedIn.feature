
#Author: Mahwish Ahmed
@regression @wishlist @addToWishlistSignedIn @teamProduct

#Author: Himaja
@regression @wishlist @addToWishlistSignedIn @teamProduct
Feature: Wishlist Signed In
  As a customer
  I want to be able to add items to my wishlist
  So that I can save them for later
# This feature focuses on when a user is already signed into the website, adds items to their Wishlist and views their Wishlist


  ###### SMOKE Test- Please donot remove ###########

  @smoke
  Scenario Outline: (smoke-1) Add a product from the Shopping Bag to the Wishlist when signed in

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I navigate to '/shopping-bag'
    When I click on the '<add_to_wishlist_link>' link on the Shopping Bag
  #  Then the link text changes to '<productStatus>'
    And the rollover mini Wishlist list appears
    And I clean my shopping bag
    And I remove items from my wishlist

    Examples:
      | country           | language | currency         | productStatus         | add_to_wishlist_link|
#      | United States     |          | US Dollar ($)    | Added to wishlist     | Add To Wishlist     |
       | United Kingdom    |          |                  | Added to wishlist     | Add To Wishlist     |
#      | France            | Français | US Dollar ($)    | Ajouter à la wishlist | Ajouter à la wishlist|
#      | Japan             | 日本語      | Japanese Yen (¥) | 위시리스트에 담았습니다.         | 欲しいものリストに登録          |
#      | Korea Republic of | 한국어      | Euro (€)         | 위시리스트에 담았습니다.         | 위시리스트 담기             |



   @smoke
  Scenario Outline: (smoke-2) Add a product from the PDP to the Wishlist when signed in

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I go to 'instock' product page
    And I select the '<size>' on the PDP
    When I click on the 'ADD TO WISHLIST' button on the PDP
  #  Then the 'ADD TO WISHLIST' button text changes to '<first_change>'
    And the mini Wishlist appears
    When the mini Wishlist rollover list disappears
 #   Then the button 'ADD TO WISHLIST' changes to 'GO TO WISHLIST' '<second_change>'
    And I remove items from my wishlist

    Examples:
      | country           | language | currency         | size | first_change          | second_change    |
      | Australia         |          | US Dollar ($)    | L    | ADDED TO WISHLIST     | GO TO WISHLIST   |
      | France            | Français | US Dollar ($)    | S    | AJOUTER À LA WISHLIST | VOIR SA WISHLIST |
#      | France            | English  | US Dollar ($)    | S    | ADDED TO WISHLIST     | GO TO WISHLIST   |
#      | Japan             | 日本語      | Japanese Yen (¥) | S    | 欲しいものリストに登録           | 欲しいものリストに進む      |
#      | Korea Republic of | 한국어      | Euro (€)         | S    | 위시리스트에 담았습니다.         | 위시리스트 보기         |

  ###################

  @regression
  Scenario Outline: (1) Add a product from the Shopping Bag to the Wishlist when signed in

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I add products to my cart
    And I navigate to '/shopping-bag'
    When I click on the '<add_to_wishlist_link>' link on the Shopping Bag
 #   Then the link text changes to '<productStatus>'
    And the rollover mini Wishlist list appears
    And I clean my shopping bag
    And I remove items from my wishlist

    Examples:
      | country           | language | currency         | productStatus         | add_to_wishlist_link|
      | United States     |          | US Dollar ($)    | Added to wishlist     | Add To Wishlist     |
      | United Kingdom    |          |                  | Added to wishlist     | Add To Wishlist     |
      | France            | Français | US Dollar ($)    | Ajouter à la wishlist | Ajouter à la wishlist|
      | Japan             | 日本語      | Japanese Yen (¥) | 위시리스트에 담았습니다.         | 欲しいものリストに登録          |
      | Korea Republic of | 한국어      | Euro (€)         | 위시리스트에 담았습니다.         | 위시리스트 담기             |

  @regression
  Scenario Outline: (2) Add a product from the PDP to the Wishlist when signed in

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I go to 'instock' product page
    And I select the '<size>' on the PDP
    When I click on the 'ADD TO WISHLIST' button on the PDP
 #   Then the 'ADD TO WISHLIST' button text changes to '<first_change>'
    And the mini Wishlist appears
    When the mini Wishlist rollover list disappears
 #   Then the button 'ADD TO WISHLIST' changes to 'GO TO WISHLIST' '<second_change>'
    And I remove items from my wishlist

    Examples:
      | country           | language | currency         | size | first_change          | second_change    |
      | Australia         |          | US Dollar ($)    | S    | ADDED TO WISHLIST     | GO TO WISHLIST   |
      | France            | Français | US Dollar ($)    | S    | AJOUTER À LA WISHLIST | VOIR SA WISHLIST |
      | France            | English  | US Dollar ($)    | S    | ADDED TO WISHLIST     | GO TO WISHLIST   |
      | Japan             | 日本語      | Japanese Yen (¥) | S    | 欲しいものリストに登録           | 欲しいものリストに進む      |
      | Korea Republic of | 한국어      | Euro (€)         | S    | 위시리스트에 담았습니다.         | 위시리스트 보기         |


  @done @detailed @regression
  Scenario Outline: (3) Add a product to Wishlist via PDP shop the look overlay when signed in

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I go to 'instock' product page
    And I click on the 'SHOP THE LOOK' button on the PDP
    When I add the product size '<size>' from the 'SHOP THE LOOK' overlay
    When I click on the 'ADD TO WISHLIST' button on the Shop The Look overlay
  #  Then the first change '<first_change>' happens to Shop The look 'ADD TO WISHLIST' text button for 'instock'
    And the mini Wishlist appears
#    Then the second change '<second_change>' happens to Shop The look 'ADD TO WISHLIST' text button for 'instock'
    And I remove items from my wishlist


    Examples:
      | country           | language | currency         | size | quantity|first_change          | second_change    |
      | United Kingdom    |          | US Dollar ($)    | XS   |1 |ADDED TO WISHLIST     | GO TO WISHLIST   |
      | France            | Français | Euro (€)         | XS   |2 |Ajouter à la wishlist | VOIR SA WISHLIST |
      | France            | English  | Euro (€)         | XS   |1 |GO TO WISHLIST     | GO TO WISHLIST   |
      | Japan             | 日本語      | Japanese Yen (¥) | S    |1| 欲しいものリストに登録           | 欲しいものリストに進む      |
      | Japan             | English  | Japanese Yen (¥) | S    |2 |GO TO WISHLIST     | GO TO WISHLIST   |
      | Korea Republic of | 한국어      | Euro (€)         | S    | 2|위시리스트에 담았습니다.         | 위시리스트 보기         |
      | Korea Republic of | English  | Euro (€)         | S    | 1|ADDED TO WISHLIST     | GO TO WISHLIST   |

  @regression
  Scenario Outline: (4) Add a product via PDP sticky nav wrapper on the PDP when signed in

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I go to 'instock' product page
    When I scroll down to the bottom of the page
    And I select size '<size>' from the sticky nav wrapper size drop down
    When I click on the 'ADD TO WISHLIST' button on the PDP sticky nav wrapper
 #   Then the 'ADD TO WISHLIST' sticky nav button text changes to '<first_change>'
    And the mini Wishlist appears
 #   Then the sticky nav button 'ADD TO WISHLIST' changes to 'GO TO WISHLIST' '<second_change>'
    And I remove items from my wishlist


    Examples:
      | country           | language | currency         | size | first_change          | second_change    |
      | United Kingdom    |          |                  | XS   | Added to wishlist     | GO TO WISHLIST   |
#      | France            | Français | Euro (€)         | XS   | Ajouter à la wishlist | VOIR SA WISHLIST |
#      | France            | English  | Euro (€)         | XS   | Added to wishlist     | GO TO WISHLIST   |
#      | Japan             | English  | Japanese Yen (¥) | XS   | Added to wishlist     | GO TO WISHLIST   |
#      | Japan             | 日本語      | Japanese Yen (¥) | XS   | 欲しいものリストに登録           | 欲しいものリストに進む      |
#      | Korea Republic of | 한국어      | Euro (€)         | S    | 위시리스트에 담았습니다.         | 위시리스트 보기         |
#      | Korea Republic of | English  | Euro (€)         | S    | ADDED TO WISHLIST     | GO TO WISHLIST   |


  @done @detailed @bug @TPR-2 @regression
  Scenario Outline: (5) Add a product via the VIEW FULLSCREEN button on the PDP when signed in

    Given I am on the website with '<country>' '<language>' '<currency>'
    And I navigate to '/login'
    And I log in with the following credentials
    And I go to 'instock' product page
    And I click on the PDP 'VIEW FULLSCREEN' button
    And I select size '<size>' from the full screen product overlay size drop down
    When I click on the 'ADD TO WISHLIST' button on the full screen product overlay
 #   Then the first full screen product overlay 'ADD TO WISHLIST' button text changes to '<first_change>'
 #   And the second full screen product overlay 'ADD TO WISHLIST' button text changes to '<second_change>'
 #   And the mini Wishlist appears
    And I remove items from my wishlist
    Examples:
      | country           | language | currency         | size | first_change          | second_change    |
      | United Kingdom    |          |                  | XS   | Added to wishlist     | GO TO WISHLIST   |
 #     | France            | Français | Euro (€)         | XS   | Ajouter à la wishlist | VOIR SA WISHLIST |
      | France            | English  | Euro (€)         | XS   | Added to wishlist     | GO TO WISHLIST   |
      | Japan             | English  | Japanese Yen (¥) | XS   | Added to wishlist     | GO TO WISHLIST   |
      | Japan             | 日本語      | Japanese Yen (¥) | XS   | 欲しいものリストに登録           | 欲しいものリストに進む      |
      | Korea Republic of | 한국어      | Euro (€)         | S    | 위시리스트에 담았습니다.         | 위시리스트 보기         |
      | Korea Republic of | English  | Euro (€)         | S    | ADDED TO WISHLIST     | GO TO WISHLIST   |
