package commonfunctions;

import config.Constants;
import io.restassured.RestAssured;
import io.restassured.http.Header;
import io.restassured.http.Headers;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import utility.ExtentTestManager;

import java.util.ArrayList;
import java.util.List;

public class ApiSetUp {
    public static Response response;
    public String endpoint;
    public int responseCode;
    public RequestSpecification httpRequest;
    public RestAssured Rest;
    public Header header1;
    public Header header2;
    public Header header3;
    public Header header4;
    NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    ExtentTestManager extentTestManager = new ExtentTestManager();

    public RequestSpecification baseURI() {
        if (navigateUsingURL.getrelativeURLJenkins() == null) {
            if (extentTestManager.getCountryName().replaceAll("\n", "").equalsIgnoreCase("japan")) {
                Rest.baseURI = Constants.BASE_URL + "/jp";
            } else if (extentTestManager.getCountryName().replaceAll("\n", "").equalsIgnoreCase("france")) {
                Rest.baseURI = Constants.BASE_URL + "/fr";
            } else if (extentTestManager.getCountryName().replaceAll("\n", "").equalsIgnoreCase("Korea Republic Of")) {
                Rest.baseURI = Constants.BASE_URL + "/kr";
            } else {
                Rest.baseURI = Constants.BASE_URL;
            }
        } else {
            Rest.baseURI = navigateUsingURL.getrelativeURLJenkins();
        }

        return httpRequest = RestAssured.given();

    }


    public void setHeader() {
        header1 = new Header("Accept", "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8");
        header2 = new Header("user-agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3538.110 Safari/537.36");
        header3 = new Header("Accept-Encoding", "gzip, deflate, br");
        header4 = new Header("Accept-Language", "en-GB,en;q=0.9,en-US;q=0.8");

        List<Header> list = new ArrayList<>();
        list.add(header1);
        list.add(header2);
        list.add(header3);
        list.add(header4);
        Headers header = new Headers(list);
        httpRequest.headers(header);
    }

    public Response hitEndPoint() {
        endpoint = Constants.API_ENDPOINT_JSON;
        String API_ENDPOINT_JSON = System.getProperty("jsonAPIEndPointURL");
        if (API_ENDPOINT_JSON == null) {
            response = httpRequest.request(Method.GET, endpoint);
        } else {
            response = httpRequest.request(Method.GET, API_ENDPOINT_JSON);
        }

        return response;
    }

    public Response hitOneSizeProductEndpoint(){
        endpoint = Constants.ONE_ENDPOINT_SIZE_JSON;
        response = httpRequest.request(Method.GET, endpoint);
        return response;
    }

    public int responseCode() {
        responseCode = response.getStatusCode();
        return responseCode;
    }

}
