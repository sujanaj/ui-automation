package commonfunctions;
import org.apache.commons.lang3.StringUtils;

import jxl.common.Assert;
import org.apache.log4j.Logger;

import baseclass.BaseClass;

public class CustomVerification extends BaseClass {

	private StringBuffer verificationMessages;
	//final static Logger logger = Logger.getLogger(CustomVerification.class);

	public CustomVerification() {
		verificationMessages = new StringBuffer();
	}

	public void verifyTrue(String message, boolean condition) {
		try {
			Assert.verify(condition, message);
		} catch (Exception e) {
			verificationMessages.append("\n" + e.getMessage());
			//logger.error(message, e);
		}
	}

	public void verifyNoErrors() {
		org.junit.Assert.assertTrue("Fail Count: " + StringUtils.countMatches(verificationMessages.toString(), "\n") + " Failures: \n" + verificationMessages.toString(), StringUtils.isEmpty(verificationMessages.toString()));
	}

	public void verifyFalse(String string, boolean condition) {
		try {
			Assert.verify(!condition, string);
		} catch (Exception e) {
			verificationMessages.append("\n" + e.getMessage());
		}
	}

}
