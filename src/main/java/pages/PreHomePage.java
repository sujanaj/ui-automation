package pages;

import commonfunctions.CommonFunctions;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PreHomePage extends CommonFunctions {

    config.ObjPropertyReader obj_Property= new config.ObjPropertyReader();
    private String womensCategoryPictureLocator = obj_Property.readProperty("womensCategoryPictureLocator");
    private String mensCategoryPictureLocator = obj_Property.readProperty("mensCategoryPictureLocator");
    private String menMenuLinksListLocator = obj_Property.readProperty("menMenuLinksListLocator");
    private String womenMenuLinksListLocator = obj_Property.readProperty("womenMenuLinksListLocator");
    public void hoverCategory(String gender) {
        waitForPageToLoad();
        if (gender.contains("women"))
            mouseOverElement(womensCategoryPictureLocator);
        else
            mouseOverElement(mensCategoryPictureLocator);
        waitForPageToLoad();
    }

    public List<String> grabActualMenuList(String gender) {
        List<String> results = new ArrayList<String>();
        String cssLocator = "";
        waitForPageToLoad();
        if(gender.equalsIgnoreCase("men") || gender == "men"){
           cssLocator = menMenuLinksListLocator;
        }else
            cssLocator = womenMenuLinksListLocator;

        List<WebElement> elementList = getWebDriver().findElements(By.cssSelector(cssLocator));
        for (WebElement webElement : elementList) {
            webElement = webElement.findElement(By.tagName("a"));
            results.add(webElement.getAttribute("innerHTML"));
        }
        return results;
    }

    public List<String> getPrehomePageLinks(String language,String gender) {

        if(language.equalsIgnoreCase("Français")){
            return getPrehomePageLinksFrench(gender);
        }
        else if(language.equalsIgnoreCase("한국어")){
            return getPrehomePageLinksKorean(gender);
        }
        else if(language.equalsIgnoreCase("日本語")){
            return getPrehomePageLinksJapanese(gender);
        }
        else
            return getPrehomePageLinksEnglish(gender);
    }

    public List<String> getPrehomePageLinksEnglish(String gender){

        List<String> preHomePageLinks = new ArrayList<String>();
        preHomePageLinks.add("JUST IN");
        preHomePageLinks.add("DESIGNERS");
        preHomePageLinks.add("CLOTHING");
        preHomePageLinks.add("SHOES");
        preHomePageLinks.add("ACCESSORIES");
        preHomePageLinks.add("HOME");
        preHomePageLinks.add("STORIES");
        preHomePageLinks.add("WHAT'S ON");
        preHomePageLinks.add("SALE");
        if(gender.equalsIgnoreCase("women") || gender ==  "women"){
            preHomePageLinks.add(4,"BAGS");
        }
        return preHomePageLinks;
    }

    public List<String> getPrehomePageLinksFrench(String gender){

        List<String> preHomePageLinks = new ArrayList<String>();
            preHomePageLinks.add("NOUVEAUTÉS");
            preHomePageLinks.add("CRÉATEURS");
            preHomePageLinks.add("VÊTEMENTS");
            preHomePageLinks.add("CHAUSSURES");
            preHomePageLinks.add("ACCESSOIRES");
            preHomePageLinks.add("MAISON");
            preHomePageLinks.add("STORIES");
            preHomePageLinks.add("WHAT'S ON");
            preHomePageLinks.add("OUTLET");
            if(gender.equalsIgnoreCase("women") || gender == "women"){
                preHomePageLinks.add(4,"SACS");
            }
            return preHomePageLinks;
    }

    public List<String> getPrehomePageLinksKorean(String gender){

        List<String> preHomePageLinks = new ArrayList<String>();
            preHomePageLinks.add("신상품");
            preHomePageLinks.add("디자이너");
            preHomePageLinks.add("의류");
            preHomePageLinks.add("슈즈");
            preHomePageLinks.add("액세서리");
            preHomePageLinks.add("인테리어");
            preHomePageLinks.add("스토리");
            preHomePageLinks.add("라이브");
            preHomePageLinks.add("세일");
            if(gender.equalsIgnoreCase("women") || gender == "women"){
                preHomePageLinks.add(4,"백");
            }
            return preHomePageLinks;

    }

    public List<String> getPrehomePageLinksJapanese(String gender){

         // Change the links to japanese once they are translated
        List<String> preHomePageLinks = new ArrayList<String>();
        preHomePageLinks.add("JUST IN");
        preHomePageLinks.add("BRANDS");
        preHomePageLinks.add("CLOTHING");
        preHomePageLinks.add("SHOES");
        preHomePageLinks.add("ACCESSORIES");
        preHomePageLinks.add("HOME");
        preHomePageLinks.add("STORIES");
        preHomePageLinks.add("WHAT'S ON");
        preHomePageLinks.add("SALE");
        if(gender.equalsIgnoreCase("women") ||  gender == "women"){
            preHomePageLinks.add(4,"BAGS");
        }

        return preHomePageLinks;
    }

    public boolean checkIfIAmOnPreHomepage() {
        waitForPageToLoad();
        List<WebElement> bodyList = waitForListElementByCssLocator("body");
        if (bodyList.get(0).getAttribute("class").contains("page-homepage")) {
            return true;
        } else {
            return false;
        }
    }
}
