package pages.signin;

import baseclass.WebDriverFactory;
import commonfunctions.CommonFunctions;
import config.Constants;
import config.ObjPropertyReader;
import datamodel.account.RegistrationErrorModel;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import utility.CustomStringUtils;

import java.util.List;

public class RegisterSignInPage extends CommonFunctions {

    ObjPropertyReader objPropertyReader = new ObjPropertyReader();
    CustomStringUtils customStringUtils = new CustomStringUtils();
    WebDriverFactory webDriverFactory = new WebDriverFactory();
    private String passwordLocator = objPropertyReader.readProperty("passwordLocatorByName");
    private String checkPwdLocator = objPropertyReader.readProperty("checkPwdLocatorByName");
    private String createAccountBtnLocator = objPropertyReader.readProperty("createAccountBtnLocatorByClassName");
    private String titleDropdownLocatorByCss = objPropertyReader.readProperty("titleDropdownLocatorByCss");
    private String firstNameInputLocatorByName = objPropertyReader.readProperty("firstNameInputLocatorByName");
    private String lastNameInputLocatorByName = objPropertyReader.readProperty("lastNameInputLocatorByName");
    private String phoneNumberByName = objPropertyReader.readProperty("phoneNumberByName");
    private String emailLocator = objPropertyReader.readProperty("emailLocator");
    private String emailCheckBox = objPropertyReader.readProperty("emailCheckBox");
    public static  String loginErrorText;
    private String createAnAccountTitleLocator = objPropertyReader.readProperty("createAnAccountTitleLocator");
    private String titlePlaceholder = objPropertyReader.readProperty("titlePlaceholder");
    private String countryPlaceholder = objPropertyReader.readProperty("countryPlaceholder");
    private String registrationErrorMessage = objPropertyReader.readProperty("registrationErrorMessage");
    private String emailErrorExistingAccount = objPropertyReader.readProperty("emailErrorExistingAccount");
    private String staySignedInByLocator = objPropertyReader.readProperty("staySignedInByLocator");
    private String loginErrorByCSSLocator = objPropertyReader.readProperty("loginErrorByCSSLocator");
    private String createAccountContinueButton = objPropertyReader.readProperty("createAccountButtonLocatorByCss");

    public void selectTitle(String title,String language){
        waitForPageToLoad();
        waitForScriptsToLoad();
        if (language.equalsIgnoreCase("Français")){
            if (title.equalsIgnoreCase("M.")){
                ((JavascriptExecutor) getWebDriver()).executeScript("$(\"select[name='titleCode']\").val(\"mr\").change()");
            }else if (title.equalsIgnoreCase("Mme")) {
                ((JavascriptExecutor) getWebDriver()).executeScript("$(\"select[name='titleCode']\").val(\"ms\").change()");

            }
        } else {
            ((JavascriptExecutor) getWebDriver()).executeScript("$(\"select[name='titleCode']\").val(\"" + title.toLowerCase() + "\").change()");
        }
    }

    public void inputFirstName(String name) {
        waitForElementByCssLocator(firstNameInputLocatorByName);
        enterTextInputBox(firstNameInputLocatorByName,name);
    }

    public void inputLastName(String name) {
        waitForElementByCssLocator(lastNameInputLocatorByName);
        enterTextInputBox(lastNameInputLocatorByName,name);
    }

    public void inputEmail(String name) {
        waitForElementByCssLocator(emailLocator);
        enterTextInputBox(emailLocator,customStringUtils.generateRandomEmail(name));
    }

    public void inputPhone(String phone) {
        waitForElementByCssLocator(phoneNumberByName);
        enterTextInputBox(phoneNumberByName,phone);
    }


    public void inputPassword(String passwordText) {
        WebElement passwordInput = waitForElementByCssLocator(passwordLocator);
        passwordInput.sendKeys(passwordText);
    }

    public void inputConfirmPassword(String passwordText) {
        WebElement passwordInput = waitForElementByCssLocator(checkPwdLocator);
        passwordInput.sendKeys(passwordText);
    }

    public void inputExisitngEmail(){
        waitForElementByCssLocator(emailLocator);
        if (webDriverFactory.browser.equalsIgnoreCase(Constants.CHROME)) {
            enterTextInputBox(emailLocator,Constants.CHROME_USERNAME);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.SAFARI)) {
            enterTextInputBox(emailLocator,Constants.CHROME_USERNAME);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.FIREFOX)) {
            enterTextInputBox(emailLocator,Constants.CHROME_USERNAME);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.IE)) {
            enterTextInputBox(emailLocator,Constants.CHROME_USERNAME);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.EDGE)) {
            enterTextInputBox(emailLocator,Constants.CHROME_USERNAME);
        }
    }

    public void inputPasswordForExitingEmail(){
        if (webDriverFactory.browser.equalsIgnoreCase(Constants.CHROME)) {
            inputPassword(Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.SAFARI)) {
            inputPassword(Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.FIREFOX)) {
            inputPassword(Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.IE)) {
            inputPassword(Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.EDGE)) {
            inputPassword(Constants.CHROME_PASSWORD);
        }
    }

    public void inputConfirmPasswordForExitingEmail(){
        if (webDriverFactory.browser.equalsIgnoreCase(Constants.CHROME)) {
            inputConfirmPassword(Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.SAFARI)) {
            inputConfirmPassword(Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.FIREFOX)) {
            inputConfirmPassword(Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.IE)) {
            inputConfirmPassword(Constants.CHROME_PASSWORD);
        } else if (webDriverFactory.browser.equalsIgnoreCase(Constants.EDGE)) {
            inputConfirmPassword(Constants.CHROME_PASSWORD);
        }
    }

    private WebElement getEmaileable() {
        waitForPageToLoad();
        //emaileable = getW.findElement(By.cssSelector(emailCheckBox));
        return getWebElement(emailCheckBox);
    }

    public void selectEmailable(){
        doSelectEmailable(!getEmaileable().isSelected());
    }

    private void doSelectEmailable(boolean select) {
       // nativeScrollToElement(emaileable);
        scrollToElement(emailCheckBox);
        if (select) {
           // toggleCheckbox(emaileable);
            selectRadioButton(emailCheckBox);
        }
    }


    public void deselectEmailable(){
        doSelectEmailable(getEmaileable().isSelected());
    }

    public void clickOnContinueCreateAnAccountButton() {
        clickElementByCSS(createAccountContinueButton);
        waitForScriptsToLoad();
    }

    public void clickOnCreateAnAccount() {
        waitForPageToLoad();
      selectRadioButton(createAccountBtnLocator);
        waitForPageToLoad();
        waitForPageToLoad(); // more wait required for safari
    }

    public String grabTitle() {
    return getWebElement(titleDropdownLocatorByCss).findElement(By.cssSelector(".cs__selected")).getText();
    }

    public String grabFirstName() {
        return getWebElement(firstNameInputLocatorByName).getAttribute("value");
    }

    public String grabLastName() {
        return getWebElement(lastNameInputLocatorByName).getAttribute("value");
    }

    public String grabPhone() {
        return getWebElement(phoneNumberByName).getAttribute("value");
    }

    public String getLoginError(){
       return loginErrorText =getWebDriver().findElement(By.cssSelector(loginErrorByCSSLocator)).getText();

    }

    public String grabCreateAnAccountTitle(){ return getTextByCssSelector(createAnAccountTitleLocator); }

    public String getTitlePlaceholder(){ return getTextByCssSelector(titlePlaceholder); }

    public String getFirstNamePlaceholder(){ return  waitForElementByCssLocator(firstNameInputLocatorByName).getAttribute("placeholder"); }

    public String getLastNamePlaceholder(){return waitForElementByCssLocator(lastNameInputLocatorByName).getAttribute("placeholder");}

    public String getEmailPlaceholder(){return waitForElementByCssLocator(emailLocator).getAttribute("placeholder");}

    public String getPasswordPlaceholder(){return waitForElementByCssLocator(passwordLocator).getAttribute("placeholder");}

    public String getConfirmPasswordPlaceholder(){return waitForElementByCssLocator(checkPwdLocator).getAttribute("placeholder");}

    public String getCountryPlaceholder(){return getTextByCssSelector(countryPlaceholder);}

    public String getMobileNumberPlaceholder(){return waitForElementByCssLocator(phoneNumberByName).getAttribute("placeholder");}

    public RegistrationErrorModel setRegistrationErrorMessage(){

        List<WebElement> errorList = waitForListElementByCssLocator(registrationErrorMessage);
        RegistrationErrorModel registrationErrorModel= new RegistrationErrorModel();

        for (WebElement webElement : errorList) {
            String forField = webElement.getAttribute("for");

            if (forField.contains("title"))
                registrationErrorModel.setTitle(webElement.getText().trim());
            if (forField.contains("firstName"))
                registrationErrorModel.setFirstName(webElement.getText().trim());
            if (forField.contains("lastName"))
                registrationErrorModel.setLastName(webElement.getText().trim());
            if (forField.contains("email"))
                registrationErrorModel.setEmail(webElement.getText().trim());
            if (forField.contains("pwd"))
                registrationErrorModel.setPassword(webElement.getText().trim());
            if (forField.contains("checkPwd"))
                registrationErrorModel.setCheckPassword(webElement.getText().trim());
            if (forField.contains("phone"))
                registrationErrorModel.setPhone(webElement.getText().trim());
        }
        return registrationErrorModel;

    }

    public String getEmailErrorExistingAccount(){
        return retrieveText(emailErrorExistingAccount);
    }


    public void selectStaySignedIn(){
        if(!getWebElement(staySignedInByLocator).isSelected()){
            selectRadioButton(staySignedInByLocator);
            waitForPageToLoad();
        }
    }
}



