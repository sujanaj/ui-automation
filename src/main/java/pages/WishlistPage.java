package pages;


import baseclass.WebDriverFactory;
import commonfunctions.CommonFunctions;
import config.Constants;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class WishlistPage extends CommonFunctions {


    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    private String removeFromWishlistLinkLocator = obj_Property.readProperty("removeFromWishlistByCSS");
    String leftSideMenuLinksLocator = obj_Property.readProperty("leftSideMenuLinksAccountByCSS");
    String wishlistColumnNamesListLocator = obj_Property.readProperty("wishlistColumnNamesListLocatorByCSS");
    String wishlistProductsCheckbox = obj_Property.readProperty("wishlistProductsCheckboxByCSS");
    String wishlistManageSelectedDD = obj_Property.readProperty("wishlistManageSelectedDDByCSS");
    String addToCartButtonLocator = obj_Property.readProperty("wishlistAddToBagLocatorByCSS");
    String deleteOverlayonWishlist = obj_Property.readProperty("wishlistDeleteOverLayByID");
    String wishlistLoadMoreBtn = obj_Property.readProperty("wishlistLoadMoreBtnByID");
    String wishlistContainerLocatorByCSS = obj_Property.readProperty("wishlistContainerLocatorByCSS");
    String wishlistItemRowByCSS = obj_Property.readProperty("wishlistItemRowByCSS");
    String wishlistItemThumbByCSS = obj_Property.readProperty("wishlistItemThumbByCSS");
    String wishlistItemDescriptionByCSS = obj_Property.readProperty("wishlistItemDescriptionByCSS");
    String wishlistItemSizeByCSS = obj_Property.readProperty("wishlistItemSizeByCSS");
    String wishlistItemColorByCSS = obj_Property.readProperty("wishlistItemColorByCSS");
    String wishlistItemUnitPriceByCSS = obj_Property.readProperty("wishlistItemUnitPriceByCSS");
    String wishlistcheckboxesListLocatorByCSS = obj_Property.readProperty("wishlistcheckboxesListLocatorByCSS");
    String wishlistCheckboxes = obj_Property.readProperty("wishlistCheckboxes");
    String manageSelectedDropDownByCSS = obj_Property.readProperty("manageSelectedDropDownByCSS");
    String wishlistActionsDropdown = obj_Property.readProperty("wishlistActionsDropdownCloseByCSS");
    String wishlistActionsDropdownOpen = obj_Property.readProperty("wishlistActionsDropdownOpenByCSS");

    public WebElement getWebElementByJQuery(final String jQuery, String link) {
        Object jqueryResult = ((JavascriptExecutor) getWebDriver()).executeScript("return Array.from($('" + jQuery + "'))");
        for (int i = 0; i < ((List) jqueryResult).size(); i++) {
            if (((WebElement) ((List) jqueryResult).get(i)).getText().trim().equalsIgnoreCase(link)||(((WebElement) ((List) jqueryResult).get(i)).getText().trim().equalsIgnoreCase("Added to wishlist"))) {
                return (WebElement) ((List) jqueryResult).get(i);
            }
        }
        return null;
    }

    public List<String> getFirstProductRelevantDetails() {
        waitForPageToLoad();
        WebElement firstProd = waitForElementByCssLocator(wishlistContainerLocatorByCSS).findElements(By.cssSelector(wishlistItemRowByCSS)).get(0);
        List<String> firstProdDetailsList = new ArrayList<String>();
        String productImageLink = firstProd.findElement(By.cssSelector(wishlistItemThumbByCSS)).getAttribute("src");
        firstProdDetailsList.add(productImageLink);
        String productDescription = firstProd.findElement(By.cssSelector(wishlistItemDescriptionByCSS)).getText().replaceAll(
                "\\s+", "");
        firstProdDetailsList.add(productDescription);
        String productSize = firstProd.findElement(By.cssSelector(wishlistItemSizeByCSS)).getText();
        firstProdDetailsList.add(productSize.replace("Size:", StringUtils.EMPTY).trim());
        String productColour = firstProd.findElement(By.cssSelector(wishlistItemColorByCSS)).getText();
        firstProdDetailsList.add(productColour.replace("Colour:", StringUtils.EMPTY).trim());
        String productPrice = firstProd.findElement(By.cssSelector(wishlistItemUnitPriceByCSS)).getText().replaceAll("\\s+", "");
        firstProdDetailsList.add(productPrice);
        return firstProdDetailsList;
    }

    public void selectWishlistFirstProduct() {
        String wishlistcheckboxesListLocatorByCSS = obj_Property.readProperty("wishlistcheckboxesListLocatorByCSS");
        waitForPageToLoad();
        List<WebElement> checkboxList = waitForListElementByCssLocator(wishlistcheckboxesListLocatorByCSS);
        for (WebElement checkbox : checkboxList.subList(0, 1)) {
            String id = checkbox.findElement(By.cssSelector(".wishlistItem")).getAttribute("id");
            if (checkbox.getAttribute("for").contains(id)) {
                clickWebElementByJSE(checkbox);
            }
        }
    }

    public List<String> getProductRelevantDetailsOnShareWishlistPage() {
        waitForPageToLoad();
        WebElement firstProd = getWebDriver().findElements(By.cssSelector(".wishlist__email-preview tbody tbody tbody tr:nth-child(2)")).get(1);
        List<String> firstProdDetailsList = new ArrayList<String>();
        String productLink = firstProd.findElement(By.cssSelector("img")).getAttribute("src");
        firstProdDetailsList.add(productLink);
        String productDescription = firstProd.findElement(By.cssSelector("td:nth-child(2)")).getText().replaceAll(
                "\\s+", "");
        firstProdDetailsList.add(productDescription);
        String productSize = firstProd.findElement(By.cssSelector("td:nth-child(3)")).getText().trim();
        firstProdDetailsList.add(productSize);
        String productColour = firstProd.findElement(By.cssSelector("td:nth-child(4)")).getText().trim();
        firstProdDetailsList.add(productColour);
        String unitPrice = firstProd.findElement(By.cssSelector("td:nth-child(5)")).getText().replaceAll("\\s+", "");
        if (unitPrice.contains("{{#if")) {
            unitPrice = unitPrice.substring(0, unitPrice.indexOf("{{#if"));
        }
        unitPrice = unitPrice.contains("Now") ? unitPrice.split("Now")[1].split(
                StringUtils.SPACE)[0] : unitPrice;
        String[] unitprice = unitPrice.split("/");
        firstProdDetailsList.add(unitprice[1]);
        return firstProdDetailsList;
    }

    public String getFirstItemID(String cssLocator) {
        waitForPageToLoad();
        List<WebElement> rowsList = waitForListElementByCssLocator(".wishlist__item-row");
        if (rowsList.size() > 0) {
            return rowsList.get(0).findElement(By.cssSelector(cssLocator)).getAttribute("id");

        } else {
            return "";
        }
    }

    public boolean isFirstCheckboxSelected(String PRODUCT_CHECKBOX_LOCATOR) {
        waitForPageToLoad();
        List<WebElement> checkboxList = getWebDriver().findElements(By.cssSelector(PRODUCT_CHECKBOX_LOCATOR));
        // Boolean checkbox = false;
        Boolean isCheckboxSelected = false;
        for (int i = 0; i <= checkboxList.size() - 1; i++) {
            isCheckboxSelected = checkboxList.get(0).isSelected();
        }
        return isCheckboxSelected;
    }

    public String isCheckboxSelected() {
        List<WebElement> checkboxList = getWebDriver().findElements(By.cssSelector(".wishlist__item-check .control-label"));
        String attri = checkboxList.get(0).getCssValue("checkbox");
        return attri;
    }

    public boolean isRemoveLinkCorrect(String expectedLink) {
        waitForPageToLoad();
        waitForPageToLoad();
        List<WebElement> linkList = getWebDriver().findElements(By.cssSelector(removeFromWishlistLinkLocator + " .caption"));
        for (WebElement link : linkList) {
            waitForPageToLoad();
            if (link.getText().equals(expectedLink)) {
                return true;
            }
        }
        return false;
    }

    public int getFirstNoOfItem() {
        return getNoOfWishlistItems();
    }

    public int getNoOfWishlistItems() {
        int NoOfItem = waitForListElementByCssLocator(".wishlist__item-row").size();
        return NoOfItem;
    }

    public void clickOnFirstRemoveLink() {
        waitForPageToLoad();
        List<WebElement> rowsList = waitForListElementByCssLocator(".wishlist__item-row");
        if (rowsList.size() > 0) {
            WebElement removeLink = rowsList.get(0).findElement(By.cssSelector("#removeFromWishlistButton"));
            clickWebElementByJSE(removeLink);
        }
    }

    public void clickOnLeftSideMenuLinks(String linkName) {
        waitForPageToLoad();
        List<WebElement> linksList = waitForListElementByCssLocator(leftSideMenuLinksLocator);
        for (WebElement webElement : linksList) {
            if (webElement.getText().toLowerCase().contains(linkName.toLowerCase())) {
                webElement.click();
                break;
            }
        }
    }

    public List<String> getColumnNames() {
        waitForPageToLoad();
        List<WebElement> columnsList = getWebDriver().findElements(By.cssSelector(wishlistColumnNamesListLocator));
        List<String> columnNames = new ArrayList<String>();
        for (WebElement column : columnsList) {
            columnNames.add(column.getText().replaceAll("[\n]", "").trim());
        }
        return columnNames;
    }

    public boolean areAllCheckboxesSelected() {
        waitForPageToLoad();
        List<WebElement> checkboxList = getWebDriver().findElements(By.cssSelector(wishlistProductsCheckbox));
        for (WebElement webElement : checkboxList) {
            //WebElement id =	waitForElementByCssLocator(wishlistCheckboxes);
            if (webElement.isSelected()) {
                return true;
            }
        }
        return false;
    }

    public boolean isManageSelectedDropDownEnabled() {
        waitForPageToLoad();
        WebElement dropDown = waitForElementByCssLocator(wishlistManageSelectedDD);
        if (dropDown.getAttribute("class").contains("disabled")) {
            return false;
        } else {
            return true;
        }
    }

    public boolean isDeleteOverlayDisplayed() {
        if (getWebElement(deleteOverlayonWishlist).isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }

    public List<String> getWishlistItemIdList() {
        waitForPageToLoad();
        List<WebElement> checkboxList = waitForListElementByCssLocator(wishlistProductsCheckbox);
        List<String> itemIdList = new ArrayList<String>();
        for (WebElement webElement : checkboxList) {
            String idAttribute = webElement.getAttribute("id");
            itemIdList.add(idAttribute);
        }
        return itemIdList;
    }

    public void checkNumberOfEntries(int expectedNumberOfEntries) {
        waitForPageToLoad();
        getWebDriverWait().until(productsLoadedOnPage(expectedNumberOfEntries));
    }

    private ExpectedCondition<Boolean> productsLoadedOnPage(final int expectedNumberOfEntries) {
        return new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                List<WebElement> allProductsCheckboxList = getWebDriver().findElements(By.cssSelector(wishlistProductsCheckbox));
                return allProductsCheckboxList.size() == expectedNumberOfEntries;
            }

            public String toString() {
                return "Expecting number of entries in wishlist: \'" + expectedNumberOfEntries + "\'";
            }
        };
    }

    public void waitForChangesToBeApplied() {
        waitForPageToLoad();
        try {
            Thread.sleep(Constants.WAIT_TIME_CONSTANT);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public boolean areTheLastXCheckboxesSelected(int number) {
        waitForPageToLoad();
        List<WebElement> checkboxList = getWebDriver().findElements(By.cssSelector(wishlistProductsCheckbox));
        for (WebElement webElement : checkboxList.subList(number, checkboxList.size())) {
            String idAttribute = "#" + webElement.getAttribute("id");
            WebElement id = waitForElementByCssLocator(idAttribute);
            if (id.isSelected()) {
                return true;
            }
        }
        return false;
    }

    public void clickOnFirstAddToBagButton(String addToBagText) {
        waitForPageToLoad();
        getWebElementByJQuery(addToCartButtonLocator, addToBagText).click();
        waitForPageToLoad();

    }

    public void selectDeleteOptnFromDd(){
        if(WebDriverFactory.browser.equalsIgnoreCase(Constants.CHROME)){
            Select selectDropdown = new Select(getWebElement(manageSelectedDropDownByCSS));
            selectDropdown.selectByIndex(2);
        }else{
            WebElement webElement = getWebElement(wishlistActionsDropdown);
            clickWebElement(webElement);
            WebElement deleteOptioninDd = getWebElement(wishlistActionsDropdownOpen);
            clickWebElement(deleteOptioninDd);
        }
    }
}

