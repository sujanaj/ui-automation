package pages.account;

import commonfunctions.CommonFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.ArrayList;
import java.util.List;

public class ManagePreferencePage extends CommonFunctions{

    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    private String phoneManagePreference = obj_Property.readProperty("phoneManagePreference");
    private String postalManagePreference = obj_Property.readProperty("postalManagePreference");
    private String sendPreferences = obj_Property.readProperty("sendPreferences");

    public boolean isPreferencesSelected(){
        return doIsPreferencesSelected(false);
    }

    public boolean isPreferencesDeselected(){
        return doIsPreferencesSelected(true);
    }

    private boolean doIsPreferencesSelected(final boolean compare) {
        boolean isPreferencesDeselected = true;
        for (WebElement preference:getManagePreferences()){
            //managePreferencesPage.nativeScrollToElement(preference);
            scrollToElement(phoneManagePreference);
            scrollScreen300Pixel();
            if (waitForElementToBeClickableByElement(preference).isSelected() == compare){
                isPreferencesDeselected = false;
                break;
            }
        }

        return isPreferencesDeselected;
    }

    public List<WebElement> getManagePreferences(){
        waitForPageToLoad();
        List<WebElement> preferenceOption = new ArrayList<WebElement>();
        preferenceOption.add(getWebElement(phoneManagePreference));
        //preferenceOption.add(sms);
        preferenceOption.add(getWebElement(postalManagePreference));
        return preferenceOption;

    }

    public void selectPreferences(){
        doSelectPreferences(false);

    }

    private void doSelectPreferences(final boolean compare) {
        for (WebElement preference:getManagePreferences()) {
            scrollScreen300Pixel();
            scrollToElement(phoneManagePreference);
            waitForPageToLoad();
            if(waitForElementToBeClickableByElement(preference).isSelected()==compare){
                //preference.click();
                clickWebElementByJSE(preference);
                waitForPageToLoad();
            }

        }
    }

    public void deselectPreferences(){
        doSelectPreferences(true);

    }

    public void savePreferences(){
        selectRadioButton(sendPreferences);
    }

}
