package pages.account;

import baseclass.WebDriverFactory;
import commonfunctions.CommonFunctions;
import config.Constants;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import pages.signin.RegisterSignInPage;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class AddressBookPage extends CommonFunctions {

    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    ContactDetailsPage contactDetailsPage = new ContactDetailsPage();
    WebDriverFactory webDriverFactory = new WebDriverFactory();
    private String addNewAddressButtonLocator = obj_Property.readProperty("addNewAddressButtonLocator");
    private String addAddressContainerLocator = obj_Property.readProperty("addAddressContainerLocator");
    private String cancelButtonLocator = obj_Property.readProperty("cancelButtonLocator");
    private String editButtonsListLocator = obj_Property.readProperty("editButtonsListLocator");
    private String addressItemsListLocator = obj_Property.readProperty("addressItemsListLocator");
    private String firstNameFieldLocator = obj_Property.readProperty("firstNameFieldLocator");
    private String lastNameFieldLocator = obj_Property.readProperty("lastNameFieldLocator");
    private String saveAddressButtonLocator = obj_Property.readProperty("saveAddressButtonLocator");
    private String errorOnSaveAddress = obj_Property.readProperty("errorOnSaveAddress");
    private String addressLine1ListLocator = obj_Property.readProperty("addressLine1ListLocator");
    private String addressLine2ListLocator = obj_Property.readProperty("addressLine2ListLocator");
    private String addressCityListLocator = obj_Property.readProperty("addressCityListLocator");
    private String addressPostcodeListLocator = obj_Property.readProperty("addressPostcodeListLocator");
    private String addressCountyListLocator = obj_Property.readProperty("addressCountyListLocator");
    private String addressLine1ListLocatorJPN = obj_Property.readProperty("addressLine1ListLocatorJPN");
    private String deleteButtonsListLocator = obj_Property.readProperty("deleteButtonsListLocator");
    private String overlayDeleteButtonLocator = obj_Property.readProperty("overlayDeleteButtonLocator");
    private String addressToBeSearched = obj_Property.readProperty("addressToBeSearched");
    private String addressList = obj_Property.readProperty("addressList");
    private String pickAddressFromList = obj_Property.readProperty("pickAddressFromList");
    private String address1FieldLocator = obj_Property.readProperty("address1FieldLocator");
    private String address2FieldLocator = obj_Property.readProperty("address2FieldLocator");
    private String cityFieldLocator = obj_Property.readProperty("cityFieldLocator");
    private String postcodeFieldLocator = obj_Property.readProperty("postcodeFieldLocator");
    private String deleteOverlayLocator = obj_Property.readProperty("deleteOverlayLocator");
    private String enterAddressManuallyLink = obj_Property.readProperty("enterAddressManuallyLink");
    private String addressFormFieldsLocator = obj_Property.readProperty("addressFormFieldsLocator");
    private String manualAddressButtonLocator = obj_Property.readProperty("manualAddressButtonLocator");
    private String countrySelector = obj_Property.readProperty("countrySelector");
    private String stateSelector = obj_Property.readProperty("stateSelector");
    private String countyStateFieldLocator = obj_Property.readProperty("countyStateFieldLocator");
    private String addressCountryListLocator = obj_Property.readProperty("addressCountryListLocator");
    private String saveButtonLocator = obj_Property.readProperty("saveButtonLocator");
    private String firstNameInputLocatorByName = obj_Property.readProperty("firstNameInputLocatorByName");

    public void clickOnAddNewAddressButton() {
        waitForPageToLoad();
        scrollToElement(addNewAddressButtonLocator);
        if((null==RegisterSignInPage.loginErrorText)||(RegisterSignInPage.loginErrorText.isEmpty()))  {
            if(webDriverFactory.browser.equalsIgnoreCase(Constants.EDGE) || webDriverFactory.browser.equalsIgnoreCase(Constants.IE)){
                scrollScreen50Pixel();
                scrollScreen50Pixel();
            }else
                scrollScreen300Pixel();
        }
        waitForPageToLoad();
        clickOnElement(getWebElement(addNewAddressButtonLocator));
        waitForPageToLoad();
    }

    public boolean isAddAddressFormDisplayed() {
        waitForPageToLoad();
        try {
            Thread.sleep(1000);
            scrollToPageTop();
            if (getWebDriver().findElement(By.cssSelector(addAddressContainerLocator)).getAttribute("style").contains("block")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void clickOnCancelButton() {
        waitForPageToLoad();
        WebElement cancel = getWebDriver().findElement(By.cssSelector(cancelButtonLocator));
        ((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].click();", cancel);
    }

    public void clickEditUnderLastAddress() {
        waitForPageToLoad();
        List<WebElement> addressContainers = getWebDriver().findElements(By.cssSelector(addressItemsListLocator));
        waitForPageToLoad();
        WebElement edit = addressContainers.get(addressContainers.size() - 1).findElement(By.cssSelector(editButtonsListLocator));
        scrollToPageBottom();
        ((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].click();", edit);
    }

    public void saveAddressNameAndSurname(String firstName, String lastName)
    {
        waitForPageToLoad();
        enterFirstName(firstName);
        enterLastName(lastName);
        clickOnSaveAddress();
    }

    private void enterFirstName(String firstName) {
        waitForPageToLoad();
        getWebDriver().findElement(By.cssSelector(firstNameFieldLocator)).clear();
        getWebDriver().findElement(By.cssSelector(firstNameFieldLocator)).sendKeys(firstName);
    }

    private void enterLastName(String lastName) {
        waitForPageToLoad();
        getWebDriver().findElement(By.cssSelector(lastNameFieldLocator)).clear();
        getWebDriver().findElement(By.cssSelector(lastNameFieldLocator)).sendKeys(lastName);
    }

    public void clickOnSaveAddress() {
        waitForPageToLoad();
        //Commented below line as after scroll element is out of focus
       // scrollToPageTop();
        //scrollIntoViewByCss(saveAddressButtonLocator);
        waitForElementToBeClickable(saveAddressButtonLocator);
        WebElement save = getWebDriver().findElement(By.cssSelector(saveAddressButtonLocator));
//        clickElementByWebElement(save);
        clickWebElementByJSE(save);
        waitForPageToLoad();
        waitForScriptsToLoad();
    }

    public List<String> getValidationMessage()
    {
        List<String> errorMessages = new ArrayList<>();
        List<WebElement> elements = getWebElementByJQuery(errorOnSaveAddress);
        for (WebElement el : elements){
            errorMessages.add(el.getText());
        }
        return errorMessages;
    }

    public List<WebElement> getWebElementByJQuery(final String jQuery) {
        Object jqueryResult = ((JavascriptExecutor) getWebDriver()).executeScript("return $('" + jQuery + "')");
        if (WebDriverFactory.browser.equalsIgnoreCase("SAFARI")) {
            WebElement e1 = ((WebElement) ((Map) jqueryResult).get("0"));
            WebElement e2 = ((WebElement) ((Map) jqueryResult).get("2"));
            List<WebElement> elements = new ArrayList<>();
            elements.add(e1);
            elements.add(e2);
            return elements;
        } else {
            WebElement e1 = ((WebElement) ((List) jqueryResult).get(0));
            WebElement e2 = ((WebElement) ((List) jqueryResult).get(2));
            List<WebElement> elements = new ArrayList<>();
            elements.add(e1);
            elements.add(e2);
            return elements;
        }
    }

    public boolean checkIfAddressExists(String ad1, String city, String county, String code) {
        ad1=ad1+ " " + webDriverFactory.browser;
        city=city+ " " + webDriverFactory.browser;
        waitForPageToLoad();
        List<WebElement> addressContainers = getWebDriver().findElements(By.cssSelector(addressItemsListLocator));
        for (WebElement add : addressContainers) {
            if (add.findElement(By.cssSelector(addressLine1ListLocator)).getText().toLowerCase().trim().contains(ad1.toLowerCase())
                    && add.findElement(By.cssSelector(addressCityListLocator)).getText().toLowerCase().trim().contains(city.toLowerCase())
                    && add.findElement(By.cssSelector(addressPostcodeListLocator)).getText().toLowerCase().trim().contains(code.toLowerCase())
                    && add.findElement(By.cssSelector(addressCountyListLocator)).getText().toLowerCase().trim().contains(county.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    public void clickOnDeleteButtonForGivenAddress(String ad1, String city, String county, String code) {
        waitForPageToLoad();
        List<WebElement> addressContainers = getWebDriver().findElements(By.cssSelector(addressItemsListLocator));
        for (WebElement add : addressContainers) {
            if (add.findElement(By.cssSelector(addressLine1ListLocator)).getText().toLowerCase().trim().contains(ad1.toLowerCase())
                    && add.findElement(By.cssSelector(addressCityListLocator)).getText().toLowerCase().trim().contains(city.toLowerCase())
                    && add.findElement(By.cssSelector(addressPostcodeListLocator)).getText().toLowerCase().trim().contains(code.toLowerCase())
                    && add.findElement(By.cssSelector(addressCountyListLocator)).getText().toLowerCase().trim().contains(county.toLowerCase())) {
                WebElement el = add.findElement(By.cssSelector(deleteButtonsListLocator));
                clickElementByWebElement(el);
            }
        }
    }


    public void clickOnDeleteOnConfirmationOverlay() {
        clickElementByCSS(overlayDeleteButtonLocator);
        waitForPageToLoad();
        refreshPage();
        waitForPageToLoad();
    }

    public void enterAddress(String address){
        waitForPageToLoad();
        waitForPageToLoad();
        enterTextInputBox(addressToBeSearched,address);
        waitForElementByCssLocator(addressList);
       // (new WebDriverWait(getDriver(),10)).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".address-picklist")));
        clickElementByCSS(pickAddressFromList);
        waitForPageToLoad();
    }

    public List<String> getFirstAddressItemDetails() {
        waitForPageToLoad();
        List<WebElement> addressContainers = getWebDriver().findElements(By.cssSelector(addressItemsListLocator));
        List<String> form = new ArrayList<String>();

        WebElement lastAddress = addressContainers.get(0);
        form.add(lastAddress.findElement(By.cssSelector(addressLine1ListLocator)).getText().trim());
        form.add(lastAddress.findElement(By.cssSelector(addressCityListLocator)).getText().trim());
        form.add(lastAddress.findElement(By.cssSelector(addressCountyListLocator)).getText().trim());
        form.add(lastAddress.findElement(By.cssSelector(addressPostcodeListLocator)).getText().trim());

        return form;
    }

    public List<String> validateAddressFormFields(){
        waitForPageToLoad();
        List<WebElement> addressFormFields = getWebDriver().findElements(By.cssSelector(addressFormFieldsLocator));
        List<String> addressFormFieldsAttribute = new ArrayList<String>();
        for(WebElement el : addressFormFields){
            if (el.isDisplayed()){
                addressFormFieldsAttribute.add(el.getAttribute("for"));
            }
        }
        return  addressFormFieldsAttribute;
    }

    public void saveAddressDetails(String ad1, String ad2, String city, String code) {
        try {
            if ((getWebElement(enterAddressManuallyLink).isDisplayed()) || (getWebElement(firstNameInputLocatorByName).isDisplayed())) {
                if (!contactDetailsPage.isPCCCDisplayed()) {
                    waitForPageToLoad();
                    enterAddress1(ad1 + " " + webDriverFactory.browser);
                    enterAddress2(ad2 + " " + webDriverFactory.browser);
                    enterCity(city + " " + webDriverFactory.browser);
                    enterPostcode(code);
                    clickOnSaveAddress();
                }
            }
        }catch(NoSuchElementException e){
            e.printStackTrace();

        }
    }

    public void saveAddressDetailsWithPCCC(String ad1, String ad2, String city, String code,String pccc) {
        waitForPageToLoad();
        try {
            if((getWebElement(enterAddressManuallyLink).isDisplayed()) ||(getWebElement(firstNameInputLocatorByName).isDisplayed())){
                if (contactDetailsPage.isPCCCDisplayed()) {
                    enterAddress1(ad1);
                    enterAddress2(ad2);
                    enterCity(city);
                    enterPostcode(code);
                    contactDetailsPage.enterPCCC(pccc);
                    clickOnSaveAddress();
                }
            }
        }catch (org.openqa.selenium.NoSuchElementException e){
            e.printStackTrace();
        }

    }

    public void enterAddress1(String address)
    {
        waitForPageToLoad();
        try
        {
            getWebDriver().findElement(By.cssSelector(address1FieldLocator)).clear();
        }
        catch (Exception e)
        {
            clickOnEnterFullAddressLink();
        }
        enterTextInputBox(address1FieldLocator,address);
    }

    private void enterAddress2(String address) {
        waitForPageToLoad();
        enterTextInputBox(address2FieldLocator, address);
    }

    public void enterCity(String city) {
        waitForPageToLoad();
        enterTextInputBox(cityFieldLocator,city);
    }

    public void enterPostcode(String code) {
        waitForPageToLoad();
        enterTextInputBox(postcodeFieldLocator,code);
    }

    public void clickOnEnterFullAddressLink() {
        waitForPageToLoad();
        if(getWebElement(enterAddressManuallyLink).isDisplayed()) {
            clickOnElement(getWebElement(enterAddressManuallyLink));
        }
        waitForPageToLoad();
        waitForScriptsToLoad();
    }

    public String getFirstAddressLine1() {
        waitForPageToLoad();
        return getWebDriver().findElements(By.cssSelector(addressLine1ListLocator)).get(0).getText().trim();
    }

    public String getFirstAddressLine1ForJPN() {
        waitForPageToLoad();
        return getWebDriver().findElements(By.cssSelector(addressLine1ListLocatorJPN)).get(0).getText().trim();
    }

    public void clickOnDeleteForFirstAddress() {
        waitForPageToLoad();
        scrollToPageTop();
        getWebDriver().findElements(By.cssSelector(deleteButtonsListLocator)).get(0).click();
    }

    public boolean isDeleteConfirmationOverlayDisplayed() {
        waitForPageToLoad();
        try {
            if (getWebDriver().findElement(By.cssSelector(deleteOverlayLocator)).isDisplayed()) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void populateAddressDetails(String country, String ad1, String ad2, String city, String state, String code)
    {
        waitForPageToLoad();
        selectCountry(country);
        if(("United States").equals(country)){
            selectAddressManually();
            selectState(state);
        } else {
            enterState(state);
        }
        enterAddress1(ad1);
        enterAddress2(ad2);
        enterCity(city);
        enterPostcode(code);
    }

    private void selectAddressManually()
    {
        waitForPageToLoad();
        getWebDriver().findElement(By.cssSelector(manualAddressButtonLocator)).click();
    }

    private void selectCountry(String country)
    {
        waitForPageToLoad();
        selectFromDropdownByCSS(countrySelector,country);
    }

    public void selectState(String state)
    {
        waitForPageToLoad();
        selectFromDropdownByCSS(stateSelector,state);
    }
    public void enterState(String state)
    {
        waitForPageToLoad();
        getWebDriver().findElement(By.cssSelector(countyStateFieldLocator)).clear();
        getWebDriver().findElement(By.cssSelector(countyStateFieldLocator)).sendKeys(state);
    }

    public String getFirstAddressLine3() {
        waitForPageToLoad();
        return getWebDriver().findElements(By.cssSelector(addressCountyListLocator)).get(0).getText().trim();
    }

    public void clickEditWhereCountryIs(String country) {
        waitForPageToLoad();
        scrollToPageTop();
        List<WebElement> addressContainers = getWebDriver().findElements(By.cssSelector(addressItemsListLocator));
        for (WebElement add : addressContainers) {
            if (add.findElement(By.cssSelector(addressCountryListLocator)).getText().trim().equals(country)) {
//                WebElement edit = add.findElement(By.cssSelector(editButtonsListLocator));
//                ((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].click();", edit);
                clickElementByCSS(editButtonsListLocator);
            }
        }
    }

    public List<String> getAddressDetails(String country) {
        waitForPageToLoad();
        List<WebElement> addressContainers = getWebDriver().findElements(By.cssSelector(addressItemsListLocator));
        List<String> addressDetails = new ArrayList<String>();
        for (WebElement add : addressContainers) {
            if (add.findElement(By.cssSelector(addressCountryListLocator)).getText().trim().equalsIgnoreCase(country)) {
                addressDetails.add(add.findElement(By.cssSelector(addressLine1ListLocator)).getText().trim());
                addressDetails.add(add.findElement(By.cssSelector(addressLine2ListLocator)).getText().trim());
                addressDetails.add(add.findElement(By.cssSelector(addressPostcodeListLocator)).getText().trim());
                addressDetails.add(add.findElement(By.cssSelector(addressCityListLocator)).getText().trim());
            }
        }
        return addressDetails;
    }

    public int getAddressesCount(){
       return getWebDriver().findElements(By.cssSelector(".address-card")).size();
    }

    public String getSaveAddressButtonText(){
        return getTextByCssSelector(saveButtonLocator);
    }

    public String getExpectedFirstAddressLine1(String add){
        add = add +" "+ webDriverFactory.browser;
        return add;
    }

}
