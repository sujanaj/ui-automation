package pages.account;

import commonfunctions.CommonFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class MyCreditsPage extends CommonFunctions {

    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();

    private String giftCardSerial = obj_Property.readProperty("giftCardSerial");
    private String  currencySelector = obj_Property.readProperty("currencySelector");
    private String convertedCreditTotal = obj_Property.readProperty("convertedCreditTotal");

    public String grabGiftCardSerialLabel() {
        waitForPageToLoad();
        return getWebElement(giftCardSerial).getAttribute("placeholder");
    }

    public void selectCurrency(String currency){
        waitForPageToLoad();
        selectCustomDropdown(currencySelector, currency);
        waitForPageToLoad();
    }

    public String grabConvertTotal(){
       // WebElement convertTotal = waitForElementByCssLocator(creditTableContainer).findElement(By.cssSelector("tfoot td:last-child"));
        return getTextByCssSelector(convertedCreditTotal);
    }
}
