package pages.account;

import commonfunctions.CommonFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class PaymentCardsPage extends CommonFunctions {
    config.ObjPropertyReader obj_Property= new config.ObjPropertyReader();

    private String cardsInfoListLocator = obj_Property.readProperty("cardsInfoListLocator");
    private String cardOwner = obj_Property.readProperty("cardOwner");
    private String deleteBtnListLocator = obj_Property.readProperty("deleteBtnListLocator");
    private  String modalDeleteBtnLocator = obj_Property.readProperty("modalDeleteBtnLocator");
    private String removePaymentDetailsModal = obj_Property.readProperty("removePaymentDetailsModal");
    private WebElement cardWithSameOwner = null;

    public boolean isCardOnPage(String owner) {
        waitForPageToLoad();
        List<WebElement> cardsList = waitForListElementByCssLocator(cardsInfoListLocator);
        for (WebElement card : cardsList) {
            if (card.findElement(By.cssSelector(cardOwner)).getText().contains(owner)) {
                cardWithSameOwner =card;
                return true;
            }
        }
        return false;
    }

    public boolean isCardOnPage(String cardType, String digits) {
        waitForPageToLoad();
        List<WebElement> cardsList = waitForListElementByCssLocator(cardsInfoListLocator);
        for (WebElement card : cardsList) {
            final String cardInfo = card.findElement(By.cssSelector(".card-number")).getText();
            if (cardInfo.contains(cardType) && cardInfo.contains(digits)) {
                return true;
            }
        }
        return false;
    }

    public void clickOnDeleteButton(String owner) {
       if(isCardOnPage(owner)){
           WebElement deleteBtn = cardWithSameOwner.findElement(By.cssSelector(deleteBtnListLocator));
           clickElementByWebElement(deleteBtn);
       }
    }

    public boolean checkIfDeleteConfirmationModalIsDisplayed() {
        waitForPageToLoad();
        if (isElementDisplayed(removePaymentDetailsModal)) {
            return true;
        } else {
            return false;
        }
    }

    public void clickOnDeleteButtonInModal() {
        clickElementByCSS(modalDeleteBtnLocator);
        waitForPageToLoad();
    }

}
