package pages;

import commonfunctions.CommonFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import java.util.List;

public class ShopTheLookListPage extends CommonFunctions {

    public void selectSizeOnShopTheLook(String css,String size){

        WebElement dropdown = getWebDriver().findElement(By.cssSelector(css + " ~ span"));
        ((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].click();", dropdown);

        List<WebElement> optionsList = getWebDriver().findElements(By.cssSelector(css + " ~ div .cs__item"));
        for (WebElement optionNow : optionsList) {
            if (optionNow.getText().trim().equalsIgnoreCase(size.trim())) {
                ((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].click();", optionNow);
                break;
            }
        }

    }
}
