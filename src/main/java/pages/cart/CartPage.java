package pages.cart;

import baseclass.WebDriverFactory;
import commonfunctions.CommonFunctions;
import commonfunctions.NavigateUsingURL;

public class CartPage extends CommonFunctions {
    NavigateUsingURL navigateUsingURL= new NavigateUsingURL();
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    WebDriverFactory webDriverFactory = new WebDriverFactory();

    private String bottomProceedToPurchaseButton = obj_Property.readProperty("bottomProceedToPurchaseButton");

    public void clickOnButtomProceedToPurchaseButton() {
        waitForPageToLoad();
        waitForElementByCssLocator(bottomProceedToPurchaseButton);
        scrollIntoViewByCss(bottomProceedToPurchaseButton);
        focusElement(bottomProceedToPurchaseButton);
        scrollToPageBottom();
        clickElementByCSS(bottomProceedToPurchaseButton);
        waitForPageToLoad();
        waitForScriptsToLoad();
    }
}
