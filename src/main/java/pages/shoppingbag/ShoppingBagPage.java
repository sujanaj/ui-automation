package pages.shoppingbag;

import commonfunctions.CommonFunctions;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import java.util.ArrayList;
import java.util.List;

public class ShoppingBagPage extends CommonFunctions {

    private String productSize;
    private String quantity;
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    String shoppingbagSizeDropdown = obj_Property.readProperty("shoppingbagSizeDropdownByCSS");
    String shoppingbagQtydd = obj_Property.readProperty("shoppingbagQtyddByCSS");

    public List<String> retrieveAvailableSizesonShoppingBag(){
        Select sizeDropdownlist = new Select(getWebElement(shoppingbagSizeDropdown));
        List<WebElement> sizeWebElementsList = sizeDropdownlist.getOptions();
        List<String> availableSizes = new ArrayList<>();
        for(int i=1 ; i < sizeWebElementsList.size() ; i++){
            WebElement sizeWebElement = sizeWebElementsList.get(i);
            if(!sizeWebElement.getText().contains("-")){
                availableSizes.add(retrieveTextByWebElement(sizeWebElement).replaceAll("\n|\t|\\s",""));
            }
        }
        return availableSizes;
    }

    public List<String> quantityListonShoppingBag(){
        List<WebElement> quantityddList = new Select(getWebElement(shoppingbagQtydd)).getOptions();
        List<String> qtyList = new ArrayList<>();
        for(WebElement qty : quantityddList){
            qtyList.add(qty.getText());
        }
        return qtyList;
    }
    public void setProductSize(String size) {
        this.productSize = size;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setQuantity(String qty){this.quantity = qty;}

    public String getQuantity(){return quantity;}

    public void changeFirstProductSize(String size){
        waitForPageToLoad();
        ((JavascriptExecutor) getWebDriver()).executeScript("$(\".items-list-row__size .cs__element:first\").val($(\".items-list-row__size .cs__element:first option:contains("+size+")\").val()).change()");
        waitForPageToLoad();
    }
}
