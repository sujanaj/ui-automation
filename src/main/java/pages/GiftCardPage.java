package pages;

import commonfunctions.CommonFunctions;
import commonfunctions.NavigateUsingURL;
import config.Constants;
import config.ObjPropertyReader;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.List;

public class GiftCardPage extends CommonFunctions {

    ObjPropertyReader objPropertyReader = new ObjPropertyReader();
    String shopGiftCardsTitle = objPropertyReader.readProperty("shopGiftCardsTitleByCSS");
    CommonFunctions commonFunctions = new CommonFunctions();
    NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
    String addToBagByCSS = objPropertyReader.readProperty("addToBagByCSS");
    String giftcardValueDropdown = objPropertyReader.readProperty("giftcardValueDropdown");
    String messageBoxByID = objPropertyReader.readProperty("messageBoxByID");
    String promoCodeByID = objPropertyReader.readProperty("promoCodeByID");
    String submitVoucherByCSS = objPropertyReader.readProperty("submitVoucherByCSS");

    public String grabShopGiftCards() {
        waitForPageToLoad();
        return getWebDriver().findElement(By.cssSelector(shopGiftCardsTitle)).getText();
    }

    public String grabActivateYourGiftCards() {
        waitForPageToLoad();
        return getWebDriver().findElement(By.cssSelector(".hero__copy > div:nth-child(2) h3")).getText();
    }

    public void selectGiftCard() {
        waitForPageToLoad();
        navigateUsingURL.navigateToRelativeURL("/gift-cards/giftcard");
        commonFunctions.getValueFromDropDownviaSelect("#giftCardValue", "£100");
    }

    public void addGiftCardButton() {
        commonFunctions.clickElementByCSS(addToBagByCSS);
    }

    public void addSameFiveGiftCard(String value) throws InterruptedException {
        List<String> giftcardDenominations = giftCardDropValues();
        List<WebElement> giftCardOptions = commonFunctions.getSelectDropdownElements(giftcardValueDropdown);
        int noOfGiftCards = Integer.valueOf(value);
        for (int j = 1; j <= giftCardOptions.size(); j++) {
            if (giftcardDenominations.get(j - 1).equalsIgnoreCase(giftCardOptions.get(j).getText().trim().replaceAll(" ", "").replaceAll("\n|\t|\\s", ""))) {
                Constants.GIFTCARD.get(giftcardDenominations.get(j).replace("[/s]", ""));
                commonFunctions.navigateTo(Constants.BASE_URL + "/at/cart/add/" + Constants.GIFTCARD.get(giftcardDenominations.get(j).trim()) + "/" + noOfGiftCards);
                waitForPageToLoad();
                break;
            }
        }
        for (int i = 0; i <= 5; i++) {
            commonFunctions.navigateTo(Constants.BASE_URL + "/at/cart/add/" + Constants.GIFTITEM1 + "/" + "1");
        }
    }

    public void addDiffAmountGifCard(String value) {
        int noOfGiftCards = Integer.valueOf(value);
        List<String> giftcardDenominations = giftCardDropValues();
        waitForPageToLoad();
        Boolean flag = false;
        List<WebElement> giftCardOptions = commonFunctions.getSelectDropdownElements(giftcardValueDropdown);
        for (int i = 0; i <= noOfGiftCards - 1; i++) {
            for (int j = 1; j <= giftCardOptions.size(); j++) {
                if (giftcardDenominations.get(j - 1).equalsIgnoreCase(giftCardOptions.get(j).getText().trim().replaceAll(" ", "").replaceAll("\n|\t|\\s", ""))) {
                    Constants.GIFTCARD.get(giftcardDenominations.get(j).replace("[/s]", ""));
                    commonFunctions.navigateTo(Constants.BASE_URL + "/at/cart/add/" + Constants.GIFTCARD.get(giftcardDenominations.get(j).trim()) + "/" + "1");
                    commonFunctions.navigateTo(Constants.BASE_URL + "/gift-cards/giftcard");
                    waitForPageToLoad();
                    giftCardOptions = commonFunctions.getSelectDropdownElements(giftcardValueDropdown);
                    if (noOfGiftCards == j) {
                        flag = true;
                        break;
                    }
                }
            }
            if (flag == true) {
                break;
            }
        }
    }

    public void addDiffAmountGiftCardKoreaJapan() {
        List<WebElement> selectItem = commonFunctions.getSelectDropdownElements(giftcardValueDropdown);
        for (int i = 0; i <= 5; i++) {
            waitForPageToLoad();
            selectItem.get(i).click();
            this.addGiftCardButton();
        }
    }

    public void addMessageToGiftCard() {
        waitForPageToLoad();
        commonFunctions.enterText("id", messageBoxByID, "Christmas Gift - Just For you");
    }

    public Boolean checkedIfButtonNotDisplayed() {
        if (!commonFunctions.isElementDisplayed(addToBagByCSS)) {
            return true;
        } else {
            return false;
        }
    }

    public void enterPromoCode() {
        commonFunctions.enterText("id", promoCodeByID, "Dev6");
        clickElementByCSS(submitVoucherByCSS);
    }

    public static ArrayList<String> giftCardDropValues() {
        ArrayList<String> giftcardDenominations = new ArrayList<>();
        giftcardDenominations.add("£100");
        giftcardDenominations.add("£200");
        giftcardDenominations.add("£300");
        giftcardDenominations.add("£400");
        giftcardDenominations.add("£500");
        giftcardDenominations.add("£750");
        return giftcardDenominations;
    }
}


