package pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import baseclass.BaseClass;
import commonfunctions.CommonFunctions;

public class ProductContactUsPage extends BaseClass {
	CommonFunctions commonFunctions = new CommonFunctions();
	config.ObjPropertyReader obj_Property= new config.ObjPropertyReader();
	String productContactusFisrtNameInputByCSS=obj_Property.readProperty("productContactusFisrtNameInputByCSS");
	String productContactUsSurnameInput=obj_Property.readProperty("productContactUsSurnameInput");
	String productContactUsemailInput=obj_Property.readProperty("productContactUsemailInput");
	String productContactUsphoneInput=obj_Property.readProperty("productContactUsphoneInput");
	String productContactUscountryDropDown=obj_Property.readProperty("productContactUscountryDropDown");
	String productContactUsEnquiryDropDown=obj_Property.readProperty("productContactUsEnquiryDropDown");
	String productContactUsSubjectInput=obj_Property.readProperty("productContactUsSubjectInput");
	String productContactUsMessageInput=obj_Property.readProperty("productContactUsMessageInput");
	String productContactUsGetTxtSizeSelect=obj_Property.readProperty("productContactUsGetTxtSizeSelect");
	String productCloseContactUsErrorContactUsLabels=obj_Property.readProperty("productCloseContactUsErrorContactUsLabels");
	String PDPContactUsLastNameJapanByCSS=obj_Property.readProperty("PDPContactUsLastNameJapanByCSS");
	public List<String> grabContactUsTextLabelsForm() {

		commonFunctions.waitForPageToLoad();
		List<String> contactForm = new ArrayList<String>();
		List<WebElement> formAttributesFields = new ArrayList<WebElement>();
		List<WebElement> formFields = new ArrayList<WebElement>();

			//formAttributesFields.add(getWebDriver().findElement(By.cssSelector(PDPContactUsLastNameJapanByCSS)));

		formAttributesFields.add(getWebDriver().findElement(By.cssSelector(productContactusFisrtNameInputByCSS)));
		formAttributesFields.add(getWebDriver().findElement(By.cssSelector(productContactUsSurnameInput)));
		formAttributesFields.add(getWebDriver().findElement(By.cssSelector(productContactUsemailInput)));
		formAttributesFields.add(getWebDriver().findElement(By.cssSelector(productContactUsphoneInput)));
		formFields.add(getWebDriver().findElement(By.cssSelector(productContactUscountryDropDown)));
		formFields.add(getWebDriver().findElement(By.cssSelector(productContactUsEnquiryDropDown)));
		formAttributesFields.add(getWebDriver().findElement(By.cssSelector(productContactUsSubjectInput)));
		formAttributesFields.add(getWebDriver().findElement(By.cssSelector(productContactUsMessageInput)));
		formFields.add(getWebDriver().findElement(By.cssSelector(productContactUsGetTxtSizeSelect)));

		for (WebElement e : formAttributesFields.subList(0, 4)) {
			contactForm.add(e.getAttribute("placeholder"));
		}
		for (WebElement e : formFields.subList(0, 2)) {
			contactForm.add(e.getText().replaceAll("[\n]","").trim());
		}
		for (WebElement e : formAttributesFields.subList(4, formAttributesFields.size())) {
			contactForm.add(e.getAttribute("placeholder"));
		}
		for (WebElement e : formFields.subList(2, formFields.size())) {
			contactForm.add(e.getText().replaceAll("[\n]","").trim());
		}
		return contactForm;
	}

	public ContactUsErrorModel grabContactUsErrorText() {
		ContactUsErrorModel result = new ContactUsErrorModel();
		commonFunctions.waitForPageToLoad();
		List<WebElement> errorTextList = getWebDriver().findElements(By.cssSelector(productCloseContactUsErrorContactUsLabels));

		for (WebElement currentElement : errorTextList) {
			if (currentElement.getAttribute("for").contentEquals("name"))
				result.setFirstName(currentElement.getText());
			if (currentElement.getAttribute("for").contentEquals("last-name"))
				result.setSurName(currentElement.getText());
			if (currentElement.getAttribute("for").contentEquals("email"))
				result.setEmail(currentElement.getText());
			if (currentElement.getAttribute("for").contentEquals("phone"))
				result.setPhone(currentElement.getText());
			if (currentElement.getAttribute("for").contentEquals("subject"))
				result.setSubject(currentElement.getText());
			if (currentElement.getAttribute("for").contentEquals("00Nm0000000HXCc"))
				result.setMessage(currentElement.getText());
			if (currentElement.getAttribute("for").contentEquals("00Nm0000000HWXZ"))
				result.setSize(currentElement.getText());
		}
		return result;
	}


}
