package pages;

import commonfunctions.CommonFunctions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import baseclass.BaseClass;

public class MiniCartPage extends CommonFunctions {

	config.ObjPropertyReader obj_Property= new config.ObjPropertyReader();
	String minicartLocator = obj_Property.readProperty("minicartLocator");

	public boolean minicartIsDisplayed() {
		waitForPageToLoad();
		WebElement minicart = waitForElementByCssLocator(minicartLocator);
		return minicart.isDisplayed();
	}

	public String grabMinicartIconNumber() {
		waitForPageToLoad();
		String minicartItemNum = obj_Property.readProperty("minicartIconNumber");
		WebElement minicartItemNumber = getWebElement(minicartItemNum);
		String itemNumber = minicartItemNumber.getText().trim();
		return itemNumber;
	}

}
