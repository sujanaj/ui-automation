package pages;

import commonfunctions.CommonFunctions;
import org.openqa.selenium.By;
import java.util.ArrayList;
import java.util.List;

public class HomePage extends CommonFunctions {

    config.ObjPropertyReader obj_Property= new config.ObjPropertyReader();
    String OUTFIT_HEADLINE_LOCATOR = obj_Property.readProperty("instantfitTitle");
    String instantFitProductsLocator = obj_Property.readProperty("instantFitProductsLocator");
    String instantFitFirstProductTitle = obj_Property.readProperty("instantFitFirstProductTitle");
    String instantFitFirstProductInfo = obj_Property.readProperty("instantFitFirstProductInfo");
    String instantFitFirstProductPrice = obj_Property.readProperty("instantFitFirstProductPrice");
    String styleReportImageByCSS = obj_Property.readProperty("styleReportImageByCSS");
    String homePageCarouselByCSS = obj_Property.readProperty("homePageCarouselByCSS");
    String styleReportLastItemByCSS = obj_Property.readProperty("styleReportLastItemByCSS");

    public String grabInstantOutfitTitle() {
        waitForPageToLoad();
        scrollIntoViewByCss(OUTFIT_HEADLINE_LOCATOR);
        return getWebElement(OUTFIT_HEADLINE_LOCATOR).getText().toUpperCase().trim();
    }

    public int grabNumberOfProducts() {
        waitForPageToLoad();
        scrollIntoViewByCss(OUTFIT_HEADLINE_LOCATOR);
        return waitForListElementByCssLocator(instantFitProductsLocator).size();
    }

    public List<String> grabInstantOutfitProductDetailsContainer() {
        waitForPageToLoad();
        scrollIntoViewByCss(OUTFIT_HEADLINE_LOCATOR);
        List<String> productDetails = new ArrayList<String>();
        String productTitle = getWebElement(instantFitFirstProductTitle).getText().toUpperCase().trim();
        String productInfo = getWebElement(instantFitFirstProductInfo).getText().toUpperCase().trim();
        String productPrice = getWebElement(instantFitFirstProductPrice).getText().toUpperCase().trim();
        productDetails.add(productTitle);
        productDetails.add(productInfo);
        productDetails.add(productPrice);
        return productDetails;
    }

    public String grabTheStyleReportImage() {
        waitForPageToLoad();
        return getWebElement(styleReportImageByCSS).getAttribute("href");
    }

    public void scrollTillLastElementIsVisible() {
        waitForPageToLoad();
        scrollIntoViewByCss(styleReportLastItemByCSS);
    }

    public int grabCarouselCounterSize() {
        waitForPageToLoad();
        return waitForListElementByCssLocator(homePageCarouselByCSS).size();
    }

    public void clickOnNextArrowOnCarousel() {
        waitForPageToLoad();
        // will change this once this is tested thoroughly
        getWebElement(".text-carousel.slick-initialized.slick-slider.slick-dotted .slick-next.slick-arrow").click();
        waitForPageToLoad();

    }

    public void clickOnPreviousArrowOnCarousel() {
        waitForPageToLoad();
        // will change this once this is tested thoroughly
        getWebElement(".text-carousel.slick-initialized.slick-slider.slick-dotted .slick-prev.slick-arrow").click();
        waitForPageToLoad();

    }
}
