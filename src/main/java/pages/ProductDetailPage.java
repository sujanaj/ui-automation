package pages;

import java.text.Normalizer;
import java.util.List;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import commonfunctions.CommonFunctions;
import org.testng.asserts.SoftAssert;

public class ProductDetailPage extends CommonFunctions {
    CommonFunctions commonFunctions = new CommonFunctions();
    config.ObjPropertyReader obj_Property = new config.ObjPropertyReader();
    String ErrorMessageUnderSizeMenuByCSS = obj_Property.readProperty("ErrorMessageUnderSizeMenuByCSS");
    SoftAssert softAssert = new SoftAssert();
    String pdpStickyNavWrapper = obj_Property.readProperty("pdpStickyNavWrapperByClass");
    String selectSizeErrorSTLMsg = obj_Property.readProperty("selectSizeErrorSTLMsgByClass");
    String selectSizeErrorPDPMsg = obj_Property.readProperty("selectSizeErrorPDPMsgByClass");
    private String productSize;
    private String requiredProduct;
    public static String requiredProductSize="";

    public void verifySizeMenuError(String errorMessage) {
        String actualErrorMessage = retrieveText(ErrorMessageUnderSizeMenuByCSS);
        actualErrorMessage = actualErrorMessage.replaceAll("[\n]", " ").trim();
        actualErrorMessage = Normalizer.normalize(actualErrorMessage, Normalizer.Form.NFKC);
        errorMessage = Normalizer.normalize(errorMessage, Normalizer.Form.NFKC);
        softAssert.assertTrue(errorMessage.contains(actualErrorMessage), "Error message is not correct. Expected: " + errorMessage + " Actual: " + actualErrorMessage);
        softAssert.assertAll();
    }


    public synchronized void clickOnFullScreen(String cssLocator) {
        ((JavascriptExecutor) getWebDriver()).executeScript("window.scrollBy(0,500)");
        getWebDriver().findElement(By.cssSelector(cssLocator)).click();
    }

    public String getTextFullScreen(String cssLocator) {
        return getWebDriver().findElement(By.cssSelector(cssLocator)).getText();
    }

    public void clickOnAddToBagFullScreen(String cssLocator) {
        waitForPageToLoad();
        getWebElement(cssLocator).click();
        waitForPageToLoad();
    }

    public boolean isDescriptionAccordionExpanded(String cssLocator) {
        boolean isValid = false;
        waitForPageToLoad();
        waitForScriptsToLoad();
        List<WebElement> accordionList = waitForListElementByCssLocator(cssLocator);

        if (accordionList.size() == 3) {
            String classAttribute = accordionList.get(0).getAttribute("class");
            if (classAttribute.contains("active"))
                isValid = true;
        }
        return isValid;
    }

    public boolean isDetailsAccordionExpanded(String cssLocator) {
        boolean isValid = false;
        waitForPageToLoad();
        waitForScriptsToLoad();
        List<WebElement> accordionList = waitForListElementByCssLocator(cssLocator);

        if (accordionList.size() == 3) {
            String classAttribute = accordionList.get(1).getAttribute("class");
            if (classAttribute.contains("active"))
                isValid = true;
        }
        return isValid;
    }

    public boolean isSizeAndFitAccordionExpanded(String cssLocator) {
        boolean isValid = false;
        waitForPageToLoad();
        List<WebElement> accordionList = waitForListElementByCssLocator(cssLocator);

        if (accordionList.size() == 3) {
            String classAttribute = accordionList.get(2).getAttribute("class");
            if (classAttribute.contains("active"))
                isValid = true;
        }
        return isValid;
    }

    public void scrollToElementWithinPage(String elementLocator) {
        ((JavascriptExecutor) getWebDriver()).executeScript("var a = $('" + elementLocator
                + "')[0].offsetTop; $('.mCustomScrollbar').mCustomScrollbar('scrollTo',a);");
        waitForPageToLoad();
    }

    public boolean isSizeEnabledOnPDP(String cssLocator) {
        String style = getWebDriver().findElement(By.cssSelector(cssLocator)).getAttribute("style");
        boolean flag = false;
        if (style.equalsIgnoreCase("pointer-events: none;")) {
            flag = true;
        } else if (style.equalsIgnoreCase("pointer-events: auto;")) {
            flag = false;
        } else {
            flag = false;
        }
        return flag;
    }

    public boolean theVideoIsDisplayed(String cssLocator) {
        waitForPageToLoad();

        String style = getWebDriver().findElement(By.cssSelector(cssLocator)).getAttribute("style");

        if (style.contains("block")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean theVideoIsPlaying(String cssLocator) {
        waitForPageToLoad();
        waitForScriptsToLoad();
        String cls = getWebElement(cssLocator).getAttribute("class");
        if (cls.contains("video-controls__button--pause")) {
            return true;
        } else {
            return false;
        }
    }

    public void clickOnCloseVideoButton(String cssLocator, String csscloseVideoButton) {
        waitForPageToLoad();
        focusElement(cssLocator);
        fireEvent("videoTag", "mouseover");
        waitForElementByCssLocator("#videoTag");
        waitForScriptsToLoad();
        selectRadioButton(csscloseVideoButton);
    }

    public boolean theVideoIsPaused(String cssLocator) {
        waitForPageToLoad();
        String btnText = getWebElement(cssLocator).getAttribute("title").trim();
        if (btnText.contains("Play")) {
            return true;
        } else {
            return false;
        }
    }


    public void clickOnThePauseButton(String cssLocator, String cssPauseVideoButton) {
        waitForPageToLoad();
        focusElement(cssLocator);
//        fireEvent("videoTag", "mouseover");
//        waitForElementByCssLocator("#videoTag");
//        getWebElement(cssPauseVideoButton).click();
        clickElementByCSS(cssPauseVideoButton);
//        getWebDriver().findElement(By.cssSelector(cssPauseVideoButton)).click();
    }

    public void clickOnThePlayButton(String cssLocator, String cssPlayVideoButton) {
        waitForPageToLoad();
        focusElement(cssLocator);
//        fireEvent("videoTag", "mouseover");
//        waitForElementByCssLocator("#videoTag");
        clickElementByCSS(cssPlayVideoButton);
//        getWebDriver().findElement(By.cssSelector(cssPlayVideoButton)).click();
    }

    public boolean mainImageIsChangedOnArrowClick(String btn, String cssLocatorPreviousButton, String cssLocatorNextButton, String pdpGetmainImageIndexByCSS, String pdpThumbImageIndex) {
        waitForPageToLoad();
            WebElement prevBtn = getWebElement(cssLocatorPreviousButton);
            WebElement nextBtn = getWebElement(cssLocatorNextButton);
        if ("Next".equalsIgnoreCase(nextBtn.getText().trim())) {
            nextBtn.click();
            } else if ("Previous".equalsIgnoreCase(prevBtn.getText().trim())) {
            prevBtn.click();
            }
        int mainImgIndex = Integer.parseInt(getWebElement(pdpGetmainImageIndexByCSS).getAttribute("data-slick-index"));
        String script = "return $('"+pdpThumbImageIndex+"').parent().attr('data-image-index')" ;
        int thumbImgIndex =  Integer.parseInt(((JavascriptExecutor) getWebDriver()).executeScript(script).toString());
        if (mainImgIndex == thumbImgIndex) {
            return true;
        } else
            return false;
    }

    public boolean largeThumbnailsContainerIsDisplayed(String cssLargeThumbnailContainerLocator, String cssActiveLargeThumbnailLocator) {
        waitForPageToLoad();
        WebElement largeThumbnailsCont = getWebElement(cssLargeThumbnailContainerLocator);
        WebElement activeThumbnail = getWebElement(cssActiveLargeThumbnailLocator);
        if (largeThumbnailsCont.isDisplayed() && activeThumbnail.isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }

    public boolean largeImageIsDisplayed(String csslocator) {
        waitForPageToLoad();

        List<WebElement> activeImgLocatorList = waitForListElementByCssLocator(csslocator);

        if (activeImgLocatorList.size() == 1) {
            return true;
        }
        return false;
    }


    public boolean checkIfMainImageIsZoomedIn(String csslocator) {
        waitForPageToLoad();

        WebElement imgDiv = getWebDriver().findElement(By.cssSelector(csslocator + ">div>div"));
        if (imgDiv.getAttribute("class").contains("zoom-on")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkIfImageReturnsToNormal(String gridContainerLocator, String activeMainImageListLocator) {
        waitForPageToLoad();
        getWebDriver().findElement(By.cssSelector(gridContainerLocator)).click();

        List<WebElement> imgList = getWebDriver().findElements(By.cssSelector(activeMainImageListLocator));
        if (imgList.get(1).getAttribute("style").contains("opacity: 0")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean checkIfFullscreenIsActive(String csslocator) {
        waitForPageToLoad();

        if (getWebDriver().findElement(By.cssSelector(csslocator)).getAttribute("class").contains("active")) {
            return true;
        } else {
            return false;
        }
    }

    public boolean mainImageIsChangedOnFullscreenThumbnailClick(String fullscreenThumbnailsDivListLocator, String fullscreenMainImgListLocator) {
        waitForPageToLoad();

        List<WebElement> thumbnailList = getWebDriver().findElements(By.cssSelector(fullscreenThumbnailsDivListLocator));
        List<WebElement> mainImgList = getWebDriver().findElements(By.cssSelector(fullscreenMainImgListLocator));
        int posThumbnail = 0;
        int posMain = 0;

        for (WebElement img : thumbnailList.subList(1, thumbnailList.size() / 2)) {
            for (WebElement mainImg : mainImgList) {
                img.click();
                if (img.getAttribute("class").contains("thumbnail active")) {
                    if (mainImg.getAttribute("class").contains("slick-active")) {
                        posThumbnail = thumbnailList.indexOf(img);
                        posMain = mainImgList.indexOf(mainImg);

                        if (posThumbnail == posMain - 1) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    public boolean checkIfNumberCorrespondsToFSThumbnail(String fullscreenImageNumberLocator, String fullscreenThumbnailsListLocator) {
        waitForPageToLoad();

        String numberSlide = getWebDriver().findElement(By.cssSelector(fullscreenImageNumberLocator)).getText();
        String number = numberSlide.split("/")[0];

        List<WebElement> thumbnailList = getWebDriver().findElements(By.cssSelector(fullscreenThumbnailsListLocator));
        int posThumbnail = 0;

        for (WebElement img : thumbnailList.subList(0, thumbnailList.size() / 2)) {
            if (img.findElement(By.cssSelector("div")).getAttribute("class").contains("active")) {
                posThumbnail = thumbnailList.indexOf(img) + 1;

                if (number.equals(Integer.toString(posThumbnail))) {
                    return true;
                }
            }
        }
        return false;
    }

    public void clickCloseFullScreenButton() {
        waitForPageToLoad();
        ((JavascriptExecutor) getWebDriver()).executeScript("$('#closeFullScreen').click();");
    }

    public boolean mainImageIsChangedOnFSArrowClick(String btn, String fullscreenPrevBtnLocator, String fullscreenNextBtnLocator, String fullscreenThumbnailsDivListLocator, String fullscreenMainImgListLocator) {
        waitForPageToLoad();

        WebElement prevBtn = getWebDriver().findElement(By.cssSelector(fullscreenPrevBtnLocator));
        WebElement nextBtn = getWebDriver().findElement(By.cssSelector(fullscreenNextBtnLocator));

        List<WebElement> thumbnailList = getWebDriver().findElements(By.cssSelector(fullscreenThumbnailsDivListLocator));
        List<WebElement> mainImgList = getWebDriver().findElements(By.cssSelector(fullscreenMainImgListLocator));
        int posThumbnail = 0;
        int posMain = 0;

        if (btn.contains(prevBtn.getText())) {
            prevBtn.click();
        } else if (btn.contains(nextBtn.getText())) {
            nextBtn.click();
        }

        for (WebElement img : thumbnailList.subList(0, thumbnailList.size() / 2)) {
            for (WebElement mainImg : mainImgList) {
                if (img.getAttribute("class").contains("active")) {
                    if (mainImg.getAttribute("class").contains("slick-active")) {
                        posThumbnail = thumbnailList.indexOf(img);
                        posMain = mainImgList.indexOf(mainImg);

                        if (posThumbnail == posMain - 1) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public boolean checkIfOverlayIsDisplayed(String cssloctor) {
        waitForScriptsToLoad();
        if (getWebDriver().findElement(By.cssSelector(cssloctor)).isDisplayed()) {
            return true;
        } else {
            return false;
        }
    }

    public String getDeliveryShippingCharge(String shipping, String csslocator) {
        waitForPageToLoad();
        List<WebElement> shippingList = getWebDriver().findElements(By.cssSelector(csslocator));
        for (WebElement shippingType : shippingList) {
            if (shippingType.findElement(By.cssSelector(".label")).getText().toLowerCase().equals(shipping.toLowerCase())) {
                //waitForElementPresenceByCssLocator(".delivery-price");
                return shippingType.findElement(By.cssSelector(".delivery-price")).getText();
            }
        }
        return "";
    }

    public void scrollDownTillProductIsVisibleOnOverlay(String csslocator) {
        waitForPageToLoad();
        //	WebElement shopTheLookContainer = waitForElementByCssLocator(shopTheLookContainerLocator);

        WebElement element = getWebDriver().findElement(By.cssSelector(csslocator));
        List<WebElement> productsList = element.findElements(By.cssSelector("li"));
        for (WebElement productNow : productsList) {
            String productID = productNow.getAttribute("class");
            scrollToElement("." + productID);
            waitForPageToLoad();
        }
    }

    public void clickOnSocialIcons(String icon, String csslocator) {
        waitForPageToLoad();
        scrollToPageTop();
        List<WebElement> iconsList = getWebDriver().findElements(By.cssSelector(csslocator));
        for (WebElement webElement : iconsList.subList(0, 4)) {
            if (webElement.getAttribute("class").toLowerCase().contains(icon.toLowerCase())) {
                webElement.click();
                break;
            }
        }
        waitForPageToLoad();
    }

    public String getNewWindowUrl(String icon, String csslocator) {
        waitForPageToLoad();
        scrollToPageTop();
        String popUpHandle = "";
        String url = "";
        String mainWindow = getWebDriver().getWindowHandle();
        clickOnSocialIcons(icon, csslocator);
        waitForScriptsToLoad();
        Set<String> openWindowsList = getWebDriver().getWindowHandles();
        for (String windowHandle : openWindowsList) {
            if (!windowHandle.equals(mainWindow)) {
                popUpHandle = windowHandle;
                getWebDriver().switchTo().window(popUpHandle);
                url = getCurrentURL();
                getWebDriver().close();
                break;
            }
        }
        getWebDriver().switchTo().window(mainWindow);
        return url;
    }

    public void clickOnFirstColourThumbnail(String csslocator) {
        waitForPageToLoad();
        List<WebElement> colours = waitForListElementByCssLocator(csslocator);
        colours.get(0).click();
    }

    public void clickOnBreadcrumb(String breadcrumb, String csslocator) {
        waitForPageToLoad();
        scrollToPageTop();
        List<WebElement> bcList = waitForListElementByCssLocator(csslocator);
        for (WebElement bc : bcList) {
            if (bc.getText().toLowerCase().contains(breadcrumb.toLowerCase())) {
                ((JavascriptExecutor) getWebDriver()).executeScript("arguments[0].click();", bc);
                // bc.click();
                break;
            }
        }
    }

    public String grabErrorMessage() {
        waitForPageToLoad();
        String result = "";
        List<WebElement> errorList = waitForListElementByCssLocator(selectSizeErrorSTLMsg);
        for (WebElement webElement : errorList) {
            result += webElement.getText();
        }
        return result;
    }

    public boolean isStickyNavPresent() {
        waitForPageToLoad();
        return isElementPresentByCssLocator(pdpStickyNavWrapper);
    }

    public String grabSizeErrorMessage() {
        String result = "";
        List<WebElement> productsList = getWebDriver().findElements(By.cssSelector(selectSizeErrorPDPMsg));
        if (productsList.size() > 0)
            result += productsList.get(0).getText();
        return result;
    }

    public void selectQtyfromDDonPDPByJSE(String qty){
        ((JavascriptExecutor) getWebDriver()).executeScript("$(\"#pdpMainWrapper #quantityDD\").val("+qty+").change()");
    }

    public void setProductSize(String size) {
        this.productSize = size;
    }

    public String getProductSize() {
        return productSize;
    }

    public void setRequiredProduct(String product) {
        this.requiredProduct = product;
    }

    public String getRequiredProduct() {
        return requiredProduct;
    }

}
