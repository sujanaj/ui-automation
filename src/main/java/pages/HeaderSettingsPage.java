package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import commonfunctions.CommonFunctions;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import utility.FormatterUtils;

public class HeaderSettingsPage extends CommonFunctions {

    config.ObjPropertyReader obj_Property= new config.ObjPropertyReader();
    String currencyOptionsListLocator = obj_Property.readProperty("currencyListSettingsHeaderByID");
    String saveButtonLocator = obj_Property.readProperty("saveSettingsButtonSettingsPageByClassName");
    String productPriceContainerLocator = obj_Property.readProperty("productPriceContainerLocator");
    String languageHeaderLocator = obj_Property.readProperty("languageHeaderLocatorByCSS");
    String shippingCountryLocator = obj_Property.readProperty("shippingCountrySettingsHeaderById");

    public List<String> checkCurrencyList() {
        List<WebElement> currencyList = getSelectDropdownElements(currencyOptionsListLocator);
        List<String> currencyStrList = new ArrayList<String>();
        for (WebElement option : currencyList) {
            currencyStrList.add(option.getText());
        }
        return currencyStrList;
    }

    public void clickShippingCountryButton() {
        waitForPageToLoad();
        waitForElementByCssLocator(shippingCountryLocator).click();
        waitForPageToLoad();
    }

    public void setCountryAndCurrency(String country, String currency) {
        currency = setCurrencyAlias(currency);
        selectFromDropdownByCSS("#shippingCountry",country);
        scrollToPageTop();
        selectFromDropdownByCSS("#billingCurrency",currency);
        scrollToPageTop();
        clickElementByCSS(saveButtonLocator);
       // saveSettings();
    }

    private String setCurrencyAlias(String currency) {
        Map<String, String> currencyAliases = new HashMap<String, String>();
        currencyAliases.put("USD", "US Dollar");
        currencyAliases.put("GBP", "Pound");
        currencyAliases.put("EUR", "Euro");
        currencyAliases.put("AUD", "Australian");
        currencyAliases.put("HKD", "Hong");
        currencyAliases.put("JPY", "Yen");
        String result = currency;
        for (Map.Entry<String, String> pair : currencyAliases.entrySet()) {
            if (pair.getKey().toLowerCase().contentEquals(currency.toLowerCase().trim())) {
                result = pair.getValue();
                break;
            }
        }

        return result;
    }

    public String getProductPriceCurrency() {
        String price = "";
        price = waitForElementByCssLocator(productPriceContainerLocator).getText();
        return FormatterUtils.parseCurrency(price);
    }

    public String grabHeaderLanguage() {
        waitForPageToLoad();
        try {
            return getWebElement(languageHeaderLocator).getText().trim();
        } catch (Exception e) {
            return "No language found in header.";
        }
    }

    public String grabCountryLabel() {
        waitForPageToLoad();
        return getWebElement(shippingCountryLocator).getText();
    }
}
