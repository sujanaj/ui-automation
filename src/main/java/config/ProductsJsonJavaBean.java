package config;

import java.util.ArrayList;


public class ProductsJsonJavaBean {

    // LEVEL1CATEGORIESDATA, ITS GETTER, SETTER
    ArrayList<Object> level1CategoriesData = new ArrayList<Object>(10000);
    //PAGINATION OBJECT
    // Pagination pagination;
    //Category NAVDATA ARRAYLIST AND ITS GETTER, SETTER
    private ArrayList<Object> categoryNavData = new ArrayList<Object>(1000);
    //RESULTS ARRAYLIST AND ITS GETTER , SETTER
    private ArrayList<Object> results = new ArrayList<Object>(10000);
    private ArrayList<Object> facets = new ArrayList<Object>(10000);
    //CATEGORYSUBTREEDATA
    private String categorySubTreeData;
    //FACETS ARRRAYLIST AND ITS GETTER,SETTER
    //CATEGORYPATHDATA ARRAYLIST AND ITS GETTER, SETTER
    private ArrayList<Object> categoryPathData = new ArrayList<Object>(10000);

    public ArrayList<Object> getCategoryNavData() {
        return categoryNavData;
    }

    public void setCategoryNavData(ArrayList<Object> categoryNavData) {
        this.categoryNavData = categoryNavData;
    }

    public ArrayList<Object> getResults() {
        return results;
    }

    public void setResults(ArrayList<Object> results) {
        this.results = results;
    }

    public ArrayList<Object> getFacets() {
        return facets;
    }

    public void setFacets(ArrayList<Object> facets) {
        this.facets = facets;
    }

    public String getCategorySubTreeData() {
        return categorySubTreeData;
    }

    public void setCategorySubTreeData(String categorySubTreeData) {
        this.categorySubTreeData = categorySubTreeData;
    }

    public ArrayList<Object> getCategoryPathData() {
        return categoryPathData;
    }

    public void setCategoryPathData(ArrayList<Object> categoryPathData) {
        this.categoryPathData = categoryPathData;
    }

    public ArrayList<Object> getLevel1CategoriesData() {
        return level1CategoriesData;
    }

    public void setLevel1CategoriesData(ArrayList<Object> level1CategoriesData) {
        this.level1CategoriesData = level1CategoriesData;
    }


}
