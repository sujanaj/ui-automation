package runner;

import baseclass.BaseClass;
import cucumber.api.CucumberOptions;
import cucumber.api.testng.AbstractTestNGCucumberTests;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Parameters;

import java.net.MalformedURLException;

@CucumberOptions(features={"src//test//resources//feature//pdp/"
}
        ,glue={"stepdefinition"}
        ,plugin = {"pretty", "html:target/cucumber",
        "json:target/cucumber/cucumber.json"})

public class PDP_Runner extends AbstractTestNGCucumberTests{


    BaseClass baseClass = new BaseClass();
    @Parameters("Browser")
    @BeforeClass
    public void test(String Browser) throws IllegalAccessException, MalformedURLException {
        baseClass.beforeClass(Browser);
    }
    @AfterClass
    public void test1()  {
        baseClass.afterClass();
    }
}
