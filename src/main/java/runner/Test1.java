package runner;

import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Factory;
import org.testng.annotations.Parameters;

public class Test1 {
	private WebDriver driver;
	/*@Factory
	public Object[] testCategories() {
	  System.out.println("start test factory");
	  Test test = new Test();
	  
	

	  List<String> category = test.getCate();
	  Object[] tests = new Object[category.size()];

	  for (int i = 0; i < category.size(); i++) 
	    tests[i] = new Test(category.get(i));

	  System.out.println("end of test factory");

	  return tests; 
	}*/
	
	@Factory
    public Object[] createInstances() {
		
        Object[] obj = new Object[1];

        //for (int iter = 0; iter < 1; iter++) {
        	//System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver");
        	
           /* obj[0] = new Test(new ChromeDriver());
            System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver");
            obj[1] = new Test(new FirefoxDriver());
            obj[2] = new Test(new SafariDriver());*/
        String accessoriesCategories="accessoriesCategories";
        String cat="cat";
        obj[0] = new Test(driver);
      //  obj[1] = new Test(driver);
      //  obj[2] = new Test(driver);
      //  }

        return obj;
    }
	
	@Parameters("Browser")
	@BeforeClass
    public void beforeClass(String Browser) throws IllegalAccessException {
		switch(Browser) {
		case "Safari":
			driver= new SafariDriver();
			driver.manage().window().maximize();
			long safari_ID= Thread.currentThread().getId();
			System.out.println("Safari browser thread id is" + safari_ID);
			break;
		case "Firefox":
			System.setProperty("webdriver.gecko.driver", "src/test/resources/drivers/geckodriver");
			driver= new FirefoxDriver();
			long firegox_ID= Thread.currentThread().getId();
			System.out.println("Firefox browser thread id is" + firegox_ID);
			break;
		case "Chrome":
			System.setProperty("webdriver.chrome.driver", "src/test/resources/drivers/chromedriver");
			driver= new ChromeDriver();
			long chrome_ID= Thread.currentThread().getId();
			System.out.println("Chrome browser thread id is" + chrome_ID);
			break;
		
		default:
			throw new IllegalAccessException();
		}
    }
	 @AfterClass
	    public void afterClass() {
	        driver.quit();
	    }

}
