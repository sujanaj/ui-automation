package utility;

import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import baseclass.BaseClass;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

public class ExtentTestManager extends BaseClass {

	String scenarioNameText=null;
	String countryText=null;
	public ExtentTest test;
	static Map<Integer, ExtentTest> extentTestMap = new HashMap<Integer, ExtentTest>();
	static ExtentReports extent = ExtentManager.getReporter();

	public static synchronized ExtentTest getTest() {
		Thread.currentThread();
		return (ExtentTest) extentTestMap.get((int) (long) (Thread.currentThread().getId()));
	}


	public static synchronized void endTest() {
		extent.endTest((ExtentTest) extentTestMap.get((int) (long) (Thread.currentThread().getId())));
	}

	public synchronized ExtentTest startTest(String testName, String desc) {
		Capabilities caps = ((RemoteWebDriver) getWebDriver()).getCapabilities();
		test = extent.startTest(testName, desc);
		test.assignCategory(caps.getBrowserName());

		extentTestMap.put((int) (long) (Thread.currentThread().getId()), test);
		return test;
	}

	public String getScenarioName()  {
		try {
			BufferedReader bufferedReader = new BufferedReader(new FileReader("scenarioName.txt"));
			StringBuilder stringBuilder = new StringBuilder();
			String scenarioText = bufferedReader.readLine();

			while (scenarioText != null) {
				stringBuilder.append(scenarioText);
				stringBuilder.append(System.lineSeparator());
				scenarioText = bufferedReader.readLine();
			}
			scenarioNameText = stringBuilder.toString();
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return scenarioNameText;
	}

	public String getCountryName()  {
		try {
			BufferedReader br = new BufferedReader(new FileReader("countryName.txt"));
			StringBuilder sb = new StringBuilder();
			String st = br.readLine();

			while (st != null) {
				sb.append(st);
				sb.append(System.lineSeparator());
				st = br.readLine();
			}
			countryText = sb.toString();
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return countryText;
	}
}
