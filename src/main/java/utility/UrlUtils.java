package utility;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

import commonfunctions.NavigateUsingURL;
import config.Constants;

public class UrlUtils {

	private String[] urlParams;
	private NavigateUsingURL navigateUsingURL = new NavigateUsingURL();
	public static String getUrlStatusCode(String resource) throws IOException {
		System.out.println("Verify image: " + resource);
		URL url = new URL(resource);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		connection.setRequestMethod("GET");
		connection.connect();
		int code = connection.getResponseCode();
		return String.valueOf(code);
	}

	public  String cleanUpURL(String pageURL) {


		if (navigateUsingURL.getrelativeURLJenkins() == null) {
			urlParams = Constants.BASE_URL.split("/");
		} else {
			String relativeURLJenkins = navigateUsingURL.getrelativeURLJenkins();
			urlParams = relativeURLJenkins.split("/");
		}
		for (String string : urlParams) {
			pageURL = pageURL.replace(string, "");
		}
		pageURL = pageURL.replace("http://", "");
		pageURL = pageURL.replace("https://", "");
		pageURL = pageURL.replace("//", "/");
		return pageURL;
	}

	public  void main(String args[]) throws IOException {
		System.out.println(getUrlStatusCode("https://api.sail-personalize.com/v1/personalize/simple?"));
	}

}
