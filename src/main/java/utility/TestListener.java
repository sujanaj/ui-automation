package utility;


import org.openqa.selenium.Capabilities;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import com.relevantcodes.extentreports.LogStatus;
import com.relevantcodes.extentreports.ExtentReports;
import baseclass.BaseClass;

public class TestListener extends BaseClass implements ITestListener{
	public static ExtentReports extent;
	ExtentTestManager ExtentTestManager = new ExtentTestManager();
	private static String getTestMethodName(ITestResult iTestResult) {

		return iTestResult.getMethod().getConstructorOrMethod().getName();
	}

	private  String getTestMethodInstance(ITestResult iTestResult) {
		Capabilities caps = ((RemoteWebDriver) getWebDriver()).getCapabilities();
		String result  =  iTestResult.getClass().getName();
		return result.replace(result, caps.getBrowserName());
	}


	//Before starting all tests, below method runs.
	@Override
	public void onStart(ITestContext iTestContext) {

		System.out.println("I am in onStart method " + iTestContext.getName());
		iTestContext.setAttribute("WebDriver", this.getWebDriver());

	}

	//After ending all tests, below method runs.
	@Override
	public void onFinish(ITestContext iTestContext) {
		System.out.println("I am in onFinish method " + iTestContext.getName());
		//Do tier down operations for extentreports reporting!
		ExtentTestManager.endTest();
		ExtentManager.getReporter().flush();
	}

	@Override
	public void onTestStart(ITestResult iTestResult) {
		Capabilities caps = ((RemoteWebDriver) getWebDriver()).getCapabilities();
		System.out.println("I am in onTestStart method " +  getTestMethodName(iTestResult) + " start");
		//ExtentTestManager.startTest(iTestResult.getMethod().getMethodName(),caps.getBrowserName());
		String scenarios= ExtentTestManager.getScenarioName();
		ExtentTestManager.startTest(scenarios,caps.getBrowserName());

	}

	@Override
	public void onTestSuccess(ITestResult iTestResult) {
		Capabilities caps = ((RemoteWebDriver) getWebDriver()).getCapabilities();
		System.out.println("I am in onTestSuccess method " +  getTestMethodName(iTestResult) + " succeed");
		System.out.println("I am in onTestSuccess method " +  getTestMethodInstance(iTestResult) + " succeed");
		String scenarios= ExtentTestManager.getScenarioName();
		String countryName =ExtentTestManager.getCountryName();
		//ExtentTestManager.getTest().log(LogStatus.PASS, scenarios,countryName,caps.getBrowserName());
		ExtentTestManager.getTest().log(LogStatus.PASS, scenarios,countryName);

	}

	@Override
	public void onTestFailure(ITestResult iTestResult) {
		System.out.println("I am in onTestFailure method " +  getTestMethodName(iTestResult) + " failed");
		String scenarios= ExtentTestManager.getScenarioName();
		String countryName =ExtentTestManager.getCountryName();
		ExtentTestManager.getTest().log(LogStatus.FAIL,scenarios+" "+countryName, iTestResult.getThrowable().getMessage());
		Object testClass = iTestResult.getInstance();
		WebDriver webDriver = ((BaseClass) testClass).getWebDriver();

		//Take Screenshot screenshot.
		String base64Screenshot = "data:image/png;base64,"+((TakesScreenshot)webDriver).
				getScreenshotAs(OutputType.BASE64);

		//Extentreports log and screenshot operations for failed tests.
		ExtentTestManager.getTest().log(LogStatus.FAIL,"Test Failed",
				ExtentTestManager.getTest().addBase64ScreenShot(base64Screenshot));

	}

	@Override
	public void onTestSkipped(ITestResult iTestResult) {
		System.out.println("I am in onTestSkipped method "+  getTestMethodName(iTestResult) + " skipped");
		//Extentreports log operation for skipped tests.
		String scenarios= ExtentTestManager.getScenarioName();
		ExtentTestManager.getTest().log(LogStatus.SKIP,scenarios, "Test Skipped");


	}

	@Override
	public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {
		System.out.println("Test failed but it is in defined success ratio " + getTestMethodName(iTestResult));
	}



}
