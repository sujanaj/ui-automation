package baseclass;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;
import java.net.URL;

public class CrossBrowser {
    static String username = "dilbag.singh%40matchesfashion.com"; // Your username
    static String authkey = "ud4eb9944f190ba6";  // Your authkey
    String testScore = "unset";

    public static void main(String[] args) throws Exception {


        DesiredCapabilities caps = new DesiredCapabilities();
        caps.setCapability("build", "1.0");
        caps.setCapability("browserName", "Internet Explorer");
        caps.setCapability("version", "11");
        caps.setCapability("platform", "Windows 10");
        caps.setCapability("screenResolution", "1366x768");
        caps.setCapability("record_video", "true");
        caps.setCapability("record_network", "false");


        RemoteWebDriver driver = new RemoteWebDriver(new URL("http://" + username + ":" + authkey + "@hub.crossbrowsertesting.com:80/wd/hub"), caps);
        System.out.println(driver.getSessionId());

        // we wrap the test in a try catch loop so we can log assert failures in our system
        try {

            // load the page url
            System.out.println("Loading Url");
            driver.get("https://regression.matchesfashion.com");

        }catch(AssertionError ae) {

        }
    }
}